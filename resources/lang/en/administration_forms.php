<?php
/**
 * By: Martin Auterský
 * Date: 11.06.2019
 * Time: 14:27
 */

return [
    // League
    'league_header' => 'New league',
    'league_name' => 'League name:',
    'league_description' => 'League description:',
    'league_overview' => 'League settings',
    'league_confirm' => 'League update',
    'league_rename' => 'Rename league',
    'league_delete_confirm' => 'Delete league',

    // Split
    'split_header' => 'Split settings',
    'split_header_new' => 'New split for :name:',
    'split_name' => 'Split name:',
    'split_platform' => 'Platform:',
    'split_game' => 'Game:',
    'split_region' => 'Region:',
    'split_max_per_team' => 'Maximum drivers per team:',
    'split_max_per_race' => 'Maximum drivers per race:',
    'split_points' => 'Points:',
    'split_season' => 'Initial season:',
    'split_season_current' => 'Current season:',
    'split_no_points_dnf' => 'No points for DNF:',
    'split_omit_inactive' => 'Omit inactive from standings:',
    'split_confirm' => 'Split update',
    'split_delete_confirm' => 'Delete split',

    // Drivers
    'driver_names' => 'Driver name(s):',
    'driver_update_title' => 'Update driver',
    'driver_add_title' => 'Add driver(s)',
    'driver_team' => 'Team:',
    'driver_class' => 'Class:',
    'driver_active' => 'Active:',
    'driver_no_team' => 'No team',
    'driver_no_class' => 'No class',
    'driver_deactivate' => 'Deactivate driver',
    'driver_remove' => 'Remove or deactivate driver',
    'driver_add_class_title' => 'Add class(es)',
    'class_names' => 'Class name(s)',
    'class_remove' => 'Remove class',

    // Teams
    'team_add_title' => 'Add team(s)',
    'team_update_title' => 'Update team',
    'team_names' => 'Team name(s):',
    'team_primary' => 'Primary colour:',
    'team_secondary1' => 'Secondary colour 1:',
    'team_secondary2' => 'Secondary colour 2:',
    'team_remove' => 'Remove team',

    // Admins
    'admin_transfer' => 'Superadmin transfer',
    'admin_revoke' => 'Revoke admin permission',

    // New season
    'season_title' => 'Start new season',
    'season_text' => 'You can transfer active drivers and teams to the new season. Every driver will be transfered as a reserve driver.',
    'season_drivers' => 'Transfer active drivers',
    'season_teams' => 'Transfer teams',
    'season_button' => 'Start new season',

    // Calendar
    'calendar_title' => 'Update race information',
    'calendar_tracks' => 'Track(s):',
    'calendar_country' => 'Country:',
    'calendar_country_empty' => 'Select country',
    'calendar_country_all' => 'All countries',
    'calendar_date' => 'Date:',
    'calendar_multiplier' => 'Multiplier:',
    'calendar_remove' => 'Remove race',

    // Penalty
    'penalty_driver' => 'Driver:',
    'penalty_race' => 'Valid for:',
    'penalty_type' => 'Penalty type:',
    'penalty_amount' => 'Amount:',
    'penalty_points_race' => 'Added in:',
    'penalty_remove_points' => 'Remove penalty points',
    'penalty_remove' => 'Remove penalty',

    // Other
    'tip_max_per_team' => 'Fill in 0 if not specified',
    'tip_points' => 'Example: 25,18,15,10,6,4,2,1',
    'tip_drivers' => 'To add more than one driver at a time, put every driver on a new line',
    'tip_teams' => 'To add more than one team at a time, put every team on a new line',
    'tip_tracks' => 'To add more than one track at a time, put every track on a new line',
    'tip_classes' => 'To add more than one class at a time, put every class on a new line',
    'button_create' => 'Create',
    'button_update' => 'Update',
    'button_close' => 'Close',
    'button_confirm' => 'Confirm',
    'button_update_description' => 'Update description',
    'button_add_drivers' => 'Add driver(s)',
    'button_update_drivers' => 'Update drivers',
    'button_update_split' => 'Update split information',
    'button_add_teams' => 'Add team(s)',
    'button_add_tracks' => 'Add track(s)',
    'button_add_track' => 'Add track',
    'button_add_penalty' => 'Issue penalty',
    'button_add_penalty_points' => 'Add penalty points',
    'button_select_race' => 'Select race',
    'button_save_results' => 'Save results',
    'button_save_changes' => 'Save changes',
    'button_rename_league' => 'Rename league',
    'button_load' => 'Load',
    'button_add_classes' => 'Add class(es)',
    'other_name' => 'Name:',
    'other_confirm' => 'Are you sure you want to make these changes?',
    'other_inactive' => 'Are you sure you want to make this driver inactive?',
    'other_transfer' => 'Are you sure you want to transfer your superadmin rights? You can\'t take this action back!',
    'other_revoke' => 'Are you sure you want to revoke admin\'s permission to edit this league? You can\'t take this action back!',
    'other_remove_race' => 'Are you sure you want to remove this race from the calendar?',
    'other_remove_points' => 'Are you sure you want to remove oldest penalty points for this driver?',
    'other_remove_penalty' => 'Are you sure you want to remove selected penalty?',
    'other_remove_driver' => 'This action will remove selected driver if the driver did not participate in the season,
                                otherwise it will make the driver inactive. Are you sure you want to do that?',
    'other_remove_team' => 'This action will remove selected team if the team did not participate in the season.
                                Are you sure you want to do that?',
    'other_rename_league' => 'Beware that this action will change league url!',
    'other_delete_confirm' => 'Are you sure you want to delete the league? This action cannot be reverted!',
    'other_delete_split_confirm' => 'If this is the league\'s last split, the league will be deleted too. Are you sure you want to delete the split? This action cannot be reverted!',
    'other_load_file_example' => 'CSV format examples',
    'other_remove_class' => 'This action will remove selected class. Are you sure you want to do that?',
];
