<?php
/**
 * By: Martin Auterský
 * Date: 04.06.2019
 * Time: 14:39
 */

return [
    'league' => 'league',
    'split' => 'split',
    'description' => 'description',
    'user' => 'user',
    'driver' => 'driver',
    'team' => 'team',
    'driver(s)' => 'driver(s)',
    'team(s)' => 'team(s)',
    'track(s)' => 'track(s)',
    'track' => 'track',
    'penalty' => 'penalty',
    'results' => 'results',
    'penalty_points' => 'penalty points',
    'class(es)' => 'class(es)',
];
