<?php
/**
 * By: Martin Auterský
 * Date: 04.06.2019
 * Time: 14:26
 */

return [
    /*
     * General
     */
    'success_update' => ':name updated successfully',
    'error_update' => 'An error occurred, :name update unsuccessful',
    'success_create' => ':name created successfully',
    'error_create' => 'An error occurred, :name creation unsuccessful',
    'error_denied' => 'This league/split doesn\'t exist or you don\'t have permission to edit it',
    'error_not_found' => 'Selected :name not found',
    'error_not_admin' => 'Selected user is not an admin',
    'success_add' => ':name added',
    'error_add' => 'An error occurred, unable to add :name',

    /*
     * League
     */
    'success_rename_league' => 'League successfully renamed',
    'success_delete_league' => 'League successfully deleted',
    'error_delete_league' => 'An error occurred, league removal unsuccessful',

    /*
     * Splits
     */
    'success_delete_split' => 'Split successfully deleted',
    'error_delete_split' => 'An error occurred, split removal unsuccessful',

    /*
     * Requests and admins
     */
    'request_approved' => 'Request successfully approved',
    'request_disapproved' => 'Request successfully disapproved',
    'error_approval' => 'Request was not resolved',
    'request_sent' => 'Request was sent',
    'error_request' => 'Request was not sent',
    'request_twice' => 'You already sent this request',
    'request_managed' => 'You already have admin rights for this split',
    'permission_revoked' => 'Permission revoked',
    'error_revoke' => 'An error occurred, permission revoke unsuccessful',
    'success_transfer_sa' => 'Superadmin transfer successful',
    'error_transfer_sa' => 'An error occurred, superadmin transfer unsuccessful',

    /*
     * Drivers and teams
     */
    'success_driver_deactivated' => 'Driver deactivated',
    'error_team_full' => 'Team is already full',
    'error_deactivate_driver' => 'An error occurred, unable to deactivate driver',
    'error_inactive' => 'An inactive driver can\'t have a team',
    'success_driver_removed' => 'Driver removed',
    'error_team_not_removed' => 'This team cannot be removed, because it\'s already participated in the season',
    'error_remove_team' => 'An error occurred, unable to remove team',
    'success_team_removed' => 'Team removed',
    'success_load' => 'File loaded successfully',
    'error_load' => 'An error occurred, unable to load the file',
    'success_update_team' => 'Team updated',
    'success_class_removed' => 'Class removed',

    /*
     * Season
     */
    'error_season' => 'An error occurred, unable to start new season',
    'success_season' => 'New season successfully started',

    /*
     * Calendar
     */
    'error_old_race' => 'Old races (with results) can\'t be updated',
    'error_remove_race' => 'An error occurred, race removal unsuccessful',
    'success_remove_race' => 'Race successfully removed',
    'error_get_tracks' => 'An error occurred when retrieving tracks',
    'error_track_exists' => 'This track already exists in the system',

    /*
     * Penalties
     */
    'success_issue_penalty' => 'Penalty was successfully issued',
    'success_add_points' => 'Points successfully added to the driver',
    'error_remove_points' => 'An error occurred, penalty points removal unsuccessful',
    'success_remove_points' => 'Penalty points successfully removed',
    'error_not_applicable' => 'No suitable results were found',
    'success_apply_penalty' => 'Penalty successfully applied to the results',
    'error_apply_penalty' => 'An error occurred, penalty application unsuccessful',
    'success_remove_penalty' => 'Penalty successfully removed',

    /*
     * Results
     */
    'error_no_race' => 'There is no such race',
];
