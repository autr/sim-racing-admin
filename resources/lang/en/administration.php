<?php
/**
 * By: Martin Auterský
 * Date: 05.07.2019
 * Time: 15:07
 */

return [
    // Splits
    'split_link' => 'Link to the split page:',

    // Drivers
    'drivers_title' => 'Drivers',
    'drivers_title_load_file' => 'Load from CSV file',
    'drivers_name' => 'Name',
    'drivers_team' => 'Team',
    'drivers_class' => 'Class',
    'drivers_active' => 'Active',
    'drivers_active_yes' => 'Yes',
    'drivers_active_no' => 'No',
    'drivers_no_team' => 'No team',
    'drivers_no_class' => 'No class',
    'drivers_overview' => 'Manage drivers in :league - :split',
    'drivers_empty' => 'No drivers yet',
    'drivers_edit_driver' => 'Edit driver',
    'drivers_remove' => 'Remove or make driver inactive',
    'drivers_add_drivers' => 'Add new driver(s)',
    'drivers_load_file' => 'Load drivers and teams from CSV file for :league - :split',
    'drivers_add_classes' => 'Add new class(es)',
    'classes_overview' => 'Manage classes',
    'classes_empty' => 'No classes yet',
    'classes_remove' => 'Remove class',
    'classes_name' => 'Name',

    // Teams
    'teams_title' => 'Teams',
    'teams_name' => 'Name',
    'teams_colour' => 'Colour',
    'teams_overview' => 'Teams in :league - :split',
    'teams_empty' => 'No teams yet',
    'teams_edit_team' => 'Edit team',
    'teams_add_teams' => 'Add team(s)',
    'teams_remove' => 'Remove team',

    // Calendar
    'calendar_title' => 'Race calendar',
    'calendar_overview' => 'Race calendar for :league - :split',
    'calendar_empty' => 'No races yet',
    'calendar_track' => 'Track',
    'calendar_country' => 'Country',
    'calendar_date' => 'Date',
    'calendar_country_not_set' => 'Unknown',
    'calendar_date_not_set' => 'TBA',
    'calendar_pick_track' => 'Add new track to the calendar',
    'calendar_existing' => 'Pick from existing tracks',
    'calendar_new' => 'Add new track',
    'calendar_edit_race' => 'Edit race',
    'calendar_remove_race' => 'Remove race',
    'calendar_multiplier' => 'Points multiplier',
    'calendar_multiplier_not_set' => 'Unknown',

    // Penalties
    'penalties_title' => 'Penalties',
    'penalties_cant_issue' => 'There are no available drivers and/or races to issue a penalty',
    'penalties_overview' => 'Issued penalties for :league - :split',
    'penalties_empty' => 'No penalties yet',
    'penalties_driver' => 'Driver',
    'penalties_race' => 'Valid for',
    'penalties_type' => 'Type',
    'penalties_amount' => 'Amount',
    'penalties_na' => 'Not applicable',
    'penalties_issue_penalty' => 'Issue a penalty',
    'penalties_add_points' => 'Add penalty points',
    'penalties_points_overview' => 'Penalty points for :league - :split',
    'penalties_points_empty' => 'No penalty points yet',
    'penalties_cant_add_points' => 'There are no available drivers and/or races to add penalty points',
    'penalties_applied' => 'Applied',
    'penalties_oldest' => 'Oldest',
    'penalties_newest' => 'Newest',
    'penalties_change_applied' => 'Change status to applied',
    'penalties_change_not_applied' => 'Change status to not applied',
    'penalties_remove' => 'Remove selected penalty',
    'penalties_apply' => 'Apply to the results and change status to applied',

    // Results
    'results_available_drivers' => 'Available drivers for selected race',
    'results' => 'Race results',
    'results_no_races' => 'There are currently no races available for results',
    'results_position' => 'Pos',
    'results_driver' => 'Driver',
    'results_team' => 'Team',
    'results_points' => 'Pts',
    'results_bonus_points' => 'Bonus pts',
    'results_no_team' => 'No team',
    'results_select_race' => 'Select a race:',
    'results_additional' => 'Additional information',
    'results_no_drivers' => 'There are currently no drivers available',
    'results_class' => 'Class',
    'results_no_class' => 'No class',
];
