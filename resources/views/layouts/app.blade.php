<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'SRA') }} {{ __(' - ') }} @yield('title')</title>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Fira+Sans:500&display=swap&subset=cyrillic,cyrillic-ext,greek,greek-ext,latin-ext,vietnamese" rel="stylesheet">

    <!-- Style -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/css/bootstrap.min.css" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/mdbootstrap/4.8.10/css/mdb.min.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Trumbowyg/2.21.0/ui/trumbowyg.min.css" integrity="sha512-XjpikIIW1P7jUS8ZWIznGs9KHujZQxhbnEsqMVQ5GBTTRmmJe32+ULipOxFePB8F8j9ahKmCjyJJ22VNEX60yg==" crossorigin="anonymous" />
    <link href="{{ asset('css/custom.css') }}" rel="stylesheet">
    <link href="{{ asset('css/cookiealert.css') }}" rel="stylesheet">

    <!-- Scripts -->
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://kit.fontawesome.com/d22a24fe70.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.4/umd/popper.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/mdbootstrap/4.8.10/js/mdb.min.js"></script>
    <script
        src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js"
        integrity="sha256-VazP97ZCwtekAsvgPBSUwPFKdrwD3unUfSGVYrahUqU="
        crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui-touch-punch/0.2.3/jquery.ui.touch-punch.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Trumbowyg/2.21.0/trumbowyg.min.js" integrity="sha512-l6MMck8/SpFCgbJnIEfVsWQ8MaNK/n2ppTiELW3I2BFY5pAm/WjkNHSt+2OD7+CZtygs+jr+dAgzNdjNuCU7kw==" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Trumbowyg/2.21.0/plugins/resizimg/resizable-resolveconflict.min.js" integrity="sha512-/HdSDGUjKvGlBJsE1yHPXmPqZZdOBt9cZyaLk76+5MhlCdAL5wlUGANYzrrq6ccMRo96UUVB5ftCFZU6/GXw/w==" crossorigin="anonymous"></script>
    <script type="text/javascript" src="{{ asset('js/jquery-resizable.min.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Trumbowyg/2.21.0/plugins/resizimg/trumbowyg.resizimg.min.js" integrity="sha512-wcv4v44EOKOhZVt3qbBUvFkpVWptDdMtgePjDxAJ0pLbm//6wJFsf6CaXKP+NYjQjdgJfVPtV4wpY/opmEYsjg==" crossorigin="anonymous"></script>
</head>
<body>
<header>

    <!-- Navbar -->
    <nav class="navbar fixed-top navbar-expand-lg bg-accent light-text navbar-dark">
        <div class="container-fluid">

            <!-- Brand -->
            <a class="navbar-brand waves-effect" href="{{ url('/') }}">
                <strong class="light-text">{{ config('app.name', 'SRA') }}</strong>
            </a>

            <!-- Collapse -->
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                    aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <!-- Links -->
            <div class="collapse navbar-collapse" id="navbarSupportedContent">

                <!-- Left -->
                <ul class="navbar-nav mr-auto">
                    @yield('navigation')
                </ul>

                <!-- Right -->
                <ul class="navbar-nav nav-flex-icons">
                    <li class="nav-item">
                        <a class="nav-link red white-text" href="{{ route('bug-report') }}" title="Bug report">
                            <i class="fas fa-bug"></i>
                            <span>{{ __('Bugs') }}</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link green white-text" href="{{ route('guide') }}" title="Guide">
                            <i class="fas fa-book"></i>
                            <span>{{ __('Guide') }}</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link blue white-text" href="https://forms.gle/BgJ95st8noEvEpxc9" target="_blank" title="Feedback">
                            <i class="fas fa-poll-h"></i>
                            <span>{{ __('Feedback') }}</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link purple white-text" href="https://discord.gg/yhnybhY" target="_blank" title="Discord">
                            <i class="fab fa-discord"></i>
                            <span>{{ __('Discord') }}</span>
                        </a>
                    </li>
                    @guest
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                        </li>
                        @if (Route::has('register'))
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                            </li>
                        @endif
                    @else
                        <li class="nav-item dropdown">
                            <a id="loginDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                {{ Auth::user()->name }} <span class="caret"></span>
                            </a>

                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="loginDropdown">
                                <a class="dropdown-item" href="{{ route('logout') }}"
                                   onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                    {{ __('Logout') }}
                                </a>

                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    @csrf
                                </form>
                            </div>
                        </li>
                    @endguest
                </ul>
            </div>
        </div>
    </nav>
    <!-- Navbar -->
</header>

<div class="wrap">
<!--Main layout-->
<main class="pt-5 mx-lg-5 h-100">
    <div class="container-fluid mt-5">
        <!-- Heading -->
        <div class="card col-md-12 mb-4">

            <!--Card content-->
            <div class="card-body d-sm-flex justify-content-between">

                <h4 class="mb-sm-0 pt-1 light-text">
                    @yield('title')
                </h4>

            </div>

        </div>
        <!-- Heading -->

        <!-- Content -->
        <div class="row justify-content-center">
            @yield('content')
        </div>
        <!-- Content -->
    </div>
</main>
<!--Main layout-->

<footer class="page-footer text-center font-small bg-accent mt-4 footer-abs">
    <!-- Footer Elements -->
    <div class="container">

        <!-- Grid row-->
        <div class="row">

            <!-- Grid column -->
            <div class="col-md-12">
                <div class="mb-5 flex-center">

                    <!-- GitLab -->
                    <a class="fb-ic" href="https://gitlab.com/autr/sim-racing-admin" target="_blank" title="GitLab">
                        <i class="fab fa-gitlab fa-lg white-text mr-md-5 mr-3 fa-2x"> </i>
                    </a>
                    <!-- E-mail -->
                    <a class="tw-ic" href="mailto:simracingadm@gmail.com" title="E-mail">
                        <i class="fas fa-envelope fa-lg white-text mr-md-5 mr-3 fa-2x"> </i>
                    </a>
                    <!-- Discord -->
                    <a class="tw-ic" href="https://discord.gg/yhnybhY" title="Discord">
                        <i class="fab fa-discord fa-lg white-text mr-md-5 mr-3 fa-2x"> </i>
                    </a>
                </div>
            </div>
            <!-- Grid column -->

        </div>
        <!-- Grid row-->

    </div>
    <!-- Footer Elements -->
    <!--Copyright-->
    <div class="footer-copyright py-3">
        v0.9.9 - &copy; 2021 Martin Auterský - GNU GPLv3 <br>
        Design powered by <a href="https://mdbootstrap.com/" target="_blank"> MDBootstrap.com </a>
    </div>
    <!--/.Copyright-->
</footer>
    @if (Cookie::get('acceptCookies') == null)
    <!-- START Bootstrap-Cookie-Alert -->
    <div class="alert text-center cookiealert" role="alert">
        <b>This app uses cookies to store session information.</b>

        <button type="button" class="btn btn-primary btn-sm acceptcookies">
            OK
        </button>
    </div>
    <!-- END Bootstrap-Cookie-Alert -->
    @endif
</div>

<!-- Scripts -->
<script src="{{ asset('js/jscript-min.js') }}"></script>
<script src="{{ asset('js/clipboard.min.js') }}"></script>
<script src="{{ asset('js/cookiealert.js') }}"></script>
<script src="{{ asset('js/html2canvas.min.js') }}"></script>
<script src="{{ asset('js/canvas2image.js') }}"></script>
<script src="{{ asset('js/screenshots.js') }}"></script>
<script src="{{ asset('js/description-editor.js') }}"></script>
{{--<script src="{{ asset('js/modal_handling.js') }}"></script>--}}
{{--<script src="{{ asset('js/calendar.js') }}"></script>--}}
{{--<script src="{{ asset('js/penalties.js') }}"></script>--}}
{{--<script src="{{ asset('js/results.js') }}"></script>--}}
{{--<script src="{{ asset('js/browsing.js') }}"></script>--}}
{{--<script src="{{ asset('js/split_overview.js') }}"></script>--}}
{{--<script src="{{ asset('js/sessionStorage.js') }}"></script>--}}
</body>
</html>
