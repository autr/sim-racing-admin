<form>
    @csrf
    <input id="idRace-u" type="hidden" name="idRace" value="">
    <div class="modal-body bg-main light-text">
        <div id="error-u" class="alert alert-danger" style="display:none"></div>
        <div id="success-u" class="alert alert-success" style="display:none"></div>
        <div class="form-group row">
            <span class="col-md-4 text-md-right light-text">{{ __('administration_forms.other_name') }}</span>

            <div class="col-md-6 light-text">
                <span id="trackName-modal"></span>
            </div>
        </div>
        <div class="form-group row">
            <span class="col-md-4 text-md-right light-text">{{ __('administration_forms.calendar_country') }}</span>

            <div class="col-md-6 light-text">
                <span id="trackCountry-modal"></span>
            </div>
        </div>
        <div class="form-group row">
            <label for="raceDate" class="col-md-4 col-form-label text-md-right light-text">{{ __('administration_forms.calendar_date') }}</label>

            <div class="col-md-6">
                <input id="raceDate" class="form-control" type="date" value="">
            </div>
        </div>
        <div class="form-group row">
            <label for="raceMultiplier" class="col-md-4 col-form-label text-md-right light-text">{{ __('administration_forms.calendar_multiplier') }}</label>

            <div class="col-md-6">
                <input id="raceMultiplier" class="form-control" type="number" value="" min="0">
            </div>
        </div>
        <div class="form-group row flex-center">
            <button type="button" class="btn btn-danger" data-dismiss="modal">{{ __('administration_forms.button_close') }}</button>
            <button id="updateRace-btn" class="btn bg-accent light-text">{{ __('administration_forms.button_update') }}</button>
        </div>
    </div>
</form>
