<form>
    @csrf
    <div class="modal-body bg-main light-text">
        <div id="error" class="alert alert-danger" style="display:none"></div>
        <div id="success" class="alert alert-success" style="display:none"></div>
        <div class="form-group row">
            <label for="teams" class="col-md-4 col-form-label text-md-right light-text">{{ __('administration_forms.team_names') }}</label>

            <div class="col-md-6">
                    <textarea id="teams" class="form-control{{ $errors->has('drivers') ? ' is-invalid' : '' }}"
                              name="teams" rows="5" placeholder="{{ __('administration_forms.tip_teams') }}" required></textarea>

                @if ($errors->has('teams'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('teams') }}</strong>
                    </span>
                @endif
            </div>
        </div>
        <div class="form-group row flex-center">
            <button type="button" class="btn btn-danger" data-dismiss="modal">{{ __('administration_forms.button_close') }}</button>
            <button id="addTeam-btn" class="btn bg-accent light-text">{{ __('administration_forms.button_add_teams') }}</button>
        </div>
    </div>
</form>
