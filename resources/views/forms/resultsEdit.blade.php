<div class="card">
    <div class="card-body">
        <div class="row">
            <div class="col-md-3">
                @if(!isset($availableDrivers) or count($availableDrivers)==0 )
                    <div class="d-flex justify-content-between bg-accent-lighter light-text p-3">
                        Additional drivers
                    </div>
                    <ul id="availableDrivers" class="list-group">
                    </ul>
                @else
                    <div class="d-flex justify-content-between bg-accent-lighter light-text p-3">
                        Additional drivers
                    </div>
                    <ul id="availableDrivers" class="list-group">
                        @foreach ($availableDrivers as $availableDriver)
                            <li id="availableDriver-{{ $availableDriver->id }}" class="list-group-item">
                                <div class="d-flex justify-content-between">
                                    <div>
                                        {{ $availableDriver->name }}
                                    </div>
                                    @if(!is_null($availableDriver->team))
                                        @if (!is_null($availableDriver->team->secondaryColour1) and !is_null($availableDriver->team->secondaryColour2))
                                            <div style="height: 25px;width: 60px;background: linear-gradient(110deg, {{ $availableDriver->team->primaryColour }} 50%, {{ $availableDriver->team->secondaryColour1 }} 75%, {{ $availableDriver->team->secondaryColour2 }} 100%);"></div>
                                        @elseif (!is_null($availableDriver->team->secondaryColour1) and is_null($availableDriver->team->secondaryColour2))
                                            <div style="height: 25px;width: 60px;background: linear-gradient(110deg, {{ $availableDriver->team->primaryColour }} 50%, {{ $availableDriver->team->secondaryColour1 }} 75%, {{ $availableDriver->team->secondaryColour1 }} 100%);"></div>
                                        @elseif (is_null($availableDriver->team->secondaryColour1) and !is_null($availableDriver->team->secondaryColour2))
                                            <div style="height: 25px;width: 60px;background: linear-gradient(110deg, {{ $availableDriver->team->primaryColour }} 50%, {{ $availableDriver->team->secondaryColour2 }} 75%, {{ $availableDriver->team->secondaryColour2 }} 100%);"></div>
                                        @else
                                            <div style="height: 25px;width: 60px;background: linear-gradient(110deg, {{ $availableDriver->team->primaryColour }} 50%, {{ $availableDriver->team->primaryColour }} 75%, {{ $availableDriver->team->primaryColour }} 100%);"></div>
                                        @endif
                                    @else
                                        <div class="team-colour"></div>
                                    @endif
                                    <button type="button" onclick="addToResults(this)" class="btn bg-accent light-text btn-sm"
                                            data-driverid="{{ $availableDriver->id }}"
                                            data-drivername="{{ $availableDriver->name }}"
                                            data-driverteam="{{ is_null($availableDriver->team) ? 0 : $availableDriver->team->name }}"
                                            data-c1="{{ is_null($availableDriver->team) ? 0 : $availableDriver->team->primaryColour }}"
                                            data-c2="{{ is_null($availableDriver->team) ? 0 : $availableDriver->team->secondaryColour1 }}"
                                            data-c3="{{ is_null($availableDriver->team) ? 0 : $availableDriver->team->secondaryColour2 }}"
                                            data-max="{{ $splitInfo->max_per_race }}"><i class="fas fa-plus"></i></button>
                                </div>
                            </li>
                        @endforeach
                    </ul>
                @endif
            </div>
            <div class="col-md-9">
                <div class="d-flex justify-content-between bg-accent-lighter light-text p-3">
                    <div class="col-md-1">
                        {{ __('administration.results_position') }}
                    </div>
                    <div class="col-md-2">
                        {{ __('administration.results_driver') }}
                    </div>
                    <div class="col-md-2">
                        {{ __('administration.results_team') }}
                    </div>
                    <div class="col-md-1">
                        {{ __('administration.results_class') }}
                    </div>
                    <div class="col-md-2">
                        {{ __('administration.results_additional') }}
                    </div>
                    <div class="col-md-1 text-md-right">
                        {{ __('administration.results_points') }}
                    </div>
                    <div class="col-md-2">
                        {{ __('administration.results_bonus_points') }}
                    </div>
                    <div class="col-md-1 text-md-right">
                    </div>
                </div>
                <form id="resultsEditForm" method="POST" action="{{ route('results.update', [urlencode($league), $splitId]) }}">
                    @csrf
                    <input type="hidden" name="idRace" value="{{ $currentRace->id }}">
                    <ul class="list-group" id="results">
                        @foreach($results as $result)
                            <li id="resultsDriver-{{ $result->driver->id }}" class="list-group-item" draggable="true">
                                <div class="d-flex justify-content-between">
                                    <div class="col-md-1">
                                        {{ $result->position }}
                                    </div>
                                    <div class="col-md-2">
                                        {{ $result->driver->name }}
                                    </div>
                                    <div class="col-md-2">
                                        @if (!is_null($result->team))
                                            {{ $result->team->name }}
                                        @else
                                            {{ __('administration.results_no_team') }}
                                        @endif
                                    </div>
                                    <div class="col-md-1">
                                        @if(is_null($result->driver->idClass))
                                            {{ __('administration.results_no_class') }}
                                        @else
                                            {{ $result->driver->driverClass->name }}
                                        @endif
                                    </div>
                                    <div class="col-md-2">
                                        <div class="custom-control custom-checkbox">
                                            <input type="checkbox" name="dnf[{{ $result->driver->id }}]"
                                                   class="custom-control-input" id="dnf{{ $result->driver->id }}"
                                                    {{ $result->didNotFinish ? 'checked="checked"' : '' }}>
                                            <label class="custom-control-label" for="dnf{{ $result->driver->id }}">DNF</label>
                                        </div>
                                    </div>
                                    <div class="col-md-1 text-md-right">
                                        {{ $result->points . '/' . (is_null($result->classPoints) ? '-' : $result->classPoints) }}
                                    </div>
                                    <div class="col-md-2">
                                        <div class="input-group mb-3 bonuspoints-edit">
                                           <input type="number" name="bonusPoints[{{ $result->driver->id }}]" class="form-control form-control-sm  bonuspoints-input"
                                                 min="-999" max="999" aria-label="bonus-points" value="{{ is_null($result->bonusPoints) ? 0 : $result->bonusPoints }}">
                                       </div>
                                    </div>
                                    <div class="col-md-1 text-md-right">
                                        <button type="button" onclick="removeFromResults(this)" class="btn bg-accent light-text btn-sm"
                                                data-driverid="{{ $result->driver->id }}"
                                                data-drivername="{{ $result->driver->name }}"
                                                data-driverteam="{{ is_null($result->driver->team) ? 0 : $result->driver->team->name }}"
                                                data-c1="{{ is_null($result->driver->team) ? 0 : $result->driver->team->primaryColour }}"
                                                data-c2="{{ is_null($result->driver->team) ? 0 : $result->driver->team->secondaryColour1 }}"
                                                data-c3="{{ is_null($result->driver->team) ? 0 : $result->driver->team->secondaryColour2 }}"
                                                data-max="{{ $splitInfo->max_per_race }}"><i class="fas fa-minus"></i></button>
                                    </div>
                                </div>
                                <input type="hidden" name="results[]" value="{{ $result->driver->id }}">
                            </li>
                        @endforeach
                    </ul>
                </form>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="d-flex justify-content-center">
                    <button id="saveChanges-Btn" type="button" class="btn bg-accent light-text">
                        {{ __('administration_forms.button_save_changes') }}
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>
