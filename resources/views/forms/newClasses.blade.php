<form>
    @csrf
    <div class="modal-body bg-main light-text">
        <div id="error-ac" class="alert alert-danger" style="display:none"></div>
        <div id="success-ac" class="alert alert-success" style="display:none"></div>
        <div class="form-group row">
            <label for="drivers" class="col-md-4 col-form-label text-md-right light-text">{{ __('administration_forms.class_names') }}</label>

            <div class="col-md-6">
                    <textarea id="classes" class="form-control{{ $errors->has('classes') ? ' is-invalid' : '' }}"
                              name="classes" rows="5" placeholder="{{ __('administration_forms.tip_classes') }}" required>{{ old('classes') }}</textarea>

                @if ($errors->has('classes'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('classes') }}</strong>
                    </span>
                @endif
            </div>
        </div>
        <div class="form-group row flex-center">
            <button type="button" class="btn btn-danger" data-dismiss="modal">{{ __('administration_forms.button_close') }}</button>
            <button id="addClass-btn" class="btn bg-accent light-text">{{ __('administration_forms.button_add_classes') }}</button>
        </div>
    </div>
</form>
