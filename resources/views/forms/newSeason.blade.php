<form>
    @csrf
    <div class="flex-center mb-4">
    <div class="col-md-6">
        <div class="custom-control custom-checkbox">
            <input name="transferDrivers" type="checkbox" class="custom-control-input" id="transferDrivers">
            <label class="custom-control-label light-text" for="transferDrivers">{{ __('administration_forms.season_drivers') }}</label>
        </div>
        <div class="custom-control custom-checkbox">
            <input name="transferTeams" type="checkbox" class="custom-control-input" id="transferTeams">
            <label class="custom-control-label light-text" for="transferTeams">{{ __('administration_forms.season_teams') }}</label>
        </div>
    </div>
    </div>
</form>
