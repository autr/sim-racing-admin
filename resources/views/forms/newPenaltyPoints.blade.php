@if(!isset($drivers) or count($drivers) == 0 or !isset($racesPP) or count($racesPP) == 0)
    {{ __('administration.penalties_cant_add_points') }}
@else
    <form>
        @csrf
        <div class="modal-body bg-main light-text">
            <div id="error-pp" class="alert alert-danger" style="display:none"></div>
            <div id="success-pp" class="alert alert-success" style="display:none"></div>
            <div class="form-group row">
                <div class="col-md-4">
                    <label for="driverSelectionPP" class="col-form-label">{{ __('administration_forms.penalty_driver') }}</label>
                    <select id="driverSelectionPP" type="text"
                            class="form-control{{ $errors->has('driverSelectionPP') ? ' is-invalid' : '' }}"
                            name="driverSelectionPP" required>
                        @foreach($drivers as $driver)
                            <option value="{{ $driver->id }}" {{ $driver->id == old('driverSelectionPP') ? 'selected' : '' }}>
                                {{ $driver->name }}</option>
                        @endforeach
                    </select>
                    @if ($errors->has('driverSelectionPP'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('driverSelectionPP') }}</strong>
                        </span>
                    @endif
                </div>
                <div class="col-md-4">
                    <label for="raceSelectionPP" class="col-form-label">{{ __('administration_forms.penalty_points_race') }}</label>
                    <select id="raceSelectionPP" type="text"
                            class="form-control{{ $errors->has('raceSelectionPP') ? ' is-invalid' : '' }}"
                            name="raceSelectionPP" required>
                        @foreach($racesPP as $racePP)
                            <option value="{{ $racePP->id }}" {{ $racePP->id == old('raceSelectionPP') ? 'selected' : '' }}>
                                {{ $racePP->track->name }}</option>
                        @endforeach
                    </select>
                    @if ($errors->has('raceSelectionPP'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('raceSelectionPP') }}</strong>
                        </span>
                    @endif
                </div>
                <div class="col-md-4">
                    <label for="amountPP" class="col-form-label">{{ __('administration_forms.penalty_amount') }}</label>
                    <input id="amountPP" type="text" class="form-control{{ $errors->has('amountPP') ? ' is-invalid' : '' }}"
                           name="amountPP" value="{{ old('amountPP') }}" required>
                    @if ($errors->has('amountPP'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('amountPP') }}</strong>
                        </span>
                    @endif
                </div>
            </div>
            <div class="form-group row flex-center">
                <button type="button" class="btn btn-danger" data-dismiss="modal">{{ __('administration_forms.button_close') }}</button>
                <button id="addPenaltyPoints-btn" class="btn bg-accent light-text"
                    {{ (!isset($drivers) or count($drivers) == 0 or !isset($racesPP) or count($racesPP) == 0) ? 'disabled' : '' }}>{{ __('administration_forms.button_add_penalty_points') }}</button>
            </div>
        </div>
    </form>
@endif
