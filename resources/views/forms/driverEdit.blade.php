<form>
    @csrf
    <input id="idDriver-u" type="hidden" name="idDriver" value="">
    <div class="modal-body bg-main light-text">
        <div id="error-u" class="alert alert-danger" style="display:none"></div>
        <div id="success-u" class="alert alert-success" style="display:none"></div>
        <div class="form-group row">
            <label for="driverName" class="col-md-4 col-form-label text-md-right light-text">{{ __('administration_forms.other_name') }}</label>

            <div class="col-md-6">
                <input id="driverName" type="text"
                       class="form-control"
                       name="driverName" value="" required>
            </div>
        </div>
        <div class="form-group row">
            <label for="driverTeam" class="col-md-4 col-form-label text-md-right light-text">{{ __('administration_forms.driver_team') }}</label>

            <div class="col-md-6">
                <select id="driverTeam"
                        class="form-control"
                        name="driverTeam" required>
                    <option value="-1">
                        {{ __('administration_forms.driver_no_team') }}</option>
                    @foreach($teams as $team)
                        <option value="{{ $team->id }}">
                            {{ $team->name }}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <div class="form-group row">
            <label for="driverActive" class="col-md-4 col-form-label text-md-right light-text">{{ __('administration_forms.driver_active') }}</label>

            <div class="col-md-6">
                <div class="custom-control custom-checkbox">
                    <input id="driverActive" type="checkbox"
                           class="custom-control-input"
                           name="driverActive">
                    <label class="custom-control-label" for="driverActive"></label>
                </div>
            </div>
        </div>
        <div class="form-group row">
            <label for="driverClass" class="col-md-4 col-form-label text-md-right light-text">{{ __('administration_forms.driver_class') }}</label>

            <div class="col-md-6">
                <select id="driverClass"
                        class="form-control"
                        name="driverClass" required>
                    <option value="-1">
                        {{ __('administration_forms.driver_no_class') }}</option>
                    @foreach($classes as $class)
                        <option value="{{ $class->id }}">
                            {{ $class->name }}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <div class="form-group row flex-center">
            <button type="button" class="btn btn-danger" data-dismiss="modal">{{ __('administration_forms.button_close') }}</button>
            <button id="updateDriver-btn" class="btn bg-accent light-text">{{ __('administration_forms.button_update') }}</button>
        </div>
    </div>
</form>
