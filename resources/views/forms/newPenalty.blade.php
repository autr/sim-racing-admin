@if(!isset($drivers) or count($drivers) == 0 or !isset($races) or count($races) == 0)
    {{ __('administration.penalties_cant_issue') }}
@else
    <form>
        @csrf
        <div class="modal-body bg-main light-text">
            <div id="error" class="alert alert-danger" style="display:none"></div>
            <div id="success" class="alert alert-success" style="display:none"></div>
            <div class="form-group row">
                <div class="col-md-3">
                    <label for="driverSelection" class="col-form-label">{{ __('administration_forms.penalty_driver') }}</label>
                    <select id="driverSelection"
                            class="form-control{{ $errors->has('driverSelection') ? ' is-invalid' : '' }}"
                            name="driverSelection" required>
                        @foreach($drivers as $driver)
                            <option value="{{ $driver->id }}" {{ $driver->id == old('driverSelection') ? 'selected' : '' }}>
                                {{ $driver->name }}</option>
                        @endforeach
                    </select>
                    @if ($errors->has('driverSelection'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('driverSelection') }}</strong>
                        </span>
                    @endif
                </div>
                <div class="col-md-3">
                    <label for="raceSelection" class="col-form-label">{{ __('administration_forms.penalty_race') }}</label>
                    <select id="raceSelection"
                            class="form-control{{ $errors->has('raceSelection') ? ' is-invalid' : '' }}"
                            name="raceSelection" required>
                        @foreach($races as $race)
                            <option value="{{ $race->id }}" {{ $race->id == old('raceSelection') ? 'selected' : '' }}>
                                {{ $race->track->name }}</option>
                        @endforeach
                    </select>
                    @if ($errors->has('raceSelection'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('raceSelection') }}</strong>
                        </span>
                    @endif
                </div>
                <div class="col-md-3">
                    <label for="typeSelection" class="col-form-label">{{ __('administration_forms.penalty_type') }}</label>
                    <select id="typeSelection"
                            class="form-control{{ $errors->has('typeSelection') ? ' is-invalid' : '' }}"
                            name="typeSelection" required>
                        @foreach($penaltyTypes as $penaltyType)
                            <option value="{{ $penaltyType->id }}" {{ $penaltyType->id == old('typeSelection') ? 'selected' : '' }}>
                                {{ $penaltyType->name }}</option>
                        @endforeach
                    </select>
                    @if ($errors->has('typeSelection'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('typeSelection') }}</strong>
                        </span>
                    @endif
                </div>
                <div class="col-md-3">
                    <label for="amount" class="col-form-label">{{ __('administration_forms.penalty_amount') }}</label>
                    <input id="amount" type="text" class="form-control{{ $errors->has('amount') ? ' is-invalid' : '' }}"
                           name="amount" value="{{ old('amount') }}" required>
                    @if ($errors->has('amount'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('amount') }}</strong>
                        </span>
                    @endif
                </div>
            </div>
            <div class="form-group row flex-center">
                <button type="button" class="btn btn-danger" data-dismiss="modal">{{ __('administration_forms.button_close') }}</button>
                <button id="issuePenalty-btn" class="btn bg-accent light-text"
                    {{ (!isset($drivers) or count($drivers) == 0 or !isset($races) or count($races) == 0) ? 'disabled' : '' }}>{{ __('administration_forms.button_add_penalty') }}</button>
            </div>
        </div>
    </form>
@endif
