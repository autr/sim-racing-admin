<form>
    @csrf
    <input id="idTeam-u" type="hidden" name="idTeam" value="">
    <div class="modal-body bg-main light-text">
        <div id="error-u" class="alert alert-danger" style="display:none"></div>
        <div id="success-u" class="alert alert-success" style="display:none"></div>
        <div class="form-group row">
            <label for="teamName" class="col-md-4 col-form-label text-md-right light-text">{{ __('administration_forms.other_name') }}</label>

            <div class="col-md-6">
                <input id="teamName" type="text"
                       class="form-control"
                       name="teamName" value="" required>
            </div>
        </div>
        <div class="form-group row">
            <label for="teamPrimary" class="col-md-4 col-form-label text-md-right light-text">{{ __('administration_forms.team_primary') }}</label>

            <div class="col-md-6">
                <input id="teamPrimary" type="color"
                        class="form-control"
                        name="teamPrimary" value="#ffffff" required>
            </div>
        </div>
        <div class="form-group row">
            <label for="teamSecondary1" class="col-md-4 col-form-label text-md-right light-text">{{ __('administration_forms.team_secondary1') }}</label>

            <div class="col-md-1">
                <div class="custom-control custom-checkbox">
                    <input id="teamSecondary1-chbox" type="checkbox"
                           class="custom-control-input"
                           name="teamSecondary1-chbox" value="">
                    <label class="custom-control-label" for="teamSecondary1-chbox"></label>
                </div>
            </div>
            <div class="col-md-5">
                <input id="teamSecondary1" type="color"
                       class="form-control"
                       name="teamSecondary1" value="#ffffff">
            </div>
        </div>
        <div class="form-group row">
            <label for="teamSecondary2" class="col-md-4 col-form-label text-md-right light-text">{{ __('administration_forms.team_secondary2') }}</label>

            <div class="col-md-1">
                <div class="custom-control custom-checkbox">
                    <input id="teamSecondary2-chbox" type="checkbox"
                           class="custom-control-input"
                           name="teamSecondary2-chbox" value="">
                    <label class="custom-control-label" for="teamSecondary2-chbox"></label>
                </div>
            </div>
            <div class="col-md-5">
                <input id="teamSecondary2" type="color"
                       class="form-control"
                       name="teamSecondary2" value="#ffffff">
            </div>
        </div>
        <div class="form-group row flex-center">
            <button type="button" class="btn btn-danger" data-dismiss="modal">{{ __('administration_forms.button_close') }}</button>
            <button id="updateTeam-btn" class="btn bg-accent light-text">{{ __('administration_forms.button_update') }}</button>
        </div>
    </div>
</form>
