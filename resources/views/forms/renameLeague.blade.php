<form>
    @csrf
    <div class="modal-body bg-main light-text">
        <div id="error" class="alert alert-danger" style="display:none"></div>
        <div id="success" class="alert alert-success" style="display:none"></div>
        <div class="form-group row flex-center light-text red">
            {{ __('administration_forms.other_rename_league') }}
        </div>
        <input id="leagueId" type="hidden" name="leagueId" value="{{ $leagueInfo->id }}">
        <div class="form-group row">
            <label for="leagueName" class="col-md-4 col-form-label text-md-right light-text">{{ __('administration_forms.league_name') }}</label>

            <div class="col-md-6">
                <input id="leagueName" type="text" class="form-control" name="leagueName" required>
            </div>
        </div>
        <div class="form-group row flex-center">
            <button type="button" class="btn btn-danger" data-dismiss="modal">{{ __('administration_forms.button_close') }}</button>
            <button id="renameLeague-btn" class="btn bg-accent light-text">{{ __('administration_forms.button_rename_league') }}</button>
        </div>
    </div>
</form>
