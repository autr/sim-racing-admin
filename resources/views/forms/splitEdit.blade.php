<form>
    @csrf
    <div class="modal-body bg-main light-text">
        <div id="error" class="alert alert-danger" style="display:none"></div>
        <div id="success" class="alert alert-success" style="display:none"></div>
        <div class="form-group row">
            <div class="col-md-5 col-form-label text-md-right light-text">{{ __('administration_forms.split_season_current') }}</div>

            <div class="col-md-5 col-form-label light-text">{{ $season }}</div>

            <input id="season" type="hidden" name="season" value="{{ $season }}">
        </div>
        <div class="form-group row">
            <label for="splitName" class="col-md-5 col-form-label text-md-right light-text">{{ __('administration_forms.split_name') }}</label>

            <div class="col-md-5">
                <input id="splitName" type="text" class="form-control" name="splitName" value="{{ $splitInfo->name }}" required>
            </div>
        </div>
        <div class="form-group row">
            <label for="idPlatform" class="col-md-5 col-form-label text-md-right light-text">{{ __('administration_forms.split_platform') }}</label>

            <div class="col-md-5">
                <select id="idPlatform" class="form-control" name="idPlatform" required>
                    @foreach($platforms as $platform)
                        <option value="{{ $platform->id }}"
                            {{ $splitInfo->platform->id === $platform->id ? __('selected') : __('') }}>
                            {{ $platform->name }}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <div class="form-group row">
            <label for="idGame" class="col-md-5 col-form-label text-md-right light-text">{{ __('administration_forms.split_game') }}</label>

            <div class="col-md-5">
                <select id="idGame" class="form-control" name="idGame" required>
                    @foreach($games as $game)
                        <option value="{{ $game->id }}"
                            {{ $splitInfo->game->id === $game->id ? __('selected') : __('') }}>
                            {{ $game->name }}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <div class="form-group row">
            <label for="idRegion" class="col-md-5 col-form-label text-md-right light-text">{{ __('administration_forms.split_region') }}</label>

            <div class="col-md-5">
                <select id="idRegion" class="form-control" name="idRegion" required>
                    @foreach($regions as $region)
                        <option value="{{ $region->id }}"
                            {{ $splitInfo->region->id === $region->id ? __('selected') : __('') }}>
                            {{ $region->name }}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <div class="form-group row">
            <label for="maxPerTeam" class="col-md-5 col-form-label text-md-right light-text">{{ __('administration_forms.split_max_per_team') }}</label>

            <div class="col-md-5">
                <input id="maxPerTeam" type="text" class="form-control"
                       name="maxPerTeam" value="{{ $splitInfo->max_per_team }}" placeholder="{{ __('administration_forms.tip_max_per_team') }}" required>
            </div>
        </div>
        <div class="form-group row">
            <label for="maxPerRace" class="col-md-5 col-form-label text-md-right light-text">{{ __('administration_forms.split_max_per_race') }}</label>

            <div class="col-md-5">
                <input id="maxPerRace" type="text" class="form-control"
                       name="maxPerRace" value="{{ $splitInfo->max_per_race }}" required>
            </div>
        </div>
        <div class="form-group row">
            <label for="points" class="col-md-5 col-form-label text-md-right light-text">{{ __('administration_forms.split_points') }}</label>

            <div class="col-md-5">
                <input id="points" type="text" class="form-control"
                       name="points" value="{{ $splitInfo->points }}" placeholder="{{ __('administration_forms.tip_points') }}" required>
            </div>
        </div>
        <div class="form-group row">
            <label for="noPointsDnf" class="col-md-5 col-form-label text-md-right light-text">{{ __('administration_forms.split_no_points_dnf') }}</label>

            <div class="col-md-5">
                <div class="custom-control custom-checkbox">
                    <input id="noPointsDnf" type="checkbox"
                           class="custom-control-input"
                           name="noPointsDnf" value="1"
                        {{ $splitInfo->no_points_dnf ? ' checked' : '' }}>
                    <label class="custom-control-label" for="noPointsDnf"></label>
                </div>
            </div>
        </div>
        <div class="form-group row">
            <label for="omitInactive" class="col-md-5 col-form-label text-md-right light-text">{{ __('administration_forms.split_omit_inactive') }}</label>

            <div class="col-md-5">
                <div class="custom-control custom-checkbox">
                    <input id="omitInactive" type="checkbox"
                           class="custom-control-input"
                           name="omitInactive" value="1"
                        {{ $splitInfo->omit_inactive ? ' checked' : '' }}>
                    <label class="custom-control-label" for="omitInactive"></label>
                </div>
            </div>
        </div>
        <div class="form-group row flex-center">
            <button type="button" class="btn btn-danger" data-dismiss="modal">{{ __('administration_forms.button_close') }}</button>
            <button id="updateSplit-btn" class="btn bg-accent light-text">{{ __('administration_forms.button_update') }}</button>
        </div>
    </div>
</form>
