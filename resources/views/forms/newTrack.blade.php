<form>
    @csrf
    <div class="modal-body bg-main light-text">
        <div id="error" class="alert alert-danger" style="display:none"></div>
        <div id="success" class="alert alert-success" style="display:none"></div>
        <div class="form-group row">
            <div class="col-md-6">
                <label for="trackName" class="col-form-label light-text">{{ __('administration_forms.other_name') }}</label>
                <input id="trackName" type="text"
                       class="form-control" name="trackName" required>
            </div>
            <div class="col-md-6">
                <label for="trackCountry" class="col-form-label light-text">{{ __('administration_forms.calendar_country') }}</label>
                <select id="trackCountry"
                        class="form-control"
                        name="trackCountry" required>
                    @foreach($countries as $country)
                        <option value="{{ $country->code }}">{{ $country->name }}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <div class="form-group row flex-center">
            <button type="button" class="btn btn-danger" data-dismiss="modal">{{ __('administration_forms.button_close') }}</button>
            <button id="newTrack-btn" class="btn bg-accent light-text">{{ __('administration_forms.button_add_track') }}</button>
        </div>
    </div>
</form>
