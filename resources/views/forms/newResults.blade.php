<div class="row mb-4">
    <div class="col-md">
        <div class="card h-100">
            <div class="card-header">
                {{ __('administration.results_available_drivers') }}
            </div>
            <div class="card-body">
                @if(!isset($availableDrivers) or count($availableDrivers)==0 )
                    {{ __('administration.results_no_drivers') }}
                @else
                    <ul id="availableDrivers" class="list-group">
                        @foreach ($availableDrivers as $availableDriver)
                            <li id="availableDriver-{{ $availableDriver->id }}" class="list-group-item">
                                <div class="d-flex justify-content-between">
                                    <div>
                                        {{ $availableDriver->name }}
                                    </div>
                                    @if(!is_null($availableDriver->team))
                                        @if (!is_null($availableDriver->team->secondaryColour1) and !is_null($availableDriver->team->secondaryColour2))
                                            <div style="height: 25px;width: 60px;background: linear-gradient(110deg, {{ $availableDriver->team->primaryColour }} 50%, {{ $availableDriver->team->secondaryColour1 }} 75%, {{ $availableDriver->team->secondaryColour2 }} 100%);"></div>
                                        @elseif (!is_null($availableDriver->team->secondaryColour1) and is_null($availableDriver->team->secondaryColour2))
                                            <div style="height: 25px;width: 60px;background: linear-gradient(110deg, {{ $availableDriver->team->primaryColour }} 50%, {{ $availableDriver->team->secondaryColour1 }} 75%, {{ $availableDriver->team->secondaryColour1 }} 100%);"></div>
                                        @elseif (is_null($availableDriver->team->secondaryColour1) and !is_null($availableDriver->team->secondaryColour2))
                                            <div style="height: 25px;width: 60px;background: linear-gradient(110deg, {{ $availableDriver->team->primaryColour }} 50%, {{ $availableDriver->team->secondaryColour2 }} 75%, {{ $availableDriver->team->secondaryColour2 }} 100%);"></div>
                                        @else
                                            <div style="height: 25px;width: 60px;background: linear-gradient(110deg, {{ $availableDriver->team->primaryColour }} 50%, {{ $availableDriver->team->primaryColour }} 75%, {{ $availableDriver->team->primaryColour }} 100%);"></div>
                                        @endif
                                    @else
                                        <div class="team-colour"></div>
                                    @endif
                                    <button type="button" onclick="addToResults(this)" class="btn bg-accent light-text btn-sm"
                                            data-driverid="{{ $availableDriver->id }}"
                                            data-drivername="{{ $availableDriver->name }}"
                                            data-driverteam="{{ is_null($availableDriver->team) ? 0 : $availableDriver->team->name }}"
                                            data-c1="{{ is_null($availableDriver->team) ? 0 : $availableDriver->team->primaryColour }}"
                                            data-c2="{{ is_null($availableDriver->team) ? 0 : $availableDriver->team->secondaryColour1 }}"
                                            data-c3="{{ is_null($availableDriver->team) ? 0 : $availableDriver->team->secondaryColour2 }}"
                                            data-max="{{ $splitInfo->max_per_race }}"><i class="fas fa-plus"></i></button>
                                </div>
                            </li>
                        @endforeach
                    </ul>
                @endif
            </div>
        </div>
    </div>
    <div class="col-md">
        <div class="card h-100">
            <div class="card-header">
                {{ __('administration.results') }}
            </div>
            <div class="card-body">
                <form id="resultsForm" method="POST" action="{{ route('results.store', [urlencode($league), $splitId]) }}">
                    @csrf
                    <input type="hidden" name="idRace" value="{{ $currentRace->id }}">
                    <ul class="list-group" id="results">
                    </ul>
                </form>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="card h-100">
            <div class="card-body d-flex justify-content-center">
                <button id="saveResults-Btn" type="button" class="btn bg-accent light-text">
                    {{ __('administration_forms.button_save_results') }}
                </button>
            </div>
        </div>
    </div>
</div>
