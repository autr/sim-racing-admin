<div class="form-group row">
    <label for="splitName" class="col-md-4 col-form-label text-md-right">{{ __('administration_forms.split_name') }}</label>

    <div class="col-md-6">
        <input id="splitName" type="text" class="form-control{{ $errors->has('splitName') ? ' is-invalid' : '' }}"
               name="splitName" value="{{ old('splitName') }}" required>

        @if ($errors->has('splitName'))
            <span class="invalid-feedback" role="alert">
                <strong>{{ $errors->first('splitName') }}</strong>
            </span>
        @endif
    </div>
</div>
<div class="form-group row">
    <label for="idPlatform" class="col-md-4 col-form-label text-md-right">{{ __('administration_forms.split_platform') }}</label>

    <div class="col-md-6">
        <select id="idPlatform" class="form-control{{ $errors->has('idPlatform') ? ' is-invalid' : '' }}"
                name="idPlatform" required>
            @foreach($platforms as $platform)
                <option value="{{ $platform->id }}"
                        {{ old('idPlatform') == $platform->id ? __('selected') : __('') }}>
                    {{ $platform->name }}</option>
            @endforeach
        </select>
        @if ($errors->has('idPlatform'))
            <span class="invalid-feedback" role="alert">
                <strong>{{ $errors->first('idPlatform') }}</strong>
            </span>
        @endif
    </div>
</div>
<div class="form-group row">
    <label for="idGame" class="col-md-4 col-form-label text-md-right">{{ __('administration_forms.split_game') }}</label>

    <div class="col-md-6">
        <select id="idGame" class="form-control{{ $errors->has('idGame') ? ' is-invalid' : '' }}"
                name="idGame" required>
            @foreach($games as $game)
                <option value="{{ $game->id }}"
                        {{ old('idGame') == $game->id ? __('selected') : __('') }}>
                    {{ $game->name }}</option>
            @endforeach
        </select>
        @if ($errors->has('idGame'))
            <span class="invalid-feedback" role="alert">
                <strong>{{ $errors->first('idGame') }}</strong>
            </span>
        @endif
    </div>
</div>
<div class="form-group row">
    <label for="idRegion" class="col-md-4 col-form-label text-md-right">{{ __('administration_forms.split_region') }}</label>

    <div class="col-md-6">
        <select id="idRegion" class="form-control{{ $errors->has('idRegion') ? ' is-invalid' : '' }}"
                name="idRegion" required>
            @foreach($regions as $region)
                <option value="{{ $region->id }}"
                        {{ old('idRegion') == $region->id ? __('selected') : __('') }}>
                    {{ $region->name }}</option>
            @endforeach
        </select>
        @if ($errors->has('idRegion'))
            <span class="invalid-feedback" role="alert">
                <strong>{{ $errors->first('idRegion') }}</strong>
            </span>
        @endif
    </div>
</div>
<div class="form-group row">
    <label for="maxPerTeam" class="col-md-4 col-form-label text-md-right">{{ __('administration_forms.split_max_per_team') }}</label>

    <div class="col-md-6">
        <input id="maxPerTeam" type="text" class="form-control{{ $errors->has('maxPerTeam') ? ' is-invalid' : '' }}"
               name="maxPerTeam" value="{{ old('maxPerTeam') }}" placeholder="{{ __('administration_forms.tip_max_per_team') }}" required>

        @if ($errors->has('maxPerTeam'))
            <span class="invalid-feedback" role="alert">
                <strong>{{ $errors->first('maxPerTeam') }}</strong>
            </span>
        @endif
    </div>
</div>
<div class="form-group row">
    <label for="maxPerRace" class="col-md-4 col-form-label text-md-right">{{ __('administration_forms.split_max_per_race') }}</label>

    <div class="col-md-6">
        <input id="maxPerRace" type="text" class="form-control{{ $errors->has('maxPerRace') ? ' is-invalid' : '' }}"
               name="maxPerRace" value="{{ old('maxPerRace') }}" required>

        @if ($errors->has('maxPerRace'))
            <span class="invalid-feedback" role="alert">
                <strong>{{ $errors->first('maxPerRace') }}</strong>
            </span>
        @endif
    </div>
</div>
<div class="form-group row">
    <label for="points" class="col-md-4 col-form-label text-md-right">{{ __('administration_forms.split_points') }}</label>

    <div class="col-md-6">
        <input id="points" type="text" class="form-control{{ $errors->has('points') ? ' is-invalid' : '' }}"
               name="points" value="{{ old('points') }}" placeholder="{{ __('administration_forms.tip_points') }}" required>

        @if ($errors->has('points'))
            <span class="invalid-feedback" role="alert">
                <strong>{{ $errors->first('points') }}</strong>
            </span>
        @endif
    </div>
</div>
<div class="form-group row">
    <label for="season" class="col-md-4 col-form-label text-md-right">{{ __('administration_forms.split_season') }}</label>

    <div class="col-md-6">
        <input id="season" type="text" class="form-control{{ $errors->has('season') ? ' is-invalid' : '' }}"
               name="season" value="{{ old('season') }}" required>

        @if ($errors->has('season'))
            <span class="invalid-feedback" role="alert">
                <strong>{{ $errors->first('season') }}</strong>
            </span>
        @endif
    </div>
</div>
<div class="form-group row">
    <label for="noPointsDnf" class="col-md-4 col-form-label text-md-right light-text">{{ __('administration_forms.split_no_points_dnf') }}</label>

    <div class="col-md-6">
        <div class="custom-control custom-checkbox">
            <input id="noPointsDnf" type="checkbox"
                   class="custom-control-input"
                   name="noPointsDnf" value="1"
                {{ old('noPointsDnf') == 1 ? ' checked' : '' }}>
            <label class="custom-control-label" for="noPointsDnf"></label>
        </div>
        @if ($errors->has('noPointsDnf'))
            <span class="invalid-feedback" role="alert">
                <strong>{{ $errors->first('noPointsDnf') }}</strong>
            </span>
        @endif
    </div>
</div>
<div class="form-group row">
    <label for="omitInactive" class="col-md-4 col-form-label text-md-right light-text">{{ __('administration_forms.split_omit_inactive') }}</label>

    <div class="col-md-6">
        <div class="custom-control custom-checkbox">
            <input id="omitInactive" type="checkbox"
                   class="custom-control-input"
                   name="omitInactive" value="1"
                {{ old('omitInactive') == 1 ? ' checked' : '' }}>
            <label class="custom-control-label" for="omitInactive"></label>
        </div>
        @if ($errors->has('omitInactive'))
            <span class="invalid-feedback" role="alert">
                <strong>{{ $errors->first('omitInactive') }}</strong>
            </span>
        @endif
    </div>
</div>
