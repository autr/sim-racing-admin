<form>
    @csrf
    <div class="modal-body bg-main light-text">
        <div id="error-m" class="alert alert-danger" style="display:none"></div>
        <div id="success-m" class="alert alert-success" style="display:none"></div>
        <div class="form-group row">
            <div class="col-md-4">
                <label for="trackCountrySelection" class="col-form-label">{{ __('administration_forms.calendar_country') }}</label>
                <select id="trackCountrySelection"
                                 class="form-control"
                                 name="trackCountrySelection" required>
                    <option value="all">
                        {{ __('administration_forms.calendar_country_all') }}</option>

                    @foreach($trackCountries as $trackCountry)
                        <option value="{{ $trackCountry->code }}">
                            {{ $trackCountry->name }}</option>
                    @endforeach
                </select>
            </div>
            <div class="col-md-8">
                <label for="trackSelection" class="col-form-label">{{ __('administration_forms.calendar_tracks') }}</label>
                <select id="trackSelection"
                        class="form-control"
                        name="trackSelection[]" size="7" multiple required>
                    @foreach($tracks as $track)
                        <option value="{{ $track->id }}">
                            {{ $track->name.', '.(!is_null($track->country) ? $track->country->code : 'Unknown') }}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <div class="form-group row flex-center">
            <button type="button" class="btn btn-danger" data-dismiss="modal">{{ __('administration_forms.button_close') }}</button>
            <button id="addTracks-btn" class="btn bg-accent light-text">{{ __('administration_forms.button_add_tracks') }}</button>
        </div>
    </div>
</form>
