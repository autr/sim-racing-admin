@extends('layouts.app')

@section('title')
    {{ __('Home') }}
@endsection
@section('navigation')
    @include('nav.main-nav')
@endsection
@section('content')
    <div class="col-md-12 mb-4">
        @if(session()->has('errorMessage'))
            <div class="alert alert-danger">
                {{ session()->get('errorMessage') }}
            </div>
        @endif
        @if(session()->has('message'))
            <div class="alert alert-success">
                {{ session()->get('message') }}
            </div>
        @endif
        <div class="card">
            @php
                $news = false;
            @endphp
            @if (isset($requests) and count($requests) > 0)
                @php
                    $news = true;
                @endphp
            <div class="card-header">{{ __('Unresolved requests:') }}</div>

            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-hover table-borderless table-sm light-text">
                        <thead class="bg-accent-lighter light-text">
                        <tr>
                            <th scope="col">{{ __('League') }}</th>
                            <th scope="col">{{ __('Split') }}</th>
                            <th scope="col">{{ __('User') }}</th>
                            <th scope="col"></th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($requests as $request)
                            <tr>
                                <td>{{ $request->split->league->name }}</td>
                                <td>{{ $request->split->name }}</td>
                                <td>{{ $request->user->name }}</td>
                                <td class="text-right">
                                    <a href="{{ url('/approve/'.$request->id) }}" class="btn bg-accent light-text btn-sm" role="button">Approve</a>
                                    <a href="{{ url('/disapprove/'.$request->id) }}" class="btn btn-danger btn-sm" role="button">Disapprove</a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
            @endif
            @if(Auth::check() and !Auth::user()->newsDismissed)
                    @php
                        $news = true;
                    @endphp
                <div class="card-header">{{ __('New version') }}</div>

                <div class="card-body mb-4">
                    {{ __('A new version has been released since you\'ve been here. Check the ') }}
                    <a class="bg-accent-lighter light-text" href="https://gitlab.com/autr/sim-racing-admin/-/releases" target="_blank">changelog</a>
                    {{ __(' to learn more.') }}
                    <a href="{{ url('/dismiss-news') }}" class="btn bg-accent light-text btn-sm" role="button">Dismiss</a>
                </div>
            @endif
            @if (!$news)
                <div class="card-header">{{ __('News:') }}</div>

                <div class="card-body mb-4">
                    {{ __('No news') }}
                </div>
            @endif
        </div>
    </div>
@endsection
