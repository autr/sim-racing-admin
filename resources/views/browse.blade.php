@extends('layouts.app')

@section('title')
    {{ __('Browse') }}
@endsection
@section('navigation')
    @include('nav.main-nav')
@endsection
@section('content')
    <div class="col-md-12 mb-4">
        <div class="card">
            <div class="card-body">
                <div class="md-form mt-0">
                    <form class="form-inline mr-auto" action="{{ route('browse.search') }}" method="get">
                        <input id="searchBar" class="form-control col-lg-4 w-100" type="text" name="q" placeholder="Search" aria-label="Search">
                        <button class="btn bg-accent light-text btn-sm col-lg-1" type="submit"><i class="fas fa-search"></i></button>
                    </form>
                </div>
                @if (!isset($splits) or count($splits) == 0)
                    <div>
                        {{ __('There are currently no leagues that meet given criteria') }}
                    </div>
                @else
                    @if(session()->has('errorMessage'))
                        <div class="alert alert-danger">
                            {{ session()->get('errorMessage') }}
                        </div>
                    @endif
                    @if(session()->has('message'))
                        <div class="alert alert-success">
                            {{ session()->get('message') }}
                        </div>
                    @endif
                    <div class="table-responsive">
                        <table class="table table-hover table-borderless table-sm light-text">
                            <thead class="bg-accent-lighter light-text">
                            <tr>
                                <th scope="col">{{ __('League') }}</th>
                                <th scope="col">{{ __('Split') }}</th>
                                <th scope="col">{{ __('Game') }}</th>
                                <th scope="col">{{ __('Platform') }}</th>
                                <th scope="col">{{ __('Region') }}</th>
                                <th scope="col"></th>
                            </tr>
                            </thead>
                            <tbody id="searched">
                            @foreach($splits as $split)
                                <tr>
                                    <td title="{{ $split->league->description }}">{{ $split->league->name }}</td>
                                    <td>{{ $split->name }}</td>
                                    <td>{{ $split->game->name }}</td>
                                    <td>{{ $split->platform->name }}</td>
                                    <td>{{ $split->region->name }}</td>
                                    <td class="text-right">
                                        <a href="{{ url('/v/'.urlencode($split->league->uri).'/'.$split->id) }}" class="btn bg-accent light-text btn-sm" role="button">View</a>
                                        @auth
                                            <a href="{{ url('/request/'.$split->id) }}" class="btn bg-accent light-text btn-sm" role="button">Request permission</a>
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    {{ $splits->appends($_GET)->links() }}
                @endif
            </div>
        </div>
    </div>
@endsection
