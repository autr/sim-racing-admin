@extends('presentation.presentation')
@section('presentation-part')
    <div class="card">
        <div class="card-body">
        <h3>Driver standings
            @if (Auth::check())
            <button id="click-scr-standings" class="btn light-text btn-bigger-font btn-sm"><i class="fas fa-camera"></i></button>
            @endif
        </h3>

        <form method="get">
            <div class="row mb-4">
                <div class="col-md-12">
                    <div class="card h-100">
                        <div class="card-body">
                            <div class="d-flex justify-content-start align-items-center">
                                <label for="season" class="col-form-label text-md-right light-text">{{ __('Season') }}</label>
                                <select id="season" name="season" class="col-md-3 browser-default custom-select mb-2 mt-2 mr-2 ml-2">
                                    @foreach($seasons as $season)
                                        <option value="{{ $season->id }}"
                                            {{ $selectedSeason == $season->id ? __('selected') : __('') }}>{{ 'Season ' . $season->season }}</option>
                                    @endforeach
                                </select>
                                <button type="submit" class="btn bg-accent light-text">{{ __('Filter') }}</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>

        <div class="table-responsive text-center">
            <div id="driver-standings-scr">
            <table class="table table-borderless table-sm light-text results-table">
                <thead class="bg-accent-lighter light-text">
                <tr>
                    <th class="bg-lighter" scope="col"></th>
                    <th class="th-sm bg-main sticky-col-driver" scope="col"></th>
                    <th class="th-sm bg-lighter" scope="col"></th>
                    @foreach($calendar as $race)
                        <th class="th-sm" scope="col" title="{{ $race->track->name }}">{{ $race->track->idCountry }}</th>
                    @endforeach
                    <th class="th-sm drivers-pts-col sticky-col-pts" scope="col">Pts</th>
                </tr>
                </thead>
                <tbody>
                @php
                    $position = 1;
                @endphp
                @foreach($results as $result)
                    <tr>
                        <th scope="row">{{ $position }}</th>
                        <td class="sticky-col-driver text-left">
                            {{ $result['driver'] }}
                        </td>
                        <td>
                            @if(!is_null($result['team']))
                                @include('teamColour', ['team' => $result['team']])
                            @endif
                        </td>
                        @foreach($calendar as $race)
                            @if($result['races'][$race->id] == '1' or $result['races'][$race->id] == '1+')
                                <td class="results-first" title="{{ $race->track->idCountry.' - '.$race->track->name }}">{{ $result['races'][$race->id] }}</td>
                            @elseif($result['races'][$race->id] == '2' or $result['races'][$race->id] == '2+')
                                <td class="results-second" title="{{ $race->track->idCountry.' - '.$race->track->name }}">{{ $result['races'][$race->id] }}</td>
                            @elseif($result['races'][$race->id] == '3' or $result['races'][$race->id] == '3+')
                                <td class="results-third" title="{{ $race->track->idCountry.' - '.$race->track->name }}">{{ $result['races'][$race->id] }}</td>
                            @elseif(strpos($result['races'][$race->id], 'DNF'))
                                <td class="results-dnf" title="{{ $race->track->idCountry.' - '.$race->track->name }}">{{ $result['races'][$race->id] }}</td>
                            @elseif($result['races'][$race->id] == 'DNS')
                                <td class="results-dns" title="{{ $race->track->idCountry.' - '.$race->track->name }}">{{ $result['races'][$race->id] }}</td>
                            @elseif($result['races'][$race->id] == 'TBR')
                                <td class="results-tbr" title="{{ $race->track->idCountry.' - '.$race->track->name }}">{{ $result['races'][$race->id] }}</td>
                            @else
                                <td title="{{ $race->track->idCountry.' - '.$race->track->name }}">{{ $result['races'][$race->id] }}</td>
                            @endif
                        @endforeach
                        <td class="drivers-pts-col sticky-col-pts">
                            {{ $result['points'] }}
                        </td>
                    </tr>
                    @php
                        $position++;
                    @endphp
                @endforeach
                </tbody>
            </table>
            </div>
        </div>
        </div>
    </div>
@endsection
