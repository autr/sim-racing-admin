@extends('presentation.presentation')
@section('presentation-part')
    <div class="card">
        <div class="card-header">
            {{ __('Upcoming penalties') }}
        </div>

        <div class="card-body">
            @if(!isset($penalties) or count($penalties) == 0)
                {{ __('administration.penalties_empty') }}
            @else
                <div class="table-responsive">
                    <table class="table table-hover table-borderless table-sm light-text">
                        <thead class="bg-accent-lighter light-text">
                        <tr>
                            <th scope="col">{{ __('administration.penalties_driver') }}</th>
                            <th scope="col">{{ __('administration.penalties_race') }}</th>
                            <th scope="col">{{ __('administration.penalties_type') }}</th>
                            <th scope="col">{{ __('administration.penalties_amount') }}</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($penalties as $penalty)
                            <tr>
                                <td>
                                    {{ $penalty->driver->name }}
                                </td>
                                <td>
                                    {{ $penalty->race->track->name.', '.$penalty->race->track->country->code}}
                                </td>
                                <td>
                                    {{ $penalty->type->name }}
                                </td>
                                <td>
                                    @if(!is_null($penalty->amount))
                                        {{ $penalty->amount }}
                                    @else
                                        {{ __('administration.penalties_na') }}
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            @endif
        </div>

        <div class="card-header">
            {{ __('Penalty points') }}
        </div>
        <div class="card-body">
            @if(!isset($penaltyPoints) or count($penaltyPoints) == 0)
                {{ __('administration.penalties_points_empty') }}
            @else
                <div class="table-responsive">
                    <table class="table table-hover table-borderless table-sm light-text">
                        <thead class="bg-accent-lighter light-text">
                        <tr>
                            <th scope="col">{{ __('administration.penalties_driver') }}</th>
                            <th scope="col">{{ __('administration.penalties_amount') }}</th>
                            <th scope="col">{{ __('administration.penalties_newest') }}</th>
                            <th scope="col">{{ __('administration.penalties_oldest') }}</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($penaltyPoints as $penaltyPoint)
                            <tr>
                                <td>
                                    {{ $penaltyPoint->driver->name }}
                                </td>
                                <td>
                                    {{ $penaltyPoint->overall_amount }}
                                </td>
                                <td>
                                    {{ \Carbon\Carbon::parse($penaltyPoint->newest)->format('d F Y') }}
                                </td>
                                <td>
                                    {{ \Carbon\Carbon::parse($penaltyPoint->oldest)->format('d F Y') }}
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            @endif
        </div>
    </div>
@endsection
