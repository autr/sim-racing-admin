@extends('presentation.presentation')
@section('presentation-part')
    <div class="card">
        <div class="card-body">
        @if(!isset($calendar) or count($calendar) == 0)
            {{ __('administration.calendar_empty') }}
        @else
            <div class="table-responsive">
                <table class="table table-hover table-borderless table-sm light-text">
                    <thead class="bg-accent-lighter light-text">
                    <tr>
                        <th scope="col">{{ __('administration.calendar_date') }}</th>
                        <th scope="col">{{ __('administration.calendar_track') }}</th>
                        <th scope="col">{{ __('administration.calendar_country') }}</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($calendar as $race)
                        <tr>
                            <td>
                                @if (!is_null($race->date))
                                    {{ $race->date->format('d F Y') }}
                                @else
                                    {{ __('administration.calendar_date_not_set') }}
                                @endif
                            </td>
                            <td>
                                {{ $race->track->name }}
                            </td>
                            <td>
                                @if (!is_null($race->track->country))
                                    {{ $race->track->country->name }}
                                @else
                                    {{ __('administration.calendar_country_not_set') }}
                                @endif
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        @endif
        </div>
    </div>
@endsection
