@extends('administration')
@section('title')
    {{ 'Viewing '.$leagueInfo->name.' - '.$splitInfo->name }}
@endsection
@section('content')
    <div class="col-md-1 mb-4">
        <div class="card h-100">
            <div class="no-padding card-body">
                @include('nav.presentation-nav')
            </div>
        </div>
    </div>
    <div class="col-md-11 mb-4">
        @yield('presentation-part')
    </div>
@endsection
