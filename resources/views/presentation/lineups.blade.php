@extends('presentation.presentation')
@section('presentation-part')
    <div class="card">
        <div class="card-body">
@php
    $empty = true;
@endphp
@if(count($teamsInfo) > 0)
    @php
        $count = 0;
        $empty = false;
    @endphp
    @foreach($teamsInfo as $team)
        @if ($count % 4 == 0)
            <div class="row mb-3">
                @php
                    $close = true;
                @endphp
                @endif
                <div class="col-md-3">
                    <ul>
                        <dt>
                            @include('teamColour', ['team' => $team])
                            <div class="float-left ml-3">{{ $team->name }}</div>
                        </dt>
                        @if(count($team->drivers) > 0)
                            <br>
                            @foreach($team->drivers as $driver)
                                <dd class="ml-3">{{ $driver->name }}</dd>
                            @endforeach
                        @else
                            <br>
                            <dd class="ml-3">{{ __('administration.drivers_empty') }}</dd>
                        @endif
                    </ul>
                </div>
                @php
                    $count++;
                @endphp
                @if ($count % 4 == 0)
            </div>
            @php
                $close = false;
            @endphp
            @endif
            @endforeach
            @if($close)
            </div>
        @endif
        @endif
        @if(count($unalignedDrivers) > 0)
            @php
                $empty = false;
            @endphp
            <div class="row mb-3">
                <ul>
                    <dt>No team</dt>
                    @foreach($unalignedDrivers as $ud)
                        <dd class="ml-3">{{ $ud->name }}</dd>
                    @endforeach
                </ul>
            </div>
        @endif
        @if($empty)
            <div class="row">
                {{ __('No teams or drivers') }}
            </div>
        @endif
    </div>
</div>
@endsection
