@extends('presentation.presentation')
@section('presentation-part')
<div class="card">
    <div class="card-body">
        <div class="row mb-2">
            <div class="col-md-5 text-md-right">{{ __('administration_forms.league_name') }}</div>
            <div class="col-md-6">{{ $leagueInfo->name }}</div>
        </div>
        <div class="row mb-2">
            <div class="col-md-5 text-md-right">{{ __('administration_forms.split_name') }}</div>
            <div class="col-md-6">{{ $splitInfo->name }}</div>
        </div>
        <div class="row mb-2">
            <div class="col-md-5 text-md-right">{{ __('administration_forms.split_platform') }}</div>
            <div class="col-md-6">{{ $splitInfo->platform->name }}</div>
        </div>
        <div class="row mb-2">
            <div class="col-md-5 text-md-right">{{ __('administration_forms.split_game') }}</div>
            <div class="col-md-6">{{ $splitInfo->game->name }}</div>
        </div>
        <div class="row mb-2">
            <div class="col-md-5 text-md-right">{{ __('administration_forms.split_region') }}</div>
            <div class="col-md-6">{{ $splitInfo->region->id . " - " . $splitInfo->region->name }}</div>
        </div>
    </div>
</div>
@endsection
