@extends('presentation.presentation')
@section('presentation-part')
    <div class="card">
        <div class="card-body">
        @if (count($teamStandings) > 0)
        <h3>Team standings
            @if (Auth::check())
            <button id="click-scr-team-standings" class="btn light-text btn-bigger-font btn-sm"><i class="fas fa-camera"></i></button>
            @endif
        </h3>
        <div class="table-responsive text-center">
            <div id="team-standings-scr" class="w-25">
            <table class="table table-borderless table-sm light-text results-teams-table">
                <thead class="bg-accent-lighter light-text">
                <tr>
                    <th scope="col"></th>
                    <th class="th-sm" scope="col"></th>
                    <th class="th-sm" scope="col"></th>
                    <th class="th-sm" scope="col">Pts</th>
                </tr>
                </thead>
                <tbody>
                @php
                    $position = 1;
                @endphp
                @foreach($teamStandings as $ts)
                <tr>
                    <th scope="row">{{ $position }}</th>
                    <td class="text-left">
                        {{ $ts['name'] }}
                    </td>
                    <td>
                        @include('teamColour', ['team' => $ts])
                    </td>
                    <td>
                        {{ $ts['points'] }}
                    </td>
                </tr>
                @php
                    $position++;
                @endphp
                @endforeach
                </tbody>
            </table>
            </div>
        </div>
        @else
            No teams were found
        @endif
        </div>
    </div>
@endsection
