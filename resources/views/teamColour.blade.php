@if (!is_null($team['secondaryColour1']) and !is_null($team['secondaryColour2']))
    <div class="float-left" style="height: 1em;width: 3em;background: linear-gradient(110deg, {{ $team['primaryColour'] }} 50%, {{ $team['secondaryColour1'] }} 75%, {{ $team['secondaryColour2'] }} 100%);"></div>
@elseif (!is_null($team['secondaryColour1']) and is_null($team['secondaryColour2']))
    <div class="float-left" style="height: 1em;width: 3em;background: linear-gradient(110deg, {{ $team['primaryColour'] }} 50%, {{ $team['secondaryColour1'] }} 75%, {{ $team['secondaryColour1'] }} 100%);"></div>
@elseif (is_null($team['secondaryColour1']) and !is_null($team['secondaryColour2']))
    <div class="float-left" style="height: 1em;width: 3em;background: linear-gradient(110deg, {{ $team['primaryColour'] }} 50%, {{ $team['secondaryColour2'] }} 75%, {{ $team['secondaryColour2'] }} 100%);"></div>
@else
    <div class="float-left" style="height: 1em;width: 3em;background: linear-gradient(110deg, {{ $team['primaryColour'] }} 50%, {{ $team['primaryColour'] }} 75%, {{ $team['primaryColour'] }} 100%);"></div>
@endif
