@extends('browse')

@section('title')
    {{ __('Information') }}
@endsection
@section('content')
    <div class="col-md-12 mb-4">
        <div class="card">
            <div class="card-body">
                <h2>About this app</h2>
                <p>
                    Main goal is to make a tool that could replace spreadsheets that many leagues use nowadays. A tool
                    that offers quality of life features and offers an automation for tasks where mistakes can be made
                    very often.
                </p>
                <h2>Current website situation</h2>
                <p>
                    <span class="red light-text">This website is just an online testing site. Features you are currently seeing
                        are incomplete. Data will be wiped very often.</span><br>
                    Later on when more features are implemented, I will create a new proper hosting with its own domain.<br>
                    For any questions use <a class="bg-accent-lighter light-text" href="mailto:simracingadm@gmail.com">simracingadm@gmail.com</a> to contact an admin.
                </p>
                <h2>Features</h2>
                <ul>
                    <li>League management</li>
                    <ul>
                        <li>Multiple splits per league</li>
                        <li>Driver management</li>
                        <li>Team management</li>
                        <li>Racing calendar</li>
                        <li>Penalties</li>
                        <li>Drag&drop result creation</li>
                    </ul>
                    <li>League presentation</li>
                    <ul>
                        <li>Lineups</li>
                        <li>Calendar</li>
                        <li>Results</li>
                    </ul>
                </ul>
            </div>
        </div>
    </div>
@endsection
