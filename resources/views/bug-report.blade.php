@extends('home')

@section('title')
    {{ __('Bug report') }}
@endsection
@section('content')
    <div class="col-md-12 mb-4">
        <div class="card">
            <div class="card-header">{{ __('Bug reporting') }}</div>

            <div class="card-body">
                For issues and/or suggestions, please, use the issue tracker for this app's repository:<br>
                <a class="bg-accent-lighter light-text" href="https://gitlab.com/autr/sim-racing-admin/issues" target="_blank">GitLab Issue Tracker for Sim-Racing Admin</a><br><br>
                or contact an admin via <a class="bg-accent-lighter light-text" href="mailto:simracingadm@gmail.com">simracingadm@gmail.com</a><br><br>
                or join <a class="bg-accent-lighter light-text" href="https://discord.gg/yhnybhY" target="_blank">Discord server</a><br><br>
                When reporting a bug, please, include steps on how to reproduce said bug. Thank you.
            </div>
        </div>
    </div>
@endsection
