@extends('administration')
@section('title')
    {{ $leagueInfo->name.' - '.$splitInfo->name.' - ' . __('administration.calendar_title') }}
@endsection
@section('content')
    <div class="col-md-1 mb-4">
        <div class="card h-100">
            <div class="no-padding card-body">
                @include('nav.admin-nav')
            </div>
        </div>
    </div>
    <div class="col-md-11 mb-4">
        @if(session()->has('errorMessage'))
            <div class="alert alert-danger">
                {{ session()->get('errorMessage') }}
            </div>
        @endif
        @if(session()->has('message'))
            <div class="alert alert-success">
                {{ session()->get('message') }}
            </div>
        @endif
        <div id="messageFromSessionStorage" class="alert alert-success" style="display: none"></div>
        <div class="card">
            <div class="card-header pt-1 pb-1">
                <div class="d-flex justify-content-between align-items-center">
                    <div class="mb-1 mt-2">
                        {{ __('administration.calendar_overview',
                            ['league' => $leagueInfo->name,
                            'split' => $splitInfo->name]) }}
                    </div>
                    <div>
                        <button type="button" class="btn btn-sm bg-lighter light-text mb-1 mt-1"
                                title="{{ __('administration.calendar_existing') }}"
                                data-toggle="modal"
                                data-target="#addTracks" {{ !isset($tracks) or count($tracks) == 0 ? 'disabled' : '' }}>
                            <i class="fas fa-calendar-alt"></i>
                            <span>{{ __('Pick existing track(s)') }}</span>
                        </button>
                        <button type="button" class="btn btn-sm bg-lighter light-text mb-1 mt-1"
                                title="{{ __('administration.calendar_new') }}"
                                data-toggle="modal"
                                data-target="#newTrack">
                            <i class="fas fa-calendar-plus"></i>
                            <span>{{ __('Add new track') }}</span>
                        </button>
                    </div>
                </div>
            </div>

            <div class="card-body">
                @if(!isset($calendar) or count($calendar) == 0)
                    {{ __('administration.calendar_empty') }}
                @else
                    <div class="table-responsive">
                        <table class="table table-hover table-borderless table-sm light-text">
                            <thead class="bg-accent-lighter light-text">
                            <tr>
                                <th scope="col">{{ __('administration.calendar_track') }}</th>
                                <th scope="col" class="text-center">{{ __('administration.calendar_country') }}</th>
                                <th scope="col" class="text-center">{{ __('administration.calendar_date') }}</th>
                                <th scope="col" class="text-center">{{ __('administration.calendar_multiplier') }}</th>
                                <th scope="col"></th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($calendar as $race)
                                <tr>
                                    <td>
                                        {{ $race->track->name }}
                                    </td>
                                    <td class="text-center">
                                        @if (!is_null($race->track->country))
                                            {{ $race->track->country->name }}
                                        @else
                                            {{ __('administration.calendar_country_not_set') }}
                                        @endif
                                    </td>
                                    <td class="text-center">
                                        @if (!is_null($race->date))
                                            {{ $race->date->format('d F Y') }}
                                        @else
                                            {{ __('administration.calendar_date_not_set') }}
                                        @endif
                                    </td>
                                    <td class="text-center">
                                        @if (!is_null($race->multiplier))
                                            {{ $race->multiplier }}
                                        @else
                                            {{ __('administration.calendar_multiplier_not_set') }}
                                        @endif
                                    </td>
                                    <td class="text-right">
                                        @if (!$race->hasResults())
                                        <button type="button" class="btn bg-accent light-text btn-sm" title="{{ __('administration.calendar_edit_race') }}"
                                                data-toggle="modal"
                                                data-target="#updateRace"
                                                data-raceid="{{ $race->id }}"
                                                data-racename="{{ $race->track->name }}"
                                                data-racecountry="{{ !is_null($race->track->idCountry) ? $race->track->country->name : -1 }}"
                                                data-racedate="{{ !is_null($race->date) ? $race->date : -1 }}"
                                                data-racemultiplier="{{ $race->multiplier }}">
                                            <i class="fas fa-edit"></i>
                                            <span>{{ __('Edit') }}</span>
                                        </button>
                                        @endif
                                        <button type="button" class="btn btn-danger btn-sm" title="{{ __('administration.calendar_remove_race') }}"
                                                data-toggle="modal"
                                                data-target="#removeRace"
                                                data-raceid="{{ $race->id }}">
                                            <i class="fas fa-trash"></i>
                                            <span>{{ __('Remove') }}</span>
                                        </button>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                @endif
            </div>
        </div>
    </div>

    <!-- Modal for adding new track -->
    <div id="newTrack" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="newTrack"
         aria-hidden="true">
        <div class="modal-dialog modal-lg modal-notify">
            <div class="modal-content">
                <div class="modal-header bg-accent-lighter light-text">
                    <h4 class="modal-title">{{ __('administration.calendar_new') }}</h4>
                </div>
                @include('forms.newTrack')
            </div>
        </div>
    </div>

    <!-- Modal for adding tracks -->
    <div id="addTracks" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="addTracks"
         aria-hidden="true">
        <div class="modal-dialog modal-lg modal-notify">
            <div class="modal-content">
                <div class="modal-header bg-accent-lighter light-text">
                    <h4 class="modal-title">{{ __('administration.calendar_existing') }}</h4>
                </div>
                @include('forms.addTracks')
            </div>
        </div>
    </div>

    <!-- Modal for race editing -->
    <div id="updateRace" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="updateRace"
         aria-hidden="true">
        <div class="modal-dialog modal-lg modal-notify">
            <div class="modal-content">
                <div class="modal-header bg-accent-lighter light-text">
                    <h4 class="modal-title">{{ __('administration_forms.calendar_title') }}</h4>
                </div>
                @include('forms.raceEdit')
            </div>
        </div>
    </div>

    <!-- Modal for removing races -->
    <div id="removeRace" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="removeRace"
         aria-hidden="true">
        <div class="modal-dialog modal-lg modal-notify">
            <div class="modal-content">
                <div class="modal-header bg-accent-lighter light-text">
                    <h4 class="modal-title">{{ __('administration_forms.calendar_remove') }}</h4>
                </div>
                <div class="modal-body bg-main light-text">
                    <div id="error-rr" class="alert alert-danger" style="display:none"></div>
                    <div id="success-rr" class="alert alert-success" style="display:none"></div>
                    <div class="form-group row flex-center light-text">
                        {{ __('administration_forms.other_remove_race') }}
                    </div>
                    <form>
                        @csrf
                        <input id="idRace-r" type="hidden" name="idRace" value="">
                    </form>
                    <div class="row flex-center">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">{{ __('administration_forms.button_close') }}</button>
                        <button id="removeRace-btn" class="btn bg-accent light-text">{{ __('administration_forms.button_confirm') }}</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
