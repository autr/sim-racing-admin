@extends('administration')
@section('title')
    {{ $leagueInfo->name.' - '.$splitInfo->name.' - '.__('administration.teams_title') }}
@endsection
@section('content')
    <div class="col-md-1 mb-4">
        <div class="card h-100">
            <div class="no-padding card-body">
                @include('nav.admin-nav')
            </div>
        </div>
    </div>
    <div class="col-md-11 mb-4">
        @if(session()->has('errorMessage'))
            <div class="alert alert-danger">
                {{ session()->get('errorMessage') }}
            </div>
        @endif
        @if(session()->has('message'))
            <div class="alert alert-success">
                {{ session()->get('message') }}
            </div>
        @endif
        <div id="messageFromSessionStorage" class="alert alert-success" style="display: none"></div>
        <div class="card">
            <div class="card-header pt-1 pb-1">
                <div class="d-flex justify-content-between">
                    <div class="mb-1 mt-2">
                        {{ __('administration.teams_overview',
                            ['league' => $leagueInfo->name,
                            'split' => $splitInfo->name]) }}
                    </div>
                    <div>
                        <button type="button" class="btn btn-sm bg-lighter light-text mb-1 mt-1"
                                title="{{ __('administration.teams_add_teams') }}"
                                data-toggle="modal"
                                data-target="#addTeam">
                            <i class="fas fa-plus"></i>
                            <span>{{ __('Add new team(s)') }}</span>
                        </button>
                    </div>
                </div>
            </div>

            <div class="card-body">
                @if(!isset($teams) or count($teams) == 0)
                    {{ __('administration.teams_empty') }}
                @else
                    <div class="table-responsive">
                        <table class="table table-hover table-borderless table-sm light-text">
                            <thead class="bg-accent-lighter light-text">
                            <tr>
                                <th scope="col">{{ __('administration.teams_name') }}</th>
                                <th scope="col">{{ __('administration.teams_colour') }}</th>
                                <th scope="col"></th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($teams as $team)
                                <tr>
                                    <td>{{ $team->name }}</td>
                                    @if (!is_null($team->secondaryColour1) and !is_null($team->secondaryColour2))
                                        <td style="background: linear-gradient(110deg, {{ $team->primaryColour }} 50%, {{ $team->secondaryColour1 }} 75%, {{ $team->secondaryColour2 }} 100%);"></td>
                                    @elseif (!is_null($team->secondaryColour1) and is_null($team->secondaryColour2))
                                        <td style="background: linear-gradient(110deg, {{ $team->primaryColour }} 50%, {{ $team->secondaryColour1 }} 75%, {{ $team->secondaryColour1 }} 100%);"></td>
                                    @elseif (is_null($team->secondaryColour1) and !is_null($team->secondaryColour2))
                                        <td style="background: linear-gradient(110deg, {{ $team->primaryColour }} 50%, {{ $team->secondaryColour2 }} 75%, {{ $team->secondaryColour2 }} 100%);"></td>
                                    @else
                                        <td style="background: linear-gradient(110deg, {{ $team->primaryColour }} 50%, {{ $team->primaryColour }} 75%, {{ $team->primaryColour }} 100%);"></td>
                                    @endif
                                    <td class="text-right">
                                        <button type="button" class="btn bg-accent light-text btn-sm" title="{{ __('administration.teams_edit_team') }}"
                                                data-toggle="modal"
                                                data-target="#updateTeam"
                                                data-teamid="{{ $team->id }}"
                                                data-teamname="{{ $team->name }}"
                                                data-teamprimarycolour="{{ $team->primaryColour }}"
                                                data-teamsecondarycolour1="{{ !is_null($team->secondaryColour1) ? $team->secondaryColour1 : -1 }}"
                                                data-teamsecondarycolour2="{{ !is_null($team->secondaryColour2) ? $team->secondaryColour2 : -1 }}">
                                            <i class="fas fa-edit"></i>
                                            <span>{{ __('Edit') }}</span>
                                        </button>
                                        @if(!$team->hasResults())
                                        <button type="button" class="btn btn-danger btn-sm" title="{{ __('administration.teams_remove') }}"
                                                data-toggle="modal"
                                                data-target="#removeTeam"
                                                data-teamid="{{ $team->id }}">
                                            <i class="fas fa-trash"></i>
                                            <span>{{ __('Remove') }}</span>
                                        </button>
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                @endif
            </div>
        </div>
    </div>

    <!-- Modal for adding teams -->
    <div id="addTeam" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="addTeam"
         aria-hidden="true">
        <div class="modal-dialog modal-lg modal-notify">
            <div class="modal-content">
                <div class="modal-header bg-accent-lighter light-text">
                    <h4 class="modal-title">{{ __('administration_forms.team_add_title') }}</h4>
                </div>
                @include('forms.newTeams')
            </div>
        </div>
    </div>

    <!-- Modal for team editing -->
    <div id="updateTeam" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="updateTeam"
         aria-hidden="true">
        <div class="modal-dialog modal-lg modal-notify">
            <div class="modal-content">
                <div class="modal-header bg-accent-lighter light-text">
                    <h4 class="modal-title">{{ __('administration_forms.team_update_title') }}</h4>
                </div>
                @include('forms.teamEdit')
            </div>
        </div>
    </div>

    <!-- Modal for removing teams -->
    <div id="removeTeam" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="removeTeam"
         aria-hidden="true">
        <div class="modal-dialog modal-lg modal-notify">
            <div class="modal-content">
                <div class="modal-header bg-accent-lighter light-text">
                    <h4 class="modal-title">{{ __('administration_forms.team_remove') }}</h4>
                </div>
                <div class="modal-body bg-main light-text">
                    <div id="error-d" class="alert alert-danger" style="display:none"></div>
                    <div id="success-d" class="alert alert-success" style="display:none"></div>
                    <div class="form-group row flex-center light-text">
                        {{ __('administration_forms.other_remove_team') }}
                    </div>
                    <form>
                        @csrf
                        <input id="idTeam-d" type="hidden" name="idTeam" value="">
                    </form>
                    <div class="row flex-center">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">{{ __('administration_forms.button_close') }}</button>
                        <button id="removeTeam-btn" class="btn bg-accent light-text">{{ __('administration_forms.button_confirm') }}</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
