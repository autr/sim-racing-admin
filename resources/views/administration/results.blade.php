@extends('administration')
@section('title')
    {{ $leagueInfo->name.' - '.$splitInfo->name.' - Manage results' }}
@endsection
@section('content')
    <div class="col-md-1 mb-4">
        <div class="card h-100">
            <div class="no-padding card-body">
                @include('nav.admin-nav')
            </div>
        </div>
    </div>
    <div class="col-md-11 mb-4">
        @if(session()->has('errorMessage'))
            <div class="alert alert-danger">
                {{ session()->get('errorMessage') }}
            </div>
        @endif
        @if(session()->has('message'))
            <div class="alert alert-success">
                {{ session()->get('message') }}
            </div>
        @endif
        <div id="messageFromSessionStorage" class="alert alert-success" style="display: none"></div>
        @if(!isset($races) or count($races) ==0)
            <div class="card h-100">
                <div class="card-body">
                    {{ __('administration.results_no_races') }}
                </div>
            </div>
        @else
            <div class="row mb-4">
                <div class="col-md-12">
                    <div class="card h-100">
                        <div class="card-body">
                            <div class="d-flex justify-content-start align-items-center">
                                <label for="race" class="col-form-label text-md-right light-text">{{ __('administration.results_select_race') }}</label>
                                <select id="race" name="race" class="col-md-3 browser-default custom-select mb-2 mt-2 mr-2 ml-2">
                                    @foreach($races as $race)
                                        <option value="{{ $race->id }}" {{ $race->id == $currentRace->id ? 'selected' : ''}}>{{ $race->date->format('d F Y').' - '.$race->track->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @if(!isset($results) or count($results) == 0)
                @include('forms.newResults')
            @else
                @include('forms.resultsEdit')
            @endif
        @endif
    </div>
@endsection
