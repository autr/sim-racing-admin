@extends('administration')

@section('content')
    <div class="col-md-1 mb-4">
        <div class="card h-100">
            <div class="no-padding card-body">
                @include('nav.admin-nav')
            </div>
        </div>
    </div>
    <div class="col-md-11 mb-4">
        @if(session()->has('errorMessage'))
            <div class="alert alert-danger">
                {{ session()->get('errorMessage') }}
            </div>
        @endif
        @if(session()->has('message'))
            <div class="alert alert-success">
                {{ session()->get('message') }}
            </div>
        @endif
        <div id="messageFromSessionStorage" class="alert alert-success" style="display: none"></div>
        <div class="card">
            <div class="card-header">{{ __('administration_forms.split_header') }}</div>

            <div class="card-body">
                <div class="row mb-2">
                    <div class="col-md-5 text-md-right">{{ __('administration_forms.split_season_current') }}</div>
                    <div class="col-md-6">{{ $season }}</div>
                </div>
                <div class="row mb-2">
                    <div class="col-md-5 text-md-right">{{ __('administration_forms.split_name') }}</div>
                    <div class="col-md-6">{{ $splitInfo->name }}</div>
                </div>
                <div class="row mb-2">
                    <div class="col-md-5 text-md-right">{{ __('administration_forms.split_platform') }}</div>
                    <div class="col-md-6">{{ $splitInfo->platform->name }}</div>
                </div>
                <div class="row mb-2">
                    <div class="col-md-5 text-md-right">{{ __('administration_forms.split_game') }}</div>
                    <div class="col-md-6">{{ $splitInfo->game->name }}</div>
                </div>
                <div class="row mb-2">
                    <div class="col-md-5 text-md-right">{{ __('administration_forms.split_region') }}</div>
                    <div class="col-md-6">{{ $splitInfo->region->id . " - " . $splitInfo->region->name }}</div>
                </div>
                <div class="row mb-2">
                    <div class="col-md-5 text-md-right">{{ __('administration_forms.split_max_per_team') }}</div>
                    <div class="col-md-6">{{ $splitInfo->max_per_team }}</div>
                </div>
                <div class="row mb-2">
                    <div class="col-md-5 text-md-right">{{ __('administration_forms.split_max_per_race') }}</div>
                    <div class="col-md-6">{{ $splitInfo->max_per_race }}</div>
                </div>
                <div class="row mb-2">
                    <div class="col-md-5 text-md-right">{{ __('administration_forms.split_points') }}</div>
                    <div class="col-md-6">{{ $splitInfo->points }}</div>
                </div>
                <div class="row mb-2">
                    <div class="col-md-5 text-md-right">{{ __('administration_forms.split_no_points_dnf') }}</div>
                    <div class="col-md-6">{!!  $splitInfo->no_points_dnf ? '<i class="fas fa-check green-fa"></i>' : '<i class="fas fa-times red-fa"></i>' !!}</div>
                </div>
                <div class="row mb-2">
                    <div class="col-md-5 text-md-right">{{ __('administration_forms.split_omit_inactive') }}</div>
                    <div class="col-md-6">{!!  $splitInfo->omit_inactive ? '<i class="fas fa-check green-fa"></i>' : '<i class="fas fa-times red-fa"></i>' !!}</div>
                </div>
                <div class="row mb-2">
                    <div class="col-md-5 text-md-right">{{ __('administration.split_link') }}</div>
                    <div class="col-md-6">
                        <div class="input-group">
                            <input id="split-link" type="text" class="form-control" aria-label="Split link"
                                   aria-describedby="button-addon2" value="{{ route('presentation.index', ['league' => urlencode($league), 'split' => $splitId]) }}">
                            <div class="input-group-append">
                                <button class="btn btn-md bg-accent light-text m-0 px-3 py-2 z-depth-0 copy-button" type="button" id="button-addon2" data-clipboard-target="#split-link" title="Copy to clipboard">
                                    <i class="fas fa-copy"></i>
                                    <span>{{ __('Copy') }}</span>
                                </button>
                                <a class="btn btn-md bg-accent light-text mt-0 mr-0 mb-0 ml-1 z-depth-0 copy-button" type="button" href="{{ route('presentation.index', ['league' => urlencode($league), 'split' => $splitId]) }}" title="Go to">
                                    <i class="fas fa-share-square"></i>
                                    <span>{{ __('Go') }}</span>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row mb-0 d-flex justify-content-center">
                    <button type="button" class="btn bg-accent light-text"
                            data-toggle="modal"
                            data-target="#updateSplit">
                        {{ __('administration_forms.button_update_split') }}
                    </button>
                    <button type="button" class="btn bg-accent light-text"
                            data-toggle="modal"
                            data-target="#newSeason">
                        {{ __('administration_forms.season_button') }}
                    </button>
                    @if($superAdmin > 0)
                        <button type="button" class="btn btn-danger light-text"
                                data-toggle="modal"
                                data-target="#deleteSplit">
                            <i class="fas fa-trash"></i>
                            <span>{{ __('Remove') }}</span>
                        </button>
                    @endif
                </div>
            </div>
        </div>
    </div>

    <!-- Modal for split update confirmation -->
    <div id="updateSplit" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="updateSplit"
         aria-hidden="true">
        <div class="modal-dialog modal-lg modal-notify">
            <div class="modal-content">
                <div class="modal-header bg-accent-lighter light-text">
                    <h4 class="modal-title">{{ __('administration_forms.split_confirm') }}</h4>
                </div>
                @include('forms.splitEdit')
            </div>
        </div>
    </div>

    <!-- Modal for new season -->
    <div id="newSeason" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="newSeason"
         aria-hidden="true">
        <div class="modal-dialog modal-lg modal-notify">
            <div class="modal-content">
                <div class="modal-header bg-accent-lighter light-text">
                    <h4 class="modal-title">{{ __('administration_forms.season_title') }}</h4>
                </div>
                <div class="modal-body bg-main light-text">
                    <div id="error" class="alert alert-danger" style="display:none"></div>
                    <div id="success" class="alert alert-success" style="display:none"></div>
                    <div class="form-group row flex-center light-text">
                        {{ __('administration_forms.season_text') }}
                    </div>
                    @include('forms.newSeason')
                    <div class="row flex-center">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">{{ __('administration_forms.button_close') }}</button>
                        <button id="newSeason-btn" class="btn bg-accent light-text">{{ __('administration_forms.button_confirm') }}</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @if ($superAdmin > 0)
        <!-- Modal for split removal confirmation -->
        <div id="deleteSplit" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="deleteSplit"
             aria-hidden="true">
            <div class="modal-dialog modal-lg modal-notify">
                <div class="modal-content">
                    <div class="modal-header bg-accent-lighter light-text">
                        <h4 class="modal-title">{{ __('administration_forms.split_delete_confirm') }}</h4>
                    </div>
                    <div class="modal-body bg-main light-text">
                        <div class="form-group row flex-center light-text">
                            {{ __('administration_forms.other_delete_split_confirm') }}
                        </div>
                        <div class="row flex-center">
                            <button type="button" class="btn btn-danger" data-dismiss="modal">{{ __('administration_forms.button_close') }}</button>
                            <a href="{{ route('split.destroy', ['league' => urlencode($league), 'split' => $splitInfo->id]) }}" id="deleteSplit-btn" class="btn bg-accent light-text">{{ __('administration_forms.button_confirm') }}</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endif
@endsection
