@extends('administration')
@section('title')
    {{ $leagueInfo->name.' - New split' }}
@endsection
@section('content')
    <div class="col-md-1 mb-4">
        <div class="card h-100">
            <div class="no-padding card-body">
                @include('nav.admin-nav')
            </div>
        </div>
    </div>
    <div class="col-md-11 mb-4">
    <div class="card">
        <form method="POST" action="{{ route('split.store', urlencode($league)) }}">
            @csrf
            <div class="card-header">{{ __('administration_forms.split_header_new', ['name' => $leagueName]) }}</div>

            <div class="card-body">
                @include('forms.splitForm')

                <div class="form-group row mb-0">
                    <div class="col-md-6 offset-md-4">
                        <button type="submit" class="btn bg-accent light-text">
                            {{ __('administration_forms.button_create') }}
                        </button>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
@endsection
