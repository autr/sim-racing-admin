@extends('administration')
@section('title')
    {{ $leagueInfo->name.' - '.$splitInfo->name.' - '. __('administration.drivers_title_load_file') }}
@endsection
@section('content')
    <div class="col-md-1 mb-4">
        <div class="card h-100">
            <div class="no-padding card-body">
                @include('nav.admin-nav')
            </div>
        </div>
    </div>
    <div class="col-md-11 mb-4">
        @if(session()->has('errorMessage'))
            <div class="alert alert-danger">
                {{ session()->get('errorMessage') }}
            </div>
        @endif
        @if(session()->has('message'))
            <div class="alert alert-success">
                {{ session()->get('message') }}
            </div>
        @endif
        <div id="messageFromSessionStorage" class="alert alert-success" style="display: none"></div>
        <div class="card">
            <div class="card-header pt-1 pb-1">
                <div class="d-flex justify-content-between">
                    <div class="mb-1 mt-2">
                        {{ __('administration.drivers_load_file',
                            ['league' => $leagueInfo->name,
                            'split' => $splitInfo->name]) }}
                    </div>
                </div>
            </div>

            <div class="card-body">
                <div class="d-flex justify-content-center">
                    <form class="w-50" method="post" action="{{ route('drivers.storeFromFile', [urlencode($league), $splitId]) }}" enctype="multipart/form-data">
                        @csrf
                        <div class="form-group row mb-0">
                            <div class="input-group">
                                <div class="custom-file">
                                    <input type="file" accept=".csv" name="file" class="custom-file-input" id="csvFile"
                                           aria-describedby="csvFile">
                                    <label class="custom-file-label" for="csvFile">Choose file</label>
                                </div>

                            </div>
                            @if ($errors->has('file'))
                                <span class="red-text" role="alert">
                                        <strong>{{ $errors->first('file') }}</strong>
                                    </span>
                            @endif
                        </div>
                        <div class="form-group row mb-0 justify-content-center">
                                <button type="submit" class="btn bg-accent light-text">
                                    {{ __('administration_forms.button_load') }}
                                </button>
                        </div>
                    </form>
                </div>
            </div>

            <div class="card-header">{{ __('administration_forms.other_load_file_example') }}</div>

            <div class="card-body">
                <div class="row text-center">
                    <div class="col-md-4">
                        <h4>Drivers and teams:</h4>
                        driver1,team1<br>
                        driver2,team1<br>
                        driver3,team2<br>
                        driver4,team3<br>
                        driver5,team4<br>
                        driver6,team4
                    </div>
                    <div class="col-md-4">
                        <h4>Drivers only:</h4>
                        driver1<br>
                        driver2<br>
                        driver3<br>
                        driver4<br>
                        driver5<br>
                        driver6
                    </div>
                    <div class="col-md-4">
                        <h4>Teams only:</h4>
                        ,team1<br>
                        ,team2<br>
                        ,team3<br>
                        ,team4
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
