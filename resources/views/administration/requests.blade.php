@extends('home')
@section('title')
    {{ 'Manage requests' }}
@endsection
@section('content')
    <div class="col-md-12 mb-4">
        <div class="card">
            <div class="card-body">
                @if(session()->has('errorMessage'))
                    <div class="alert alert-danger">
                        {{ session()->get('errorMessage') }}
                    </div>
                @endif
                @if(session()->has('message'))
                    <div class="alert alert-success">
                        {{ session()->get('message') }}
                    </div>
                @endif
                <div id="messageFromSessionStorage" class="alert alert-success" style="display: none"></div>
                @if (!isset($requests) or count($requests) == 0)
                    <div>
                        {{ __('You have no pending requests') }}
                    </div>
                @else
                    <div class="table-responsive">
                        <table class="table table-hover table-borderless table-sm light-text">
                            <thead class="bg-accent-lighter light-text">
                            <tr>
                                <th scope="col">{{ __('League') }}</th>
                                <th scope="col">{{ __('Split') }}</th>
                                <th scope="col">{{ __('Status') }}</th>
                                <th scope="col">{{ __('Last updated') }}</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($requests as $request)
                                <tr>
                                    <td>{{ $request->split->league->name }}</td>
                                    <td>{{ $request->split->name }}</td>
                                    <td>{{ $request->status }}</td>
                                    @if (($request->status === 'disapproved') and ($request->updated_at < \Carbon\Carbon::now()->subDays(7)))
                                        <td><a href="{{ url('/request/'.$request->split->id) }}" class="btn bg-accent light-text active" role="button">Resend request</a></td>
                                    @else
                                        <td>{{ $request->updated_at->format('d F Y H:i:s') . ' (' . $request->updated_at->timezone . ')' }}</td>
                                    @endif
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                @endif
            </div>
        </div>
    </div>
@endsection
