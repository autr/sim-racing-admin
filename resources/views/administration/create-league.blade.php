@extends('home')
@section('title')
    {{ __('New league') }}
@endsection
@section('content')
    <div class="col-md-12 mb-4">
        <form method="POST" action="{{ route('league.store') }}">
            @csrf
            <div class="card">
                <div class="card-header">{{ __('administration_forms.league_header') }}</div>

                <div class="card-body">
                    <div class="form-group row">
                        <label for="leagueName" class="col-md-4 col-form-label text-md-right">{{ __('administration_forms.league_name') }}</label>

                        <div class="col-md-6 trumbowyg-dark">
                            <input id="leagueName" type="text" class="form-control{{ $errors->has('leagueName') ? ' is-invalid' : '' }}"
                                   name="leagueName" value="{{ old('leagueName') }}" required autofocus>

                            @if ($errors->has('leagueName'))
                                <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('leagueName') }}</strong>
                            </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="description" class="col-md-4 col-form-label text-md-right">{{ __('administration_forms.league_description') }}</label>

                        <div class="col-md-6 trumbowyg-dark">
                            <textarea id="description" class="form-control{{ $errors->has('description') ? ' is-invalid' : '' }}"
                                      name="description" rows="5">{{ old('description') }}</textarea>

                            @if ($errors->has('description'))
                                <span class="red-text" role="alert">
                                <strong>{{ $errors->first('description') }}</strong>
                            </span>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="card-header">{{ __('administration_forms.split_header') }}</div>

                <div class="card-body">
                    @include('forms.splitForm')

                    <div class="form-group row mb-0">
                        <div class="col-md-6 offset-md-4">
                            <button type="submit" class="btn bg-accent light-text">
                                {{ __('administration_forms.button_create') }}
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
@endsection
