@extends('administration')

@section('content')
    <div class="col-md-1 mb-4">
        <div class="card h-100">
            <div class="no-padding card-body">
                @include('nav.admin-nav')
            </div>
        </div>
    </div>
    <div class="col-md-11 mb-4">
        @if(session()->has('errorMessage'))
            <div class="alert alert-danger">
                {{ session()->get('errorMessage') }}
            </div>
        @endif
        @if(session()->has('message'))
            <div class="alert alert-success">
                {{ session()->get('message') }}
            </div>
        @endif
        <div id="messageFromSessionStorage" class="alert alert-success" style="display: none"></div>
        <div class="card">
            <div class="card-header">{{ __('Splits in '.$leagueInfo->name.':') }}</div>

            <div class="card-body">
                @if(!isset($splits))
                    No available splits
                @else
                    <div class="table-responsive">
                        <table class="table table-hover table-borderless table-sm light-text">
                            <thead class="bg-accent-lighter light-text">
                            <tr>
                                <th scope="col">{{ __('Split') }}</th>
                                <th scope="col">{{ __('Game') }}</th>
                                <th scope="col">{{ __('Platform') }}</th>
                                <th scope="col">{{ __('Region') }}</th>
                                <th scope="col"></th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($splits as $split)
                                <tr>
                                    <td>{{ $split->name }}</td>
                                    <td>{{ $split->game->name }}</td>
                                    <td>{{ $split->platform->name }}</td>
                                    <td>{{ $split->region->name }}</td>
                                    <td class="text-right">
                                        <a href="{{ url('a/'.urlencode($league).'/'.$split->id.'/home') }}" class="btn bg-accent light-text active btn-sm" role="button">
                                            <i class="fas fa-eye"></i>
                                            <span>{{ __('View') }}</span>
                                        </a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                @endif
            </div>
            <div class="card-header">{{ __('administration_forms.league_overview') }}</div>

            <div class="card-body">
                <form id="updateLeague-Form" method="POST" action="{{ route('league.update', urlencode($league)) }}">
                    @method('PATCH')
                    @csrf
                    <div class="form-group row">
                        <div class="col-md-4 col-form-label text-md-right">{{ __('administration_forms.league_name') }}</div>

                        <div class="col-md-6 col-form-label">
                            {{ $leagueInfo->name }}
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="description" class="col-md-4 col-form-label text-md-right">{{ __('administration_forms.league_description') }}</label>

                        <div class="col-md-6 trumbowyg-dark">
                                <textarea id="description" class="form-control{{ $errors->has('description') ? ' is-invalid' : '' }}"
                                          name="description" rows="5">{{ $leagueInfo->description }}</textarea>

                            @if ($errors->has('description'))
                                <span class="red-text" role="alert">
                                    <strong>{{ $errors->first('description') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <input name="leagueId" type="hidden" value="{{ $leagueInfo->id }}">
                    <div class="form-group row mb-0 d-flex justify-content-center">
                        <button type="button" class="btn bg-accent light-text"
                                data-toggle="modal"
                                data-target="#updateLeague">
                            {{ __('administration_forms.button_update_description') }}
                        </button>
                        @if ($superAdmin > 0)
                        <button type="button" class="btn bg-accent light-text"
                                data-toggle="modal"
                                data-target="#renameLeague">
                            {{ __('administration_forms.button_rename_league') }}
                        </button>
                        <button type="button" class="btn btn-danger light-text"
                                data-toggle="modal"
                                data-target="#deleteLeague">
                            <i class="fas fa-trash"></i>
                            <span>{{ __('Remove') }}</span>
                        </button>
                        @endif
                    </div>
                </form>
            </div>
        </div>
    </div>

    <!-- Modal for league update confirmation -->
    <div id="updateLeague" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="updateLeague"
         aria-hidden="true">
        <div class="modal-dialog modal-lg modal-notify">
            <div class="modal-content">
                <div class="modal-header bg-accent-lighter light-text">
                    <h4 class="modal-title">{{ __('administration_forms.league_confirm') }}</h4>
                </div>
                <div class="modal-body bg-main light-text">
                    <div class="form-group row flex-center light-text">
                        {{ __('administration_forms.other_confirm') }}
                    </div>
                    <div class="row flex-center">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">{{ __('administration_forms.button_close') }}</button>
                        <button id="updateLeague-btn" class="btn bg-accent light-text">{{ __('administration_forms.button_confirm') }}</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @if ($superAdmin > 0)
    <!-- Modal for renaming league -->
    <div id="renameLeague" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="renameLeague"
         aria-hidden="true">
        <div class="modal-dialog modal-lg modal-notify">
            <div class="modal-content">
                <div class="modal-header bg-accent-lighter light-text">
                    <h4 class="modal-title">{{ __('administration_forms.league_rename') }}</h4>
                </div>
                @include('forms.renameLeague')
            </div>
        </div>
    </div>
    <!-- Modal for league removal confirmation -->
    <div id="deleteLeague" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="deleteLeague"
         aria-hidden="true">
        <div class="modal-dialog modal-lg modal-notify">
            <div class="modal-content">
                <div class="modal-header bg-accent-lighter light-text">
                    <h4 class="modal-title">{{ __('administration_forms.league_delete_confirm') }}</h4>
                </div>
                <div class="modal-body bg-main light-text">
                    <div class="form-group row flex-center light-text">
                        {{ __('administration_forms.other_delete_confirm') }}
                    </div>
                    <div class="row flex-center">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">{{ __('administration_forms.button_close') }}</button>
                        <a href="{{ route('league.destroy', ['league' => urlencode($league)]) }}" id="deleteLeague-btn" class="btn bg-accent light-text">{{ __('administration_forms.button_confirm') }}</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @endif
@endsection
