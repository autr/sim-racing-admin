@extends('administration')
@section('title')
    {{ $leagueInfo->name.' - '.$splitInfo->name.' - '. __('administration.drivers_title') }}
@endsection
@section('content')
    <div class="col-md-1 mb-4">
        <div class="card h-100">
            <div class="no-padding card-body">
                @include('nav.admin-nav')
            </div>
        </div>
    </div>
    <div class="col-md-11 mb-4">
        @if(session()->has('errorMessage'))
            <div class="alert alert-danger">
                {{ session()->get('errorMessage') }}
            </div>
        @endif
        @if(session()->has('message'))
            <div class="alert alert-success">
                {{ session()->get('message') }}
            </div>
        @endif
        <div id="messageFromSessionStorage" class="alert alert-success" style="display: none"></div>
        <div class="card">
            <div class="card-header pt-1 pb-1">
                <div class="d-flex justify-content-between align-items-center">
                    <div class="mb-1 mt-2">
                        {{ __('administration.drivers_overview',
                            ['league' => $leagueInfo->name,
                            'split' => $splitInfo->name]) }}
                    </div>
                    <div>
                        <button type="button" class="btn btn-sm bg-lighter light-text mb-1 mt-1"
                                title="{{ __('administration.drivers_add_drivers') }}"
                                data-toggle="modal"
                                data-target="#addDriver">
                            <i class="fas fa-user-plus"></i>
                            <span>{{ __('Add new driver(s)') }}</span>
                        </button>
                    </div>
                </div>
            </div>

            <div class="card-body">
                @if(!isset($drivers) or count($drivers) == 0)
                    {{ __('administration.drivers_empty') }}
                @else
                    <div class="table-responsive">
                        <table class="table table-hover table-borderless table-sm light-text">
                            <thead class="bg-accent-lighter light-text">
                            <tr>
                                <th scope="col">{{ __('administration.drivers_name') }}</th>
                                <th scope="col">{{ __('administration.drivers_team') }}</th>
                                @if(isset($classes) and count($classes) > 0)
                                    <th scope="col">{{ __('administration.drivers_class') }}</th>
                                @endif
                                <th scope="col">{{ __('administration.drivers_active') }}</th>
                                <th scope="col"></th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($drivers as $driver)
                                <tr>
                                    <td>
                                        {{ $driver->name }}
                                    </td>
                                    <td>
                                        @if (!is_null($driver->team))
                                            {{ $driver->team->name }}
                                        @else
                                            {{ __('administration.drivers_no_team') }}
                                        @endif
                                    </td>
                                    @if(isset($classes) and count($classes) > 0)
                                    <td>
                                        @if (!is_null($driver->driverClass))
                                            {{ $driver->driverClass->name }}
                                        @else
                                            {{ __('administration.drivers_no_class') }}
                                        @endif
                                    </td>
                                    @endif
                                    <td>
                                        @if ($driver->active)
                                            <i class="fas fa-check green-fa"></i>
                                        @else
                                            <i class="fas fa-times red-fa"></i>
                                        @endif
                                    </td>
                                    <td class="text-right">
                                        <button type="button" class="btn bg-accent light-text btn-sm" title="{{ __('administration.drivers_edit_driver') }}"
                                                data-toggle="modal"
                                                data-target="#updateDriver"
                                                data-driverid="{{ $driver->id }}"
                                                data-drivername="{{ $driver->name }}"
                                                data-driverteam="{{ !is_null($driver->team) ? $driver->team->id : -1 }}"
                                                data-driveractive="{{ $driver->active }}"
                                                data-driverclass="{{ !is_null($driver->driverClass) ? $driver->driverClass->id : -1 }}">
                                            <i class="fas fa-edit"></i>
                                            <span>{{ __('Edit') }}</span>
                                        </button>
                                        <button type="button" class="btn btn-danger btn-sm" title="{{ __('administration.drivers_remove') }}"
                                                data-toggle="modal"
                                                data-target="#deactivateDriver"
                                                data-driverid="{{ $driver->id }}">
                                            <i class="fas fa-trash"></i>
                                            <span>{{ __('Remove') }}</span>
                                        </button>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                @endif
            </div>

            <div class="card-header pt-1 pb-1">
                <div class="d-flex justify-content-between align-items-center">
                    <div class="mb-1 mt-2">
                        {{ __('administration.classes_overview') }}
                    </div>
                    <div>
                        <button type="button" class="btn btn-sm bg-lighter light-text mb-1 mt-1"
                                title="{{ __('administration.drivers_add_classes') }}"
                                data-toggle="modal"
                                data-target="#addClass">
                            <i class="fas fa-plus"></i>
                            <span>{{ __('Add new class(es)') }}</span>
                        </button>
                    </div>
                </div>
            </div>

            <div class="card-body">
                @if(!isset($classes) or count($classes) == 0)
                    {{ __('administration.classes_empty') }}
                @else
                    <div class="table-responsive">
                        <table class="table table-hover table-borderless table-sm light-text">
                            <thead class="bg-accent-lighter light-text">
                            <tr>
                                <th scope="col">{{ __('administration.classes_name') }}</th>
                                <th scope="col"></th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($classes as $class)
                                <tr>
                                    <td>
                                        {{ $class->name }}
                                    </td>
                                    <td class="text-right">
                                        <button type="button" class="btn btn-danger btn-sm" title="{{ __('administration.classes_remove') }}"
                                                data-toggle="modal"
                                                data-target="#removeClass"
                                                data-classid="{{ $class->id }}">
                                            <i class="fas fa-trash"></i>
                                            <span>{{ __('Remove') }}</span>
                                        </button>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                @endif
            </div>
        </div>
    </div>

    <!-- Modal for adding drivers -->
    <div id="addDriver" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="addDriver"
         aria-hidden="true">
        <div class="modal-dialog modal-lg modal-notify">
            <div class="modal-content">
                <div class="modal-header bg-accent-lighter light-text">
                    <h4 class="modal-title">{{ __('administration_forms.driver_add_title') }}</h4>
                </div>
                @include('forms.newDrivers')
            </div>
        </div>
    </div>

    <!-- Modal for driver editing -->
    <div id="updateDriver" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="updateDriver"
         aria-hidden="true">
        <div class="modal-dialog modal-lg modal-notify">
            <div class="modal-content">
                <div class="modal-header bg-accent-lighter light-text">
                    <h4 class="modal-title">{{ __('administration_forms.driver_update_title') }}</h4>
                </div>
                @include('forms.driverEdit')
            </div>
        </div>
    </div>

    <!-- Modal for deactivating drivers -->
    <div id="deactivateDriver" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="deactivateDriver"
         aria-hidden="true">
        <div class="modal-dialog modal-lg modal-notify">
            <div class="modal-content">
                <div class="modal-header bg-accent-lighter light-text">
                    <h4 class="modal-title">{{ __('administration_forms.driver_remove') }}</h4>
                </div>
                <div class="modal-body bg-main light-text">
                    <div id="error-d" class="alert alert-danger" style="display:none"></div>
                    <div id="success-d" class="alert alert-success" style="display:none"></div>
                    <div class="form-group row flex-center light-text">
                        {{ __('administration_forms.other_remove_driver') }}
                    </div>
                    <form>
                        @csrf
                        <input id="idDriver-d" type="hidden" name="idDriver" value="">
                    </form>
                    <div class="row flex-center">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">{{ __('administration_forms.button_close') }}</button>
                        <button id="deactivateDriver-btn" class="btn bg-accent light-text">{{ __('administration_forms.button_confirm') }}</button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal for adding classes -->
    <div id="addClass" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="addClass"
         aria-hidden="true">
        <div class="modal-dialog modal-lg modal-notify">
            <div class="modal-content">
                <div class="modal-header bg-accent-lighter light-text">
                    <h4 class="modal-title">{{ __('administration_forms.driver_add_class_title') }}</h4>
                </div>
                @include('forms.newClasses')
            </div>
        </div>
    </div>

    <!-- Modal for removing class -->
    <div id="removeClass" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="removeClass"
         aria-hidden="true">
        <div class="modal-dialog modal-lg modal-notify">
            <div class="modal-content">
                <div class="modal-header bg-accent-lighter light-text">
                    <h4 class="modal-title">{{ __('administration_forms.class_remove') }}</h4>
                </div>
                <div class="modal-body bg-main light-text">
                    <div id="error-dc" class="alert alert-danger" style="display:none"></div>
                    <div id="success-dc" class="alert alert-success" style="display:none"></div>
                    <div class="form-group row flex-center light-text">
                        {{ __('administration_forms.other_remove_class') }}
                    </div>
                    <form>
                        @csrf
                        <input id="idClass" type="hidden" name="idClass" value="">
                    </form>
                    <div class="row flex-center">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">{{ __('administration_forms.button_close') }}</button>
                        <button id="removeClass-btn" class="btn bg-accent light-text">{{ __('administration_forms.button_confirm') }}</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
