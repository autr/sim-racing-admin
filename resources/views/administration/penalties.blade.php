@extends('administration')
@section('title')
    {{ $leagueInfo->name.' - '.$splitInfo->name.' - Manage penalties' }}
@endsection
@section('content')
    <div class="col-md-1 mb-4">
        <div class="card h-100">
            <div class="no-padding card-body">
                @include('nav.admin-nav')
            </div>
        </div>
    </div>
    <div class="col-md-11 mb-4">
        @if(session()->has('errorMessage'))
            <div class="alert alert-danger">
                {{ session()->get('errorMessage') }}
            </div>
        @endif
        @if(session()->has('message'))
            <div class="alert alert-success">
                {{ session()->get('message') }}
            </div>
        @endif
        <div id="messageFromSessionStorage" class="alert alert-success" style="display: none"></div>
        <div class="card">
            <div class="card-header pt-1 pb-1">
                <div class="d-flex justify-content-between">
                    <div class="mb-1 mt-2">
                        {{ __('administration.penalties_overview',
                            ['league' => $leagueInfo->name,
                             'split' => $splitInfo->name]) }}
                    </div>
                    <div>
                        <button type="button" class="btn bg-lighter light-text btn-sm mb-1 mt-1"
                                title="{{ __('administration.penalties_issue_penalty') }}"
                                data-toggle="modal"
                                data-target="#issuePenalty"
                                {{ (!isset($drivers) or count($drivers) == 0 or !isset($races) or count($races) == 0) ? 'disabled' : '' }}>
                            <i class="fas fa-plus"></i>
                            <span>{{ __('Add new penalty') }}</span>
                        </button>
                        <button id="collapseButton-pen-overview" class="btn btn-sm"
                                data-toggle="collapse"
                                data-target="#collapseOverview-pen"><i class="fas fa-eye-slash"></i></button>
                    </div>
                </div>
            </div>

            <div id="collapseOverview-pen" class="card-body collapse show">
                @if(!isset($penalties) or count($penalties) == 0)
                    {{ __('administration.penalties_empty') }}
                @else
                    <div class="table-responsive">
                        <table class="table table-hover table-borderless table-sm light-text">
                            <thead class="bg-accent-lighter light-text">
                            <tr>
                                <th scope="col">{{ __('administration.penalties_driver') }}</th>
                                <th scope="col">{{ __('administration.penalties_race') }}</th>
                                <th scope="col">{{ __('administration.penalties_type') }}</th>
                                <th scope="col">{{ __('administration.penalties_amount') }}</th>
                                <th scope="col">{{ __('administration.penalties_applied') }}</th>
                                <th scope="col"></th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($penalties as $penalty)
                                <tr>
                                    <td>
                                        {{ $penalty->driver->name }}
                                    </td>
                                    <td>
                                        {{ $penalty->race->track->name.', '.$penalty->race->track->country->code}}
                                    </td>
                                    <td>
                                        {{ $penalty->type->name }}
                                    </td>
                                    <td>
                                        @if(!is_null($penalty->amount))
                                            {{ $penalty->amount }}
                                        @else
                                            {{ __('administration.penalties_na') }}
                                        @endif
                                    </td>
                                    <td>
                                        @if ($penalty->applied)
                                            <i class="fas fa-check green-fa"></i>
                                        @else
                                            <i class="fas fa-times red-fa"></i>
                                        @endif
                                    </td>
                                    <td class="text-right">
                                        @if ($penalty->isApplicable() and $penalty->idPenaltyType == 2)
                                            <a href="{{ route('penalties.apply', [urlencode($league), $splitId, $penalty->id]) }}" class="btn bg-accent light-text btn-sm" title="{{ __('administration.penalties_apply') }}">
                                                <i class="fas fa-check-double"></i>
                                                <span>{{ __('Apply') }}</span>
                                            </a>
                                        @endif
                                        <a href="{{ route('penalties.makeApplied', [urlencode($league), $splitId, $penalty->id]) }}" class="btn bg-accent light-text btn-sm" title="{{ __('administration.penalties_change_applied') }}">
                                            <span>{{ __('Change to ') }}</span>
                                            <i class="fas fa-check"></i>
                                        </a>
                                        <a href="{{ route('penalties.makeNotApplied', [urlencode($league), $splitId, $penalty->id]) }}" class="btn bg-accent light-text btn-sm" title="{{ __('administration.penalties_change_not_applied') }}">
                                            <span>{{ __('Change to ') }}</span>
                                            <i class="fas fa-times"></i>
                                        </a>
                                        <button type="button" class="btn btn-danger btn-sm" title="{{ __('administration.penalties_remove') }}"
                                                data-toggle="modal"
                                                data-target="#removePenalty"
                                                data-penaltyid="{{ $penalty->id }}">
                                            <i class="fas fa-trash"></i>
                                            <span>{{ __('Remove') }}</span>
                                        </button>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                @endif
            </div>
            <div class="card-header pt-1 pb-1">
                <div class="d-flex justify-content-between">
                    <div class="mb-1 mt-2">
                        {{ __('administration.penalties_points_overview',
                            ['league' => $leagueInfo->name,
                             'split' => $splitInfo->name]) }}
                    </div>
                    <div>
                        <button type="button" class="btn bg-lighter light-text btn-sm mb-1 mt-1"
                                title="{{ __('administration.penalties_add_points') }}"
                                data-toggle="modal"
                                data-target="#addPenaltyPoints"
                                {{ (!isset($drivers) or count($drivers) == 0 or !isset($racesPP) or count($racesPP) == 0) ? 'disabled' : '' }}>
                            <i class="fas fa-plus"></i>
                            <span>{{ __('Add new penalty points') }}</span>
                        </button>
                        <button id="collapseButton-penp-overview" class="btn btn-sm"
                                data-toggle="collapse"
                                data-target="#collapseOverview-penp"><i class="fas fa-eye-slash"></i></button>
                    </div>
                </div>
            </div>
            <div id="collapseOverview-penp" class="card-body collapse show">
                @if(!isset($penaltyPoints) or count($penaltyPoints) == 0)
                    {{ __('administration.penalties_points_empty') }}
                @else
                    <div class="table-responsive">
                        <table class="table table-hover table-borderless table-sm light-text">
                            <thead class="bg-accent-lighter light-text">
                            <tr>
                                <th scope="col">{{ __('administration.penalties_driver') }}</th>
                                <th scope="col">{{ __('administration.penalties_amount') }}</th>
                                <th scope="col">{{ __('administration.penalties_newest') }}</th>
                                <th scope="col">{{ __('administration.penalties_oldest') }}</th>
                                <th scope="col"></th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($penaltyPoints as $penaltyPoint)
                                <tr>
                                    <td>
                                        {{ $penaltyPoint->driver->name }}
                                    </td>
                                    <td>
                                        {{ $penaltyPoint->overall_amount }}
                                    </td>
                                    <td>
                                        {{ \Carbon\Carbon::parse($penaltyPoint->newest)->format('d F Y') }}
                                    </td>
                                    <td>
                                        {{ \Carbon\Carbon::parse($penaltyPoint->oldest)->format('d F Y') }}
                                    </td>
                                    <td class="text-right">
                                        <button type="button" class="btn btn-danger btn-sm" title="Remove the oldest penalty points"
                                                data-toggle="modal"
                                                data-target="#removePoints"
                                                data-driverid="{{ $penaltyPoint->driver->id }}">
                                            <i class="fas fa-trash"></i>
                                            <span>{{ __('Remove oldest') }}</span>
                                        </button>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                @endif
            </div>
        </div>
    </div>

    <!-- Modal for issuing penalties -->
    <div id="issuePenalty" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="issuePenalty"
         aria-hidden="true">
        <div class="modal-dialog modal-lg modal-notify">
            <div class="modal-content">
                <div class="modal-header bg-accent-lighter light-text">
                    <h4 class="modal-title">{{ __('administration.penalties_issue_penalty') }}</h4>
                </div>
                @include('forms.newPenalty')
            </div>
        </div>
    </div>

    <!-- Modal for adding penalty points -->
    <div id="addPenaltyPoints" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="addPenaltyPoints"
         aria-hidden="true">
        <div class="modal-dialog modal-lg modal-notify">
            <div class="modal-content">
                <div class="modal-header bg-accent-lighter light-text">
                    <h4 class="modal-title">{{ __('administration.penalties_add_points') }}</h4>
                </div>
                @include('forms.newPenaltyPoints')
            </div>
        </div>
    </div>

    <!-- Modal for removing penalties -->
    <div id="removePenalty" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="removePenalty"
         aria-hidden="true">
        <div class="modal-dialog modal-lg modal-notify">
            <div class="modal-content">
                <div class="modal-header bg-accent-lighter light-text">
                    <h4 class="modal-title">{{ __('administration_forms.penalty_remove') }}</h4>
                </div>
                <div class="modal-body bg-main light-text">
                    <div id="error-rp" class="alert alert-danger" style="display:none"></div>
                    <div id="success-rp" class="alert alert-success" style="display:none"></div>
                    <div class="form-group row flex-center light-text">
                        {{ __('administration_forms.other_remove_penalty') }}
                    </div>
                    <form>
                        @csrf
                        <input id="idPenalty-rp" type="hidden" name="idPenalty" value="">
                    </form>
                    <div class="row flex-center">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">{{ __('administration_forms.button_close') }}</button>
                        <button id="removePenalty-btn" class="btn bg-accent light-text">{{ __('administration_forms.button_confirm') }}</button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal for removing oldest penalty points -->
    <div id="removePoints" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="removePoints"
         aria-hidden="true">
        <div class="modal-dialog modal-lg modal-notify">
            <div class="modal-content">
                <div class="modal-header bg-accent-lighter light-text">
                    <h4 class="modal-title">{{ __('administration_forms.penalty_remove_points') }}</h4>
                </div>
                <div class="modal-body bg-main light-text">
                    <div id="error-rpp" class="alert alert-danger" style="display:none"></div>
                    <div id="success-rpp" class="alert alert-success" style="display:none"></div>
                    <div class="form-group row flex-center light-text">
                        {{ __('administration_forms.other_remove_points') }}
                    </div>
                    <form>
                        @csrf
                        <input id="idDriver-rpp" type="hidden" name="idDriver" value="">
                    </form>
                    <div class="row flex-center">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">{{ __('administration_forms.button_close') }}</button>
                        <button id="removePoints-btn" class="btn bg-accent light-text">{{ __('administration_forms.button_confirm') }}</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
