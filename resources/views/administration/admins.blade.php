@extends('administration')
@section('title')
    {{ $leagueInfo->name.' - '.$splitInfo->name.' - Manage admins' }}
@endsection
@section('content')
    <div class="col-md-1 mb-4">
        <div class="card h-100">
            <div class="no-padding card-body">
                @include('nav.admin-nav')
            </div>
        </div>
    </div>
    <div class="col-md-11 mb-4">
        @if(session()->has('errorMessage'))
            <div class="alert alert-danger">
                {{ session()->get('errorMessage') }}
            </div>
        @endif
        @if(session()->has('message'))
            <div class="alert alert-success">
                {{ session()->get('message') }}
            </div>
        @endif
        <div id="messageFromSessionStorage" class="alert alert-success" style="display: none"></div>
        <div class="card h-100">
            <div class="card-body">
                @if (!isset($admins) or count($admins) == 0)
                    <div>
                        {{ __('There are currently no admins for this split') }}
                    </div>
                @else
                    <div class="table-responsive">
                        <table class="table table-hover table-borderless table-sm light-text">
                            <thead class="bg-accent-lighter light-text">
                            <tr>
                                <th scope="col">{{ __('Name') }}</th>
                                <th scope="col"></th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($admins as $admin)
                                <tr>
                                    <td>{{ $admin->name }}</td>
                                    <td class="text-right">
                                        <button type="button" class="btn bg-accent light-text btn-sm"
                                                data-toggle="modal"
                                                data-target="#transferSuperadmin"
                                                data-adminid="{{ $admin->id }}"
                                                data-splitid="{{ $splitId }}"
                                                data-league="{{ urlencode($league) }}">Transfer superadmin rights</button>
                                        <button type="button" class="btn btn-danger btn-sm"
                                                data-toggle="modal"
                                                data-target="#revokePermission"
                                                data-adminid="{{ $admin->id }}"
                                                data-splitid="{{ $splitId }}"
                                                data-league="{{ urlencode($league) }}">Revoke permission</button>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                @endif
            </div>
        </div>
    </div>

    <!-- Modal for superadmin transfer confirmation -->
    <div id="transferSuperadmin" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="transferSuperadmin"
         aria-hidden="true">
        <div class="modal-dialog modal-lg modal-notify">
            <div class="modal-content">
                <div class="modal-header bg-accent-lighter light-text">
                    <h4 class="modal-title">{{ __('administration_forms.admin_transfer') }}</h4>
                </div>
                <div class="modal-body bg-main light-text">
                    <div class="form-group row flex-center light-text">
                        {{ __('administration_forms.other_transfer') }}
                    </div>
                    <div class="row flex-center">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">{{ __('administration_forms.button_close') }}</button>
                        <a id="transferSuperadmin-btn" href="" class="btn bg-accent light-text" role="button">{{ __('administration_forms.button_confirm') }}</a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal for revoke permission confirmation -->
    <div id="revokePermission" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="revokePermission"
         aria-hidden="true">
        <div class="modal-dialog modal-lg modal-notify">
            <div class="modal-content">
                <div class="modal-header bg-accent-lighter light-text">
                    <h4 class="modal-title">{{ __('administration_forms.admin_revoke') }}</h4>
                </div>
                <div class="modal-body bg-main light-text">
                    <div class="form-group row flex-center light-text">
                        {{ __('administration_forms.other_revoke') }}
                    </div>
                    <div class="row flex-center">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">{{ __('administration_forms.button_close') }}</button>
                        <a id="revokePermission-btn" href="" class="btn bg-accent light-text" role="button">{{ __('administration_forms.button_confirm') }}</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
