@extends('home')

@section('title')
    {{ __('Guide') }}
@endsection
@section('content')
    <button onclick="topFunction()" id="to-top-button" title="Go to top">Go to top</button>
    <div class="col-md-2 mb-4">
        <div class="card h-100">
            <div class="no-padding card-body">
                @include('nav.guide-nav')
            </div>
        </div>
    </div>
    <div class="col-md-10 mb-4">
        <div class="card guide-card">
            <div id="getting-started" class="card-header">{{ __('Getting started') }}</div>
            <div class="card-body">
                <h5>Registration</h5>
                <p>To start using the app as an administrator you need to register.<br>
                    You can either create a new account using the registration form (1) or you can sign in with your Google account (2). Regular users don't have to register.</p>
                <img class="w-100" src="{{ asset('guide-img/register.png') }}" alt="Registration form">
                <hr>
                <h5>Login</h5>
                <p>After you register to the app you can either log in with your created account using login form (1) or you can log in with your Google account (2).</p>
                <img class="w-100" src="{{ asset('guide-img/login.png') }}" alt="Login form">
                <hr>
                <h5>Basic navigation</h5>
                <p>After you log in you'll be greeted with an administration page. Through the top navigation, you have access to: </p>
                <ol>
                    <li>Your home page</li>
                    <li>League browser</li>
                    <li>League creation</li>
                    <li>Your leagues</li>
                    <li>Your permission requests</li>
                    <li>Bug reporting</li>
                    <li>This guide</li>
                    <li>Feedback survey</li>
                    <li>Discord server</li>
                    <li>Log out link</li>
                </ol>
                <img class="w-100" src="{{ asset('guide-img/navigation.png') }}" alt="Navigation">
            </div>
        </div>
        <div class="card guide-card mt-4">
            <div id="leagues" class="card-header">{{ __('Leagues') }}</div>
            <div class="card-body">
                <h5>League creation</h5>
                <p>You can access the league creation form using the "Create new league" button in top navigation.<br>
                    The form consists of:</p>
                <dl>
                    <dt>League name</dt>
                    <dd class="pl-4">A unique name for your league.</dd>
                    <dt>League description</dt>
                    <dd class="pl-4">A short description for your league.</dd>
                    <dt>Split name</dt>
                    <dd class="pl-4">A name for a league split, e.g. F1, F2, F3.</dd>
                    <dt>Platform</dt>
                    <dd class="pl-4">A gaming platform the league uses.</dd>
                    <dt>Game</dt>
                    <dd class="pl-4">A game the league uses.</dd>
                    <dt>Region</dt>
                    <dd class="pl-4">A world region where the league takes place.</dd>
                    <dt>Maximum drivers per team</dt>
                    <dd class="pl-4">A maximum number of drivers that can be assigned to a team (0 for unspecified).</dd>
                    <dt>Maximum drivers per race</dt>
                    <dd class="pl-4">A maximum drivers per race. The option is then used in result management to restrict the results.</dd>
                    <dt>Points</dt>
                    <dd class="pl-4">A points string that will be automatically used in result management. Points should be formatted from highest to lowest and have to be separated by a comma without any blank spaces, e.g. 25,18,15,10,6,4,2,1.</dd>
                    <dt>Initial season</dt>
                    <dd class="pl-4">Initial season of the league in the app. For situation when the league existed before this tool was created.</dd>
                </dl>
                <hr>
                <h5>League management</h5>
                <p>You can access the league management screen using the "My leagues" button in top navigation and clicking the league's name.<br>
                    The league management screen consists of:</p>
                <ol>
                    <li>League overview where you can change the league description</li>
                    <li>Split overview</li>
                </ol>
                <p>You can access the split management screen using the "eye" button (3) in the split overview. You can create a new split using the "plus" button (4) in the left navigation.</p>
                <img class="w-100" src="{{ asset('guide-img/league-management.png') }}" alt="League management">
                <hr>
                <h5>Split management</h5>
                <p>First thing you can see in the split management is split overview with information about the split (10). You can update the split information by clicking the "Update split information" button (11). You can also start a new season by clicking the "Start new season" button (12), which also allows you to transfer current driver lineups. You can also get a link to the split page by copying it manualy or by clicking the "Copy to clipboard" button (13).</p>
                <p>On the left-hand side there is a navigation menu with following options:</p>
                <ol>
                    <li>Back to league management</li>
                    <li>Add new split</li>
                    <li>Split settings/overview</li>
                    <li>Driver management</li>
                    <li>Team management</li>
                    <li>Racing calendar</li>
                    <li>Penalty management</li>
                    <li>Result management</li>
                    <li>Admin management (available for league's superadmin)</li>
                </ol>
                <img class="w-100" src="{{ asset('guide-img/split-management.png') }}" alt="Split management">
            </div>
        </div>
        <div class="card guide-card mt-4">
            <div id="drivers" class="card-header">{{ __('Drivers') }}</div>
            <div class="card-body">
                <h5>Driver overview</h5>
                <p>Driver management screen gives you access to a driver overview. Through this screen you can add drivers (1), edit drivers (2) and make drivers inactive (3).</p>
                <p>Inactive drivers can't be put in a team and won't appear in result management.</p>
                <img class="w-100" src="{{ asset('guide-img/driver-management.png') }}" alt="Driver management">
                <hr>
                <h5>Add driver(s)</h5>
                <p>After clicking the button for adding drivers a window with a simple form will appear. You can add one or multiple drivers by adding them to a new line.</p>
                <div class="w-100 text-center">
                    <img class="w-50 mx-auto" src="{{ asset('guide-img/driver-add.png') }}" alt="Add drivers">
                </div>
                <hr>
                <h5>Update driver</h5>
                <p>After clicking the button for updating a driver a window with multiple options will appear. You can change driver's name, driver's team and make the driver inactive.</p>
                <div class="w-100 text-center">
                    <img class="w-50 mx-auto" src="{{ asset('guide-img/driver-update.png') }}" alt="Update drivers">
                </div>
                <hr>
                <h5>Load drivers and teams from file</h5>
                <p>You can add drivers and teams by loading a CSV file (1). Format examples are under the file form (2).</p>
                <img class="w-100" src="{{ asset('guide-img/drivers-file.png') }}" alt="Load from file">
            </div>
        </div>
        <div class="card guide-card mt-4">
            <div id="teams" class="card-header">{{ __('Teams') }}</div>
            <div class="card-body">
                <h5>Team overview</h5>
                <p>Team management screen gives you access to a team overview. Through this screen you can add teams (1) and edit teams (2).</p>
                <img class="w-100" src="{{ asset('guide-img/team-management.png') }}" alt="Team management">
                <hr>
                <h5>Add team(s)</h5>
                <p>After clicking the button for adding teams a window with a simple form will appear. This form works the same way driver's is and you can add one or multiple teams by adding them to a new line.</p>
                <div class="w-100 text-center">
                    <img class="w-50 mx-auto" src="{{ asset('guide-img/team-add.png') }}" alt="Add teams">
                </div>
                <hr>
                <h5>Update team</h5>
                <p>After clicking the button for updating a team a window with multiple options will appear. You can change team's name and its colours. The primary colour is mandatory, secondary colours are at disposal to make team colours more unique.</p>
                <div class="w-100 text-center">
                    <img class="w-50 mx-auto" src="{{ asset('guide-img/team-update.png') }}" alt="Update teams">
                </div>
            </div>
        </div>
        <div class="card guide-card mt-4">
            <div id="calendar" class="card-header">{{ __('Calendar') }}</div>
            <div class="card-body">
                <h5>Calendar overview</h5>
                <p>Calendar management screen gives you access to a calendar overview. Through this screen you can add existing tracks (1), add new tracks (2), update race information (3) and remove the race (4).
                When removing a race, every part of the split linked to the race (results, penalties, etc.) will be removed as well.</p>
                <img class="w-100" src="{{ asset('guide-img/calendar-management.png') }}" alt="Calendar management">
                <hr>
                <h5>Add existing track(s)</h5>
                <p>After clicking the button for adding existing tracks a window with a simple form will appear. You can pick and add one or multiple tracks (using CTRL or SHIFT) into the calendar. The tracks can be filtered by country.</p>
                <div class="w-100 text-center">
                    <img class="w-50 mx-auto" src="{{ asset('guide-img/calendar-existing.png') }}" alt="Calendar add existing">
                </div>
                <hr>
                <h5>Add new track</h5>
                <p>After clicking the button for adding a new track a window with multiple options will appear. You can then fill in the track name and pick a country.</p>
                <div class="w-100 text-center">
                    <img class="w-50 mx-auto" src="{{ asset('guide-img/calendar-new.png') }}" alt="Calendar add new">
                </div>
                <hr>
                <h5>Update race information</h5>
                <p>After clicking the button for updating race information a window will appear. You can change race's date.</p>
                <div class="w-100 text-center">
                    <img class="w-50 mx-auto" src="{{ asset('guide-img/calendar-update.png') }}" alt="Update race">
                </div>
            </div>
        </div>
        <div class="card guide-card mt-4">
            <div id="penalties" class="card-header">{{ __('Penalties') }}</div>
            <div class="card-body">
                <h5>Penalty overview</h5>
                <p>Penalty management screen gives you access to penalty and penalty points overview. Through this screen you can issue penalties (1), change status to applied (2) or not applied (3), remove penalty (4).</p>
                <p>You can also automatically apply a grid penalty to the results (5). This action cannot be reversed at the moment, so you'll have to manually alter the results to undo the penalty application.</p>
                <p>In the second half of the overview you can add penalty points (6) and remove oldest issued penalty points (7).</p>
                <img class="w-100" src="{{ asset('guide-img/penalty-management.png') }}" alt="Penalty management">
                <hr>
                <h5>Issue a penalty</h5>
                <p>After clicking the button for issuing penalties a window with a simple form will appear. You can pick a combo of a driver, a race for which the penalty is being issued, penalty type and amount (if applicable).</p>
                <div class="w-100 text-center">
                    <img class="w-50 mx-auto" src="{{ asset('guide-img/penalty-new.png') }}" alt="Issue penalty">
                </div>
                <hr>
                <h5>Issue penalty points</h5>
                <p>After clicking the button for issuing penalty points a window with multiple options will appear. You can pick a driver, a race in which the penalty points were issued and amount.</p>
                <div class="w-100 text-center">
                    <img class="w-50 mx-auto" src="{{ asset('guide-img/penalty-points.png') }}" alt="Issue penalty points">
                </div>
            </div>
        </div>
        <div class="card guide-card mt-4">
            <div id="results" class="card-header">{{ __('Results') }}</div>
            <div class="card-body">
                <h5>Result management</h5>
                <p>Result management screen gives you access to result creation and result modification. After selecting a race (1), you can add drivers from "Available drivers for selected race" into the "Race results" by clicking the plus button (4). You can also remove the driver from results by clicking the minus button (7).</p>
                <p>In the results you have an option to mark the driver as DNF (5) and you have an option to add (positive and negative) bonus points (6).</p>
                <p>You can save the results by clicking the "Save results" button (8).</p>
                <img class="w-100 mb-4" src="{{ asset('guide-img/result-management.png') }}" alt="Result management">
                <div class="w-100 text-center">
                    <video controls>
                        <source src="{{ asset('guide-img/results.webm') }}" type="video/webm">
                        Your browser does not support the video tag.
                    </video>
                </div>
            </div>
        </div>
        <div class="card guide-card mt-4">
            <div id="admins" class="card-header">{{ __('Admins') }}</div>
            <div class="card-body">
                <h5>Requesting permission</h5>
                <p>You can request permission to access league/split via browsing screen by clicking the "Request permission" button (1).</p>
                <img class="w-100" src="{{ asset('guide-img/admins-request-p.png') }}" alt="Requesting permission">
                <hr>
                <h5>User's requests</h5>
                <p>You can check status of your requests in "My requests" section.</p>
                <img class="w-100" src="{{ asset('guide-img/admins-requests.png') }}" alt="User's requests">
                <hr>
                <h5>Permission approval</h5>
                <p>As a superadmin of the league/split you can approve (1)/dissaprove (2) user's request via home dashboard.</p>
                <img class="w-100" src="{{ asset('guide-img/admins-approval.png') }}" alt="Permission approval">
                <hr>
                <h5>Admin overview</h5>
                <p>For every split there is admin overview page, where you can transfer superadmin rights (1) or revoke permission (2) for selected admin.</p>
                <img class="w-100" src="{{ asset('guide-img/admins-overview.png') }}" alt="Admin overview">
            </div>
        </div>
        <div class="card guide-card mt-4">
            <div id="browsing" class="card-header">{{ __('Browsing') }}</div>
            <div class="card-body">
                <h5>Browser</h5>
                <p>For searching leagues there is a simple browser. You can search via the search field (1) by:</p>
                <ul>
                    <li>League's name</li>
                    <li>Split's name</li>
                    <li>Platform</li>
                    <li>Game</li>
                    <li>Region</li>
                </ul>
                <p>You can access the league/split page by clicking the "View" button (2).</p>
                <img class="w-100" src="{{ asset('guide-img/browsing-browser.png') }}" alt="Browser">
                <hr>
                <h5>League/split page</h5>
                <p>On the league page you can access split overview (1), driver lineups (2), racing calendar (3) and standings (4).</p>
                <img class="w-100" src="{{ asset('guide-img/browsing-league.png') }}" alt="League page">
            </div>
        </div>
    </div>
    <script>
        //Get the button
        var toTopButton = document.getElementById("to-top-button");

        // When the user scrolls down 20px from the top of the document, show the button
        window.onscroll = function() {scrollFunction()};

        function scrollFunction() {
            if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
                toTopButton.style.display = "block";
            } else {
                toTopButton.style.display = "none";
            }
        }

        // When the user clicks on the button, scroll to the top of the document
        function topFunction() {
            document.body.scrollTop = 0;
            document.documentElement.scrollTop = 0;
        }
    </script>
@endsection
