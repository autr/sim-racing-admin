@extends('layouts.app')
@section('title')
    {{ __('Sign in') }}
@endsection
@section('content')
<div class="col-md-6 mb-4">
    <div class="card">
        <div class="card-body">
            <form class="text-center" method="POST" action="{{ route('login') }}">
                @csrf

                <!-- Email -->
                <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required autofocus placeholder="E-mail" aria-describedby="signInEmail">
                @if ($errors->has('email'))
                    <small id="signInEmail" class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('email') }}</strong>
                    </small>
                @endif

                <!-- Password -->
                <input id="password" type="password" class="form-control mt-4{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required placeholder="Password" aria-describedby="signInPassword">
                @if ($errors->has('password'))
                    <small id="signInPassword" class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('password') }}</strong>
                    </small>
                @endif

                <div class="d-flex justify-content-around mt-4">
                    <div>
                        <!-- Remember me -->
                        <div class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
                            <label class="custom-control-label" for="remember">Remember me</label>
                        </div>
                    </div>
                    <div>
                        @if (Route::has('password.request'))
                            <div>
                                <!-- Forgot password -->
                                <a class="light-text" href="{{ route('password.request') }}">{{ __('Forgot password?') }}</a>
                            </div>
                        @endif
                    </div>
                </div>

                <!-- Sign in button -->
                <button class="btn bg-accent light-text btn-block my-4" type="submit">Sign in</button>

                <!-- Social login -->
                <p>or sign in with:</p>

                <a href="{{ route('googleRedirect') }}" class="light-green-text mx-2 google-link">
                    <i class="fab fa-google"></i>
                </a>
            </form>
        </div>
    </div>
</div>
@endsection
