@extends('layouts.app')
@section('title')
    {{ __('Register') }}
@endsection
@section('content')
<div class="col-md-6 mb-4">
    <div class="card">
        <div class="card-body">
            <form class="text-center" method="POST" action="{{ route('register') }}">
                @csrf

                <!-- Name -->
                <input type="text" id="name" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old('name') }}" required autofocus placeholder="Name">
                @if ($errors->has('name'))
                    <small class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('name') }}</strong>
                    </small>
                @endif

                <!-- E-mail -->
                <input id="email" type="email" class="form-control mt-4{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required placeholder="E-mail">
                @if ($errors->has('email'))
                    <small class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('email') }}</strong>
                    </small>
                @endif

                <!-- Password -->
                <input id="password" type="password" class="form-control mt-4{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required placeholder="Password">
                @if ($errors->has('password'))
                    <small class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('password') }}</strong>
                    </small>
                @endif

                <!-- Confirm password -->
                <input id="password-confirm" type="password" class="form-control mt-4" name="password_confirmation" required placeholder="Confirm password">

                <!-- Sign up button -->
                <button class="btn bg-accent light-text my-4 btn-block" type="submit">Sign in</button>

                <!-- Social register -->
                <p>or sign up with:</p>

                <a href="{{ route('googleRedirect') }}" class="light-green-text mx-2 google-link">
                    <i class="fab fa-google"></i>
                </a>
            </form>
        </div>
    </div>
</div>
@endsection
