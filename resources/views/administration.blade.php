@extends('layouts.app')

@section('title')
    @if (isset($splitInfo))
        {{ $leagueInfo->name.' - '.$splitInfo->name }}
    @else
        {{ $leagueInfo->name }}
    @endif
@endsection
@section('navigation')
    @include('nav.main-nav')
@endsection
