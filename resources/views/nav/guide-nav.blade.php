<ul class="sidenav">
    <li class="nav-item">
        <a class="nav-link" href="#getting-started" title="{{ __('Getting started') }}">{{ __('Getting started') }}</a>
    </li>
    <li class="nav-item">
        <a class="nav-link" href="#leagues" title="{{ __('Leagues') }}">{{ __('Leagues') }}</a>
    </li>
    <li class="nav-item">
        <a class="nav-link" href="#drivers" title="{{ __('Drivers') }}">{{ __('Drivers') }}</a>
    </li>
    <li class="nav-item">
        <a class="nav-link" href="#teams" title="{{ __('Teams') }}">{{ __('Teams') }}</a>
    </li>
    <li class="nav-item">
        <a class="nav-link" href="#calendar" title="{{ __('Calendar') }}">{{ __('Calendar') }}</a>
    </li>
    <li class="nav-item">
        <a class="nav-link" href="#penalties" title="{{ __('Penalties') }}">{{ __('Penalties') }}</a>
    </li>
    <li class="nav-item">
        <a class="nav-link" href="#results" title="{{ __('Results') }}">{{ __('Results') }}</a>
    </li>
    <li class="nav-item">
        <a class="nav-link" href="#admins" title="{{ __('Admins') }}">{{ __('Admins') }}</a>
    </li>
    <li class="nav-item">
        <a class="nav-link" href="#browsing" title="{{ __('Browsing') }}">{{ __('Browsing') }}</a>
    </li>
</ul>
