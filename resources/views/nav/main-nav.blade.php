@auth
    <li class="nav-item">
        <a class="nav-link" href="{{ url('home') }}"><i class="fas fa-home"></i></a>
    </li>
@endif
<li class="nav-item">
    <a class="nav-link" href="{{ url('browse') }}">{{ __('Browse') }}</a>
</li>
@auth
    <li class="nav-item">
        <a class="nav-link" href="{{ url('new-league') }}">{{ __('Create new league') }}</a>
    </li>
    @if (isset($leagues) and !is_null($leagues))
        <li class="nav-item dropdown">
            <a id="leaguesDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                {{ __('My leagues') }} <span class="caret"></span>
            </a>

            <div class="dropdown-menu" aria-labelledby="leaguesDropdown">
                @if (count($leagues) === 0)
                    <span class="dropdown-item">No leagues available</span>
                @else
                    @foreach ($leagues as $l)
                        <a class="dropdown-item" href="{{ url('a/'.urlencode($l->uri)) }}">
                            {{ $l->name }}
                        </a>
                    @endforeach
                @endif
            </div>
        </li>
    @endif
    <li class="nav-item">
        <a class="nav-link" href="{{ url('requests') }}">{{ __('My requests') }}</a>
    </li>
@endif
