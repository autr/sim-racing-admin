<ul class="sidenav">
@if(isset($splitId) or Route::currentRouteName() === 'split.create')
    <li class="nav-item">
        <a class="nav-link admin-nav" href="{{ url('a/'.urlencode($league)) }}" title="{{ __('Back to league overview') }}">
            <i class="fas fa-chevron-circle-left"></i>
            <div class="admin-nav-text">{{ __('League') }}</div>
        </a>
    </li>
@endif
@if ($superAdmin)
    <li class="nav-item">
        <a class="nav-link admin-nav" href="{{ url('a/'.urlencode($league).'/new-split') }}" title="{{ __('New split') }}">
            <i class="fas fa-plus"></i>
            <div class="admin-nav-text">{{ __('New split') }}</div>
        </a>
    </li>
@endif
@if(isset($splitId))
    <li class="nav-item">
        <a class="nav-link admin-nav {{ Route::currentRouteName() === 'split.index' ? 'active' : '' }}" href="{{ url('a/'.urlencode($league).'/'.$splitId.'/home') }}" title="{{ __('Split settings') }}">
            <i class="fas fa-cog"></i>
            <div class="admin-nav-text">{{ __('Settings') }}</div>
        </a>
    </li>
    <li class="nav-item">
        <a class="nav-link admin-nav {{ Route::currentRouteName() === 'drivers.createFromFile' ? 'active' : '' }}" href="{{ url('a/'.urlencode($league).'/'.$splitId.'/load-file') }}" title="{{ __('Load from file') }}">
            <i class="fas fa-upload"></i>
            <div class="admin-nav-text">{{ __('Load file') }}</div>
        </a>
    </li>
    <li class="nav-item">
        <a class="nav-link admin-nav {{ Route::currentRouteName() === 'drivers.index' ? 'active' : '' }}" href="{{ url('a/'.urlencode($league).'/'.$splitId.'/drivers') }}" title="{{ __('Drivers') }}">
            <i class="fas fa-users"></i>
            <div class="admin-nav-text">{{ __('Drivers') }}</div>
        </a>
    </li>
    <li class="nav-item">
        <a class="nav-link admin-nav {{ Route::currentRouteName() === 'teams.index' ? 'active' : '' }}" href="{{ url('a/'.urlencode($league).'/'.$splitId.'/teams') }}" title="{{ __('Teams') }}">
            <i class="fas fa-car"></i>
            <div class="admin-nav-text">{{ __('Teams') }}</div>
        </a>
    </li>
    <li class="nav-item">
        <a class="nav-link admin-nav {{ Route::currentRouteName() === 'calendar.index' ? 'active' : '' }}" href="{{ url('a/'.urlencode($league).'/'.$splitId.'/calendar') }}" title="{{ __('Calendar') }}">
            <i class="fas fa-calendar-alt"></i>
            <div class="admin-nav-text">{{ __('Calendar') }}</div>
        </a>
    </li>
    <li class="nav-item">
        <a class="nav-link admin-nav {{ Route::currentRouteName() === 'penalties.index' ? 'active' : '' }}" href="{{ url('a/'.urlencode($league).'/'.$splitId.'/penalties') }}" title="{{ __('Penalties') }}">
            <i class="fas fa-exclamation-circle"></i>
            <div class="admin-nav-text">{{ __('Penalties') }}</div>
        </a>
    </li>
    <li class="nav-item">
        <a class="nav-link admin-nav {{ Route::currentRouteName() === 'results.index' ? 'active' : '' }}" href="{{ url('a/'.urlencode($league).'/'.$splitId.'/results') }}" title="{{ __('Results') }}">
            <i class="fas fa-poll-h"></i>
            <div class="admin-nav-text">{{ __('Results') }}</div>
        </a>
    </li>
    @if ($superAdmin > 0)
        <li class="nav-item">
            <a class="nav-link admin-nav {{ Route::currentRouteName() === 'manageAdmins' ? 'active' : '' }}" href="{{ url('a/'.urlencode($league).'/'.$splitId.'/admins') }}" title="{{ __('Admins') }}">
                <i class="fas fa-users-cog"></i>
                <div class="admin-nav-text">{{ __('Admins') }}</div>
            </a>
        </li>
    @endif
@endif
</ul>
