<ul class="sidenav">
    <li class="nav-item">
        <a class="nav-link admin-nav {{ Route::currentRouteName() === 'presentation.index' ? 'active' : '' }}" href="{{ url('v/'.urlencode($league).'/'.$splitId) }}" title="{{ __('League overview') }}">
            <i class="fas fa-home"></i>
            <div class="admin-nav-text">{{ __('Home') }}</div>
        </a>
    </li>
    <li class="nav-item">
        <a class="nav-link admin-nav {{ Route::currentRouteName() === 'presentation.description' ? 'active' : '' }}" href="{{ url('v/'.urlencode($league).'/'.$splitId.'/description') }}" title="{{ __('League description') }}">
            <i class="fas fa-info-circle"></i>
            <div class="admin-nav-text">{{ __('Description') }}</div>
        </a>
    </li>
    <li class="nav-item">
        <a class="nav-link admin-nav {{ Route::currentRouteName() === 'presentation.lineups' ? 'active' : '' }}" href="{{ url('v/'.urlencode($league).'/'.$splitId.'/lineups') }}" title="{{ __('Lineups') }}">
            <i class="fas fa-users"></i>
            <div class="admin-nav-text">{{ __('Lineups') }}</div>
        </a>
    </li>
    <li class="nav-item">
        <a class="nav-link admin-nav {{ Route::currentRouteName() === 'presentation.calendar' ? 'active' : '' }}" href="{{ url('v/'.urlencode($league).'/'.$splitId.'/calendar') }}" title="{{ __('Calendar') }}">
            <i class="fas fa-calendar-alt"></i>
            <div class="admin-nav-text">{{ __('Calendar') }}</div>
        </a>
    </li>
    <li class="nav-item">
        <a class="nav-link admin-nav {{ Route::currentRouteName() === 'presentation.penalties' ? 'active' : '' }}" href="{{ url('v/'.urlencode($league).'/'.$splitId.'/penalties') }}" title="{{ __('Penalties') }}">
            <i class="fas fa-exclamation-circle"></i>
            <div class="admin-nav-text">{{ __('Penalties') }}</div>
        </a>
    </li>
    <li class="nav-item">
        <a class="nav-link admin-nav {{ Route::currentRouteName() === 'presentation.standings' ? 'active' : '' }}" href="{{ url('v/'.urlencode($league).'/'.$splitId.'/standings') }}" title="{{ __('Driver standings') }}">
            <i class="fas fa-trophy"></i>
            <div class="admin-nav-text">{{ __('Driver standings') }}</div>
        </a>
    </li>
    <li class="nav-item">
        <a class="nav-link admin-nav {{ Route::currentRouteName() === 'presentation.teamStandings' ? 'active' : '' }}" href="{{ url('v/'.urlencode($league).'/'.$splitId.'/team-standings') }}" title="{{ __('Team standings') }}">
            <i class="fas fa-trophy"></i>
            <div class="admin-nav-text">{{ __('Team standings') }}</div>
        </a>
    </li>
</ul>
