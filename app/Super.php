<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Super extends Model {
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'idLeague', 'idAdmin'
    ];
}
