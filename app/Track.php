<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Track extends Model {
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'idCountry',
    ];

    /**
     * Returns country for selected track
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function country() {
        return $this->belongsTo('App\Country', 'idCountry', 'code');
    }
}
