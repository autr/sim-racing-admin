<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Result extends Model {
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'idDriver', 'idRace', 'idTeam', 'position', 'points', 'classPoints', 'bonusPoints', 'didNotFinish', 'disqualified'
    ];

    /**
     * Returns team for selected result record
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function team() {
        return $this->belongsTo('App\Team', 'idTeam');
    }

    /**
     * Returns driver for selected result record
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function driver() {
        return $this->belongsTo('App\Driver', 'idDriver');
    }

    /**
     * Returns race for selected result record
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function race() {
        return $this->belongsTo('App\Race', 'idRace');
    }
}
