<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Request extends Model {
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'idSplit', 'idUser', 'status',
    ];

    /**
     * Returns user for selected request
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user() {
        return $this->belongsTo('App\User', 'idUser');
    }

    /**
     * Returns split for selected request
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function split() {
        return $this->belongsTo('App\Split', 'idSplit');
    }
}
