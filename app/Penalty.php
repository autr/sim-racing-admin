<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Penalty extends Model {
    protected $table = 'penalties';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'idDriver', 'idRace', 'idPenaltyType', 'amount', 'applied'
    ];

    /**
     * Returns driver for selected penalty
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function driver() {
        return $this->belongsTo('App\Driver', 'idDriver');
    }

    /**
     * Returns race for selected penalty
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function race() {
        return $this->belongsTo('App\Race', 'idRace');
    }

    /**
     * Returns penalty type of selected penalty
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function type() {
        return $this->belongsTo('App\Penalty_type', 'idPenaltyType');
    }

    /**
     * Returns if results were found and penalty is applicable
     *
     * @return bool
     */
    public function isApplicable() {
        $results = Result::where('idRace', $this->idRace)->get();
        return !$this->applied and count($results) > 0;
    }

    /**
     * Returns all penalties for current season of selected split
     *
     * @param int $splitSeason
     * @return mixed
     */
    public static function getPenalties(int $splitSeason) {
        return Penalty::select('penalties.id', 'penalties.idDriver', 'penalties.idRace', 'penalties.idPenaltyType', 'penalties.amount', 'penalties.applied')
            ->leftJoin('drivers', 'drivers.id', '=', 'penalties.idDriver')
            ->where('drivers.idSplit', $splitSeason)
            ->leftJoin('races', 'races.id', '=', 'penalties.idRace')
            ->orderBy('races.date')
            ->get();
    }

    /**
     * Returns upcoming penalties for current season of selected split
     *
     * @param int $splitSeason
     * @return mixed
     */
    public static function getUpcomingPenalties(int $splitSeason) {
        return Penalty::select('penalties.id', 'penalties.idDriver', 'penalties.idRace', 'penalties.idPenaltyType', 'penalties.amount', 'penalties.applied')
            ->leftJoin('drivers', 'drivers.id', '=', 'penalties.idDriver')
            ->where('drivers.idSplit', $splitSeason)
            ->leftJoin('races', 'races.id', '=', 'penalties.idRace')
            ->where('races.date', '>=', Carbon::today())
            ->orderBy('races.date')
            ->get();
    }
}
