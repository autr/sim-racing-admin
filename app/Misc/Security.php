<?php
/**
 * By: Martin Auterský
 * Date: 11.06.2019
 * Time: 15:14
 */

namespace App\Misc;


use App\Managed_league;
use App\Super;

class Security {
    /**
     * Checks if user is superadmin in selected league
     *
     * @param int $idAdmin
     * @param int $idLeague
     * @return mixed
     */
    public function superAdmin(int $idAdmin, int $idLeague) {
        return Super::where('idAdmin', $idAdmin)
            ->where('idLeague', $idLeague)
            ->count();
    }

    /**
     * Checks if user can edit selected league
     *
     * @param int $idAdmin
     * @param int $idLeague
     * @return bool
     */
    public function canEditLeague(int $idAdmin, int $idLeague) {
        $managed = Managed_league::where('idAdmin', $idAdmin)
            ->where('idLeague', $idLeague)->first();
        if ($managed) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Checks if user can edit selected split
     *
     * @param int $idAdmin
     * @param int $idLeague
     * @param int $idSplit
     * @return bool
     */
    public function canEditSplit(int $idAdmin, int $idLeague, int $idSplit) {
        $managed = Managed_league::where('idAdmin', $idAdmin)
            ->where('idSplit', $idSplit)
            ->where('idLeague', $idLeague)
            ->first();
        if ($managed) {
            return true;
        } else {
            return false;
        }
    }
}