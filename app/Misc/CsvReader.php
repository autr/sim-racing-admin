<?php
/**
 * By: Martin Auterský
 * Date: 12.10.2020
 * Time: 15:27
 */

namespace App\Misc;


class CsvReader {
    /**
     * Reads the CSV file and return an array
     *
     * @param $file
     * @param $args array
     */
    public function ReadCsv($file, $args) {
        $csvLines = [];
        $fileHandle = fopen($file, 'r');

        while(!feof($fileHandle)) {
            $csvLines[] = fgetcsv($fileHandle, 0, $args['delimiter']);
        }

        fclose($fileHandle);

        return $csvLines;
    }
}
