<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Driver extends Model {
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'idSplit', 'idTeam', 'active', 'idClass'
    ];

    /**
     * Returns team for selected driver
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function team(): \Illuminate\Database\Eloquent\Relations\BelongsTo {
        return $this->belongsTo('App\Team', 'idTeam');
    }

    /**
     * Returns class for selected driver
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function driverClass(): \Illuminate\Database\Eloquent\Relations\BelongsTo {
        return $this->belongsTo('App\DriverClass', 'idClass');
    }

    /**
     * Returns true when there are results for the driver
     *
     * @return bool
     */
    public function hasResults(): bool {
        $results = Result::where('idDriver', $this->id)->get();
        return count($results) > 0;
    }

    /**
     * Returns drivers for selected season
     *
     * @param $season
     * @return mixed
     */
    public static function getCurrentDrivers($season) {
        return Driver::select('drivers.id', 'drivers.name', 'drivers.idTeam', 'drivers.active', 'drivers.idClass')
            ->where('drivers.idSplit', $season)
            ->leftJoin('teams', 'teams.id', '=', 'drivers.idTeam')
            ->orderBy('active', 'DESC')
            ->orderByRaw('CASE WHEN drivers.idTeam IS NULL THEN 1 ELSE 0 END')
            ->orderBy('teams.name')
            ->orderBy('drivers.name')
            ->get();
    }
}
