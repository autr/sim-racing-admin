<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class League extends Model {
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'description', 'uri',
    ];

    /**
     * Returns user's leagues
     *
     * @param int $userId
     * @return mixed
     */
    public static function getUsersLeagues(int $userId) {
        return League::select('managed_leagues.idLeague', 'leagues.name', 'leagues.uri')
            ->leftJoin('managed_leagues', 'managed_leagues.idLeague', '=', 'leagues.id')
            ->where('managed_leagues.idAdmin', $userId)
            ->groupBy('managed_leagues.idLeague')
            ->get();
    }
}
