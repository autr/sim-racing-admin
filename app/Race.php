<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Race extends Model {
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'idTrack', 'date', 'idSplit', 'multiplier'
    ];

    protected $dates = ['date'];

    /**
     * Returns track for selected race
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function track() {
        return $this->belongsTo('App\Track', 'idTrack');
    }

    /**
     * Returns true when there are results for the race
     *
     * @return bool
     */
    public function hasResults() {
        $results = Result::where('idRace', $this->id)->get();
        return count($results) > 0;
    }

    /**
     * Returns results for selected race
     *
     * @return Result
     */
    public function getResults() {
        return Result::where('idRace', $this->id)->get();
    }

    /**
     * Returns penalties issued for selected race
     *
     * @return Penalty
     */
    public function getPenalties() {
        return Penalty::where('idRace', $this->id)->get();
    }

    /**
     * Returns penalty points issued in selected race
     *
     * @return Penalty_point
     */
    public function getPenaltyPoints() {
        return Penalty_point::where('idRace', $this->id)->get();
    }
}
