<?php

namespace App\Http\Controllers;

use App\League;
use App\Managed_league;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller {
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index() {
        $idUser = Auth::user()->id;
        $leagues = League::getUsersLeagues($idUser);
        $requests = \App\Request::join('managed_leagues', 'requests.idSplit', '=', 'managed_leagues.idSplit')
            ->join('supers', 'supers.idLeague', '=', 'managed_leagues.idLeague')
            ->where('managed_leagues.idAdmin', Auth::user()->id)
            ->where('requests.status', 'unresolved')
            ->get("requests.*");

        return view('home',
            [
                'leagues' => $leagues,
                'requests' => $requests,
            ]);
    }

    /**
     * Dismisses new news from the dashboard
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function dismissNews() {
        Auth::user()->update([
           'newsDismissed' => 1
        ]);

        $idUser = Auth::user()->id;
        $leagues = League::getUsersLeagues($idUser);
        $requests = \App\Request::join('managed_leagues', 'requests.idSplit', '=', 'managed_leagues.idSplit')
            ->join('supers', 'supers.idLeague', '=', 'managed_leagues.idLeague')
            ->where('managed_leagues.idAdmin', Auth::user()->id)
            ->where('requests.status', 'unresolved')
            ->get("requests.*");

        return view('home',
            [
                'leagues' => $leagues,
                'requests' => $requests,
            ]);
    }
}
