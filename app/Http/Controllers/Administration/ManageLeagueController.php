<?php

namespace App\Http\Controllers\Administration;

use App\Game;
use App\Http\Requests\CreateLeagueRequest;
use App\Http\Requests\RenameLeagueRequest;
use App\Http\Requests\UpdateLeagueRequest;
use App\League;
use App\Managed_league;
use App\Misc\Security;
use App\Platform;
use App\Region;
use App\Season;
use App\Split;
use App\Super;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Lang;

class ManageLeagueController extends Controller {
    //Where to redirect
    protected $redirHome = '/home';
    protected $security;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('auth');
        $this->security = new Security();
    }

    /**
     * Show splits for selected league
     *
     * @param string $league
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View|void
     */
    public function index(string $league) {
        $idUser = Auth::user()->id;
        $leagueInfo = League::where('uri', $league)->first();

        // Verifying that user can edit selected league
        if (!$leagueInfo or !$this->security->canEditLeague($idUser, $leagueInfo->id)) {
            return abort(403);
        }

        $splits = Split::select('splits.*')
            ->leftJoin('managed_leagues', 'managed_leagues.idSplit', '=', 'splits.id')
            ->where('managed_leagues.idAdmin', $idUser)
            ->where('managed_leagues.idLeague', $leagueInfo->id)
            ->get();

        $leagues = League::getUsersLeagues($idUser);

        return view('administration.league',
            [
                'league' => $league,
                'splits' => $splits,
                'leagues' => $leagues,
                'leagueInfo' => $leagueInfo,
                'superAdmin' => $this->security->superAdmin($idUser, $leagueInfo->id),
            ]);
    }

    /**
     * Show create league form
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create() {
        $idUser = Auth::user()->id;
        $platforms = Platform::all();
        $games = Game::orderBy('name')->get();
        $regions = Region::all();
        $leagues = League::getUsersLeagues($idUser);

        return view('administration.create-league',
            [
                'platforms' => $platforms,
                'games' => $games,
                'regions' => $regions,
                'leagues' => $leagues,
                'superAdmin' => 0,
            ]);
    }

    /**
     * Create league and initial split
     *
     * @param CreateLeagueRequest $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(CreateLeagueRequest $request) {
        $validated = $request->validated();

        $idUser = Auth::user()->id;

        // Begin transaction to ensure all data are stored in the database
        DB::beginTransaction();
        try {
            // Create and save league information
            $league = League::create([
                'name' => $validated['leagueName'],
                'description' => $validated['description'],
                'uri' => urlencode($validated['leagueName'])
            ]);

            // Create and save split information
            $split = Split::create([
                'name' => $validated['splitName'],
                'idLeague' => $league->id,
                'idPlatform' => $validated['idPlatform'],
                'idGame' => $validated['idGame'],
                'idRegion' => $validated['idRegion'],
                'max_per_team' => $validated['maxPerTeam'],
                'max_per_race' => $validated['maxPerRace'],
                'points' => $validated['points'],
                'no_points_dnf' => isset($validated['noPointsDnf']) ? $validated['noPointsDnf'] : 0,
                'omit_inactive' => isset($validated['omitInactive']) ? $validated['omitInactive'] : 0
            ]);

            // Create record in managed leagues table
            $managed = Managed_league::create([
                'idAdmin' => $idUser,
                'idSplit' => $split->id,
                'idLeague' => $league->id
            ]);

            // Create new season
            $season = Season::create([
                'idSplit' =>  $split->id,
                'season' => $validated['season']
            ]);

            // Create superadmin record
            $superAdmin = Super::create([
                'idAdmin' => $idUser,
                'idLeague' => $league->id
            ]);

            // Commit changes
            DB::commit();
        } catch (\Exception $ex) {
            // Rollback if exception
            DB::rollback();
            // Redirect with an error message
            return redirect($this->redirHome)
                ->with('errorMessage', Lang::get('messages.error_create',
                    ['name' => Lang::get('league')]));
        }

        // Redirect after completion
        return redirect($this->redirHome)
            ->with('message', Lang::get('messages.success_create',
                ['name' => ucfirst(Lang::get('league'))]));
    }

    /**
     * Update league's description
     *
     * @param UpdateLeagueRequest $request
     * @param string $league
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|void
     */
    public function update(UpdateLeagueRequest $request, string $league) {
        $redirTo = 'a/' . urlencode($league);
        $leagueInfo = League::where('uri', $league)->first();

        if (!$leagueInfo or !$this->security->canEditLeague(Auth::user()->id, $leagueInfo->id)) {
            return abort(403);
        }

        $validated = $request->validated();

        DB::beginTransaction();
        try {
            // Get league via id
            $leagueInfo = League::find($validated['leagueId']);

            // Change attributes
            $leagueInfo->description = $validated['description'];

            // Update the database
            $leagueInfo->save();
            DB::commit();
        } catch (\Exception $ex) {
            DB::rollback();
            return redirect($redirTo)
                ->with('errorMessage', Lang::get('messages.error_update',
                    ['name' => ucfirst(Lang::get('description'))]));
        }

        return redirect($redirTo)
            ->with('message', Lang::get('messages.success_update',
                ['name' => ucfirst(Lang::get('description'))]));
    }

    /**
     * Renames the league
     *
     * @param RenameLeagueRequest $request
     * @param string $league
     * @return \Illuminate\Http\JsonResponse|void
     * @throws \Exception
     */
    public function rename(RenameLeagueRequest $request, string $league) {
        $idUser = Auth::user()->id;
        $leagueInfo = League::where('uri', $league)->first();

        // Verifying that user can edit selected league
        if (!$leagueInfo or !$this->security->superAdmin($idUser, $leagueInfo->id)) {
            return abort(403);
        }

        $validated = $request->validated();

        DB::beginTransaction();
        try {
            $leagueInfo->update([
                'name' => $validated['leagueName'],
                'uri' => urlencode($validated['leagueName'])
            ]);
            $redirTo = 'a/' . urlencode($validated['leagueName']);

            DB::commit();
        } catch (\Exception $ex) {
            DB::rollBack();
            return response()->json(['errors' => [Lang::get('messages.error_update',
                ['name' => Lang::get('vars.split')])]]);
        }
        return response()->json([
            'success' => $redirTo,
            'message' => Lang::get('messages.success_rename_league')
        ]);
    }

    /**
     * Deletes the league
     *
     * @param string $league
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|void
     * @throws \Exception
     */
    public function destroy(string $league) {
        $redirTo = 'a/' . urlencode($league);
        $idUser = Auth::user()->id;
        $leagueInfo = League::where('uri', $league)->first();

        // Verifying that user can edit selected league
        if (!$leagueInfo or !$this->security->superAdmin($idUser, $leagueInfo->id)) {
            return abort(403);
        }

        DB::beginTransaction();
        try {
            // Delete the league
            $leagueInfo->delete();
            DB::commit();
        } catch (\Exception $ex) {
            DB::rollback();

            return redirect($redirTo)
                ->with('errorMessage', Lang::get('messages.error_delete_league'));
        }
        $redirTo = 'home';
        return redirect($redirTo)
            ->with('message', Lang::get('messages.success_delete_league'));
    }
}
