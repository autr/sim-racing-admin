<?php

namespace App\Http\Controllers\Administration;

use App\Driver;
use App\Http\Requests\AddPenaltyPointsRequest;
use App\Http\Requests\IssuePenaltyRequest;
use App\League;
use App\Managed_league;
use App\Misc\Security;
use App\Penalty;
use App\Penalty_point;
use App\Penalty_type;
use App\Race;
use App\Result;
use App\Season;
use App\Split;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Lang;

class ManagePenaltiesController extends Controller {
    protected $security;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('auth');
        $this->security = new Security();
    }

    /**
     * Show penalties management forms
     *
     * @param string $league
     * @param string $split
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View|void
     */
    public function index(string $league, string $split) {
        $idUser = Auth::user()->id;
        $leagueInfo = League::where('uri', $league)->first();

        // Verifying that user can edit selected league
        if (!$leagueInfo or !$this->security->canEditSplit($idUser, $leagueInfo->id, $split)) {
            return abort(403);
        }

        $splitInfo = Split::find($split);

        $seasonInfo = Season::getCurrentSeason($split);

        // Get active drivers
        $drivers = Driver::where('idSplit', $seasonInfo->id)
            ->where('active', 1)
            ->get();

        // Get upcoming races
        $races = Race::where('idSplit', $seasonInfo->id)
            ->get();

        $penaltyTypes = Penalty_type::all();
        $penalties = Penalty::getPenalties($seasonInfo->id);

        // Get past races
        $racesPP = Race::where('idSplit', $seasonInfo->id)
            ->whereDate('date', '<=', now())
            ->get();

        $penaltyPoints = Penalty_point::getPenaltyPoints($seasonInfo->id);

        $leagues = League::getUsersLeagues($idUser);

        return view('administration.penalties',
            [
                'league' => $league,
                'splitId' => $split,
                'splitInfo' => $splitInfo,
                'leagueInfo' => $leagueInfo,
                'drivers' => $drivers,
                'races' => $races,
                'penaltyTypes' => $penaltyTypes,
                'penalties' => $penalties,
                'racesPP' => $racesPP,
                'penaltyPoints' => $penaltyPoints,
                'leagues' => $leagues,
                'superAdmin' => $this->security->superAdmin($idUser, $leagueInfo->id),
            ]);
    }

    /**
     * Saves penalty to the database
     *
     * @param IssuePenaltyRequest $request
     * @param string $league
     * @param string $split
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Http\JsonResponse|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|void
     */
    public function store(IssuePenaltyRequest $request, string $league, string $split) {
        $idUser = Auth::user()->id;
        $leagueInfo = League::where('uri', $league)->first();

        // Verifying that user can edit selected league
        if (!$leagueInfo or !$this->security->canEditSplit($idUser, $leagueInfo->id, $split)) {
            return abort(403);
        }

        $validated = $request->validated();

        // Get driver's info and check if said driver exists
        $driverInfo = Driver::find($validated['driverSelection']);
        if (!$driverInfo) {
            return response()->json(['errors' => [Lang::get('messages.error_not_found', [
                'name' => Lang::get('vars.driver')
            ])]]);
        }

        // Get race info and check if said race exists
        $raceInfo = Race::find($validated['raceSelection']);
        if (!$raceInfo) {
            return response()->json(['errors' => [Lang::get('messages.error_not_found', [
                'name' => Lang::get('vars.race')
            ])]]);
        }

        // Get penalty type and check if it exists
        $typeInfo = Penalty_type::find($validated['typeSelection']);
        if (!$raceInfo) {
            return response()->json(['errors' => [Lang::get('messages.error_not_found', [
                'name' => Lang::get('vars.pen_type')
            ])]]);
        }

        DB::beginTransaction();
        try {
            // Issue new penalty
            $newPenalty = Penalty::create([
                'idDriver' => $driverInfo->id,
                'idRace' => $raceInfo->id,
                'idPenaltyType' => $typeInfo->id,
                'amount' => ($typeInfo->id == 3 or $typeInfo->id == 4) ? null : $validated['amount'],
                'applied' => 0
            ]);

            DB::commit();
        } catch (\Exception $ex) {
            DB::rollBack();
            return response()->json(['errors' => [Lang::get('messages.error_add', [
                'name' => Lang::get('vars.penalty')
            ])]]);
        }

        return response()->json(['success' => Lang::get('messages.success_issue_penalty')]);
    }

    public function destroy(Request $request, string $league, string $split, string $penalty) {
        return abort(404);
    }

    /**
     * Changes penalty status to applied
     *
     * @param string $league
     * @param string $split
     * @param string $penalty
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|void
     */
    public function makeApplied(string $league, string $split, string $penalty) {
        $redirTo = 'a/' . urlencode($league) . '/' . $split . '/penalties';

        $idUser = Auth::user()->id;
        $leagueInfo = League::where('uri', $league)->first();

        // Verifying that user can edit selected league
        if (!$leagueInfo or !$this->security->canEditSplit($idUser, $leagueInfo->id, $split)) {
            return abort(403);
        }


        // Get penalty info
        $penaltyInfo = Penalty::find($penalty);

        if (!$penaltyInfo) {
            return redirect($redirTo)
                ->with('errorMessage', Lang::get('messages.error_not_found', [
                    'name' => ucfirst(Lang::get('vars.penalty'))
                ]));
        }

        DB::beginTransaction();
        try {
            $penaltyInfo->applied = 1;
            $penaltyInfo->save();

            DB::commit();
        } catch (\Exception $ex) {
            DB::rollBack();
            return redirect($redirTo)
                ->with('errorMessage', Lang::get('messages.error_update', [
                    'name' => ucfirst(Lang::get('vars.penalty'))
                ]));
        }

        return redirect($redirTo)
            ->with('message', Lang::get('messages.success_update', [
                'name' => ucfirst(Lang::get('vars.penalty'))
            ]));
    }

    /**
     * Changes penalty status to not applied
     *
     * @param string $league
     * @param string $split
     * @param string $penalty
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|void
     */
    public function makeNotApplied(string $league, string $split, string $penalty) {
        $redirTo = 'a/' . urlencode($league) . '/' . $split . '/penalties';

        $idUser = Auth::user()->id;
        $leagueInfo = League::where('uri', $league)->first();

        // Verifying that user can edit selected league
        if (!$leagueInfo or !$this->security->canEditSplit($idUser, $leagueInfo->id, $split)) {
            return abort(403);
        }

        // Get penalty info
        $penaltyInfo = Penalty::find($penalty);

        if (!$penaltyInfo) {
            return redirect($redirTo)
                ->with('errorMessage', Lang::get('messages.error_not_found', [
                    'name' => ucfirst(Lang::get('vars.penalty'))
                ]));
        }

        DB::beginTransaction();
        try {
            $penaltyInfo->applied = 0;
            $penaltyInfo->save();

            DB::commit();
        } catch (\Exception $ex) {
            DB::rollBack();
            return redirect($redirTo)
                ->with('errorMessage', Lang::get('messages.error_update', [
                    'name' => ucfirst(Lang::get('vars.penalty'))
                ]));
        }

        return redirect($redirTo)
            ->with('message', Lang::get('messages.success_update', [
                'name' => ucfirst(Lang::get('vars.penalty'))
            ]));
    }

    /**
     * Adds penalty points into the database
     *
     * @param AddPenaltyPointsRequest $request
     * @param string $league
     * @param string $split
     * @return \Illuminate\Http\JsonResponse
     */
    public function storePoints(AddPenaltyPointsRequest $request, string $league, string $split) {
        $redirTo = 'a/' . urlencode($league) . '/' . $split . '/penalties';

        $idUser = Auth::user()->id;
        $leagueInfo = League::where('uri', $league)->first();

        // Verifying that user can edit selected league
        if (!$leagueInfo or !$this->security->canEditSplit($idUser, $leagueInfo->id, $split)) {
            return abort(403);
        }

        $validated = $request->validated();

        // Get driver's info and check if said driver exists
        $driverInfo = Driver::find($validated['driverSelectionPP']);
        if (!$driverInfo) {
            return response()->json(['errors' => [Lang::get('messages.error_add', [
                'name' => Lang::get('vars.driver')
            ])]]);
        }

        // Get race info and check if said race exists
        $raceInfo = Race::find($validated['raceSelectionPP']);
        if (!$raceInfo) {
            return response()->json(['errors' => [Lang::get('messages.error_add', [
                'name' => Lang::get('vars.race')
            ])]]);
        }

        $seasonInfo = Season::getCurrentSeason($split);

        DB::beginTransaction();
        try {
            // Issue new penalty
            $newPenaltyPoints = Penalty_point::create([
                'idDriver' => $driverInfo->id,
                'idRace' => $raceInfo->id,
                'idSplit' => $seasonInfo->id,
                'amount' => $validated['amountPP']
            ]);

            DB::commit();
        } catch (\Exception $ex) {
            DB::rollBack();
            return response()->json(['errors' => [Lang::get('messages.error_add', [
                'name' => Lang::get('vars.penaltyPoints')
            ])]]);
        }

        return response()->json(['success' => Lang::get('messages.success_add_points')]);
    }

    /**
     * Removes oldest penalty points of selected driver
     *
     * @param string $league
     * @param string $split
     * @param string $driver
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Http\JsonResponse|void
     */
    public function destroyPoints(string $league, string $split, string $driver) {
        $idUser = Auth::user()->id;
        $leagueInfo = League::where('uri', $league)->first();

        // Verifying that user can edit selected league
        if (!$leagueInfo or !$this->security->canEditSplit($idUser, $leagueInfo->id, $split)) {
            return abort(403);
        }

        $seasonInfo = Season::getCurrentSeason($split);

        $penaltyPoints = Penalty_point::where('idSplit', $seasonInfo->id)
            ->where('idDriver', $driver)
            ->orderBy('created_at')
            ->first();

        if (!$penaltyPoints) {
            return response()->json(['errors' => [Lang::get('messages.error_not_found')]]);
        }

        DB::beginTransaction();
        try {
            $penaltyPoints->delete();

            DB::commit();
        } catch (\Exception $ex) {
            DB::rollBack();
            return response()->json(['errors' => [Lang::get('messages.error_remove_points')]]);
        }

        return response()->json(['success' => Lang::get('messages.success_remove_points')]);
    }

    /**
     * Removes selected penalty
     *
     * @param string $league
     * @param string $split
     * @param string $penalty
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Http\JsonResponse|void
     */
    public function destroyPenalty(string $league, string $split, string $penalty) {
        $idUser = Auth::user()->id;
        $leagueInfo = League::where('uri', $league)->first();

        // Verifying that user can edit selected league
        if (!$leagueInfo or !$this->security->canEditSplit($idUser, $leagueInfo->id, $split)) {
            return abort(403);
        }

        $penalty = Penalty::find($penalty);

        if (!$penalty) {
            return response()->json(['errors' => [Lang::get('messages.penalty_not_found')]]);
        }

        DB::beginTransaction();
        try {
            $penalty->delete();

            DB::commit();
        } catch (\Exception $ex) {
            DB::rollBack();
            return response()->json(['errors' => [Lang::get('messages.error_remove_penalty')]]);
        }

        return response()->json(['success' => Lang::get('messages.success_remove_penalty')]);
    }

    /**
     * Applies selected penalty to the results
     *
     * @param string $league
     * @param string $split
     * @param string $penalty
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Http\JsonResponse|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|void
     * @throws \Exception
     */
    public function applyPenalty(string $league, string $split, string $penalty) {
        $redirTo = 'a/' . urlencode($league) . '/' . $split . '/penalties';

        $idUser = Auth::user()->id;
        $leagueInfo = League::where('uri', $league)->first();

        // Verifying that user can edit selected league
        if (!$leagueInfo or !$this->security->canEditSplit($idUser, $leagueInfo->id, $split)) {
            return abort(403);
        }

        $splitInfo = Split::find($split);

        // Get penalty info
        $penaltyInfo = Penalty::find($penalty);

        if (!$penaltyInfo) {
            return redirect($redirTo)
                ->with('errorMessage', Lang::get('messages.error_not_found', [
                    'name' => ucfirst(Lang::get('vars.penalty'))
                ]));
        }

        if (!$penaltyInfo->isApplicable()) {
            return redirect($redirTo)
                ->with('errorMessage', Lang::get('messages.error_not_applicable'));
        }

        $results = Result::where('idRace', $penaltyInfo->idRace)
            ->orderBy('position')
            ->get();

        $points = $splitInfo->getPoints();

        $initialPosition = 0;

        DB::beginTransaction();
        try {
            foreach ($results as $result) {
                if ($initialPosition > 0) {
                    // If intitial position is set, shift applicable drivers up
                    if ($result->position > $initialPosition and $result->position <= $initialPosition + $penaltyInfo->amount) {
                        $newPosition = $result->position - 1;

                        $result->update([
                           'position' => $newPosition,
                           'points' => $points[$newPosition]
                        ]);
                    }
                } else {
                    // If initial position is not set, check if the results belongs to the driver, who got the penalty
                    if ($result->idDriver == $penaltyInfo->idDriver) {
                        $initialPosition = $result->position;

                        $newPosition = $result->position + $penaltyInfo->amount > count($results) ? count($results) : $result->position + $penaltyInfo->amount;

                        $result->update([
                            'position' =>  $newPosition,
                            'points' => $points[$newPosition]
                        ]);
                    }
                }
            }

            $penaltyInfo->applied = 1;
            $penaltyInfo->save();

            DB::commit();
        } catch (\Exception $ex) {
            DB::rollBack();
            return redirect($redirTo)
                ->with('errorMessage', Lang::get('messages.error_apply_penalty'));
        }

        return redirect($redirTo)
            ->with('message', Lang::get('messages.success_apply_penalty'));
    }
}
