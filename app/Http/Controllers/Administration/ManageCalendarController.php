<?php

namespace App\Http\Controllers\Administration;

use App\Country;
use App\Http\Requests\AddTrackRequest;
use App\Http\Requests\AddTracksRequest;
use App\Http\Requests\GetTracksRequest;
use App\Http\Requests\UpdateRaceRequest;
use App\League;
use App\Managed_league;
use App\Misc\Security;
use App\Race;
use App\Season;
use App\Split;
use App\Track;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Lang;

class ManageCalendarController extends Controller {
    protected $security;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('auth');
        $this->security = new Security();
    }

    /**
     * Shows race calendar
     *
     * @param string $league
     * @param string $split
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View|void
     */
    public function index(string $league, string $split) {
        $idUser = Auth::user()->id;
        $leagueInfo = League::where('uri', $league)->first();

        // Verifying that user can edit selected league
        if (!$leagueInfo or !$this->security->canEditSplit($idUser, $leagueInfo->id, $split)) {
            return abort(403);
        }

        $splitInfo = Split::find($split);

        $seasonInfo = Season::getCurrentSeason($split);

        $calendar = Race::where('idSplit', $seasonInfo->id)
            ->orderByRaw('CASE WHEN races.date IS NULL THEN 1 ELSE 0 END')
            ->orderBy('races.date')
            ->get();

        $countries = Country::orderBy('name')->get();
        $tracks = Track::all();

        $trackCountries = Track::select('countries.code', 'countries.name')
            ->groupBy('idCountry')
            ->leftJoin('countries', 'tracks.idCountry', '=', 'countries.code')
            ->orderBy('countries.name')
            ->get();

        $leagues = League::getUsersLeagues($idUser);

        return view('administration.calendar',
            [
                'league' => $league,
                'splitId' => $split,
                'leagueInfo' => $leagueInfo,
                'splitInfo' => $splitInfo,
                'calendar' => $calendar,
                'countries' => $countries,
                'tracks' => $tracks,
                'trackCountries' => $trackCountries,
                'leagues' => $leagues,
                'superAdmin' => $this->security->superAdmin($idUser, $leagueInfo->id),
            ]);
    }

    /**
     * Adds existing tracks into the calendar
     *
     * @param AddTracksRequest $request
     * @param string $league
     * @param string $split
     * @return \Illuminate\Http\JsonResponse|void
     */
    public function store(AddTracksRequest $request, string $league, string $split) {
        $idUser = Auth::user()->id;
        $leagueInfo = League::where('uri', $league)->first();

        // Verifying that user can edit selected league
        if (!$leagueInfo or !$this->security->canEditSplit($idUser, $leagueInfo->id, $split)) {
            return abort(403);
        }

        $validated = $request->validated();

        DB::beginTransaction();
        try {
            // Get season info
            $seasonInfo = Season::getCurrentSeason($split);

            // Save each track as a race to the database
            foreach ($validated['trackSelection'] as $track) {
                $newRace = Race::create([
                    'idTrack' => $track,
                    'date' => NULL,
                    'idSplit' => $seasonInfo->id,
                ]);
            }

            DB::commit();
        } catch (\Exception $ex) {
            DB::rollBack();
            return response()->json(['errors' => [Lang::get('messages.error_add', [
                'name' => Lang::get('vars.tracks(s)')
            ])]]);
        }
        return response()->json(['success' => Lang::get('messages.success_add', [
            'name' => ucfirst(Lang::get('vars.track(s)'))
        ])]);
    }

    /**
     * Creates and stores new track into the calendar
     *
     * @param AddTrackRequest $request
     * @param string $league
     * @param string $split
     * @return \Illuminate\Http\JsonResponse|void
     */
    public function storeNew(AddTrackRequest $request, string $league, string $split) {
        $idUser = Auth::user()->id;
        $leagueInfo = League::where('uri', $league)->first();

        // Verifying that user can edit selected league
        if (!$leagueInfo or !$this->security->canEditSplit($idUser, $leagueInfo->id, $split)) {
            return abort(403);
        }

        $validated = $request->validated();

        DB::beginTransaction();
        try {
            // Get season info
            $seasonInfo = Season::getCurrentSeason($split);

            // Check if the track exists
            $track = Track::where('name', $validated['trackName'])->first();

            if ($track) {
                return response()->json(['errors' => [Lang::get('messages.error_track_exists')]]);
            }

            // Create new track and save it as a race to the database
            $newTrack = Track::create([
                'name' => $validated['trackName'],
                'idCountry' => $validated['trackCountry']
            ]);

            $newTrack->save();

            $newRace = Race::create([
                'idTrack' => $newTrack->id,
                'date' => NULL,
                'idSplit' => $seasonInfo->id,
            ]);

            DB::commit();
        } catch (\Exception $ex) {
            DB::rollBack();
            return response()->json(['errors' => [Lang::get('messages.error_add', [
                'name' => Lang::get('vars.tracks')
            ])]]);
        }
        return response()->json(['success' => Lang::get('messages.success_add', [
            'name' => ucfirst(Lang::get('vars.track'))
        ])]);
    }

    /**
     * Updates race information
     *
     * @param UpdateRaceRequest $request
     * @param string $league
     * @param string $split
     * @param string $race
     * @return \Illuminate\Http\JsonResponse|void
     */
    public function update(UpdateRaceRequest $request, string $league, string $split, string $race) {
        $idUser = Auth::user()->id;
        $leagueInfo = League::where('uri', $league)->first();

        // Verifying that user can edit selected league
        if (!$leagueInfo or !$this->security->canEditSplit($idUser, $leagueInfo->id, $split)) {
            return abort(403);
        }

        $validated = $request->validated();

        // Get season info
        $seasonInfo = Season::getCurrentSeason($split);

        // Get actual race information and change it
        $raceInfo = Race::where('id', $race)
            ->where('idSplit', $seasonInfo->id)
            ->first();

        // Deny updating of races with results
        if ($raceInfo->hasResults()) {
            return response()->json(['errors' => [Lang::get('messages.error_old_race')]]);
        }

        DB::beginTransaction();
        try {
            $raceInfo->date = $validated['date'];
            $raceInfo->multiplier = $validated['multiplier'];

            $raceInfo->save();

            DB::commit();
        } catch (\Exception $ex) {
            DB::rollBack();
            return response()->json(['errors' => [Lang::get('messages.error_update', [
                'name' => Lang::get('vars.track'),
            ])]]);
        }
        return response()->json(['success' => Lang::get('messages.success_update', [
            'name' => ucfirst(Lang::get('vars.track'))
        ])]);
    }

    /**
     * Removes selected race
     *
     * @param Request $request
     * @param string $league
     * @param string $split
     * @param string $race
     * @return \Illuminate\Http\JsonResponse|void
     */
    public function destroy(Request $request, string $league, string $split, string $race) {
        $idUser = Auth::user()->id;
        $leagueInfo = League::where('uri', $league)->first();

        // Verifying that user can edit selected league
        if (!$leagueInfo or !$this->security->canEditSplit($idUser, $leagueInfo->id, $split)) {
            return abort(403);
        }

        $raceInfo = Race::find($race);

        DB::beginTransaction();
        try {
            // Check and delete results from the race
            if ($raceInfo->hasResults()) {
                $results = $raceInfo->getResults();

                foreach ($results as $result) {
                    $result->delete();
                }
            }

            // Delete all penalties issued for the race
            $penalties = $raceInfo->getPenalties();
            foreach ($penalties as $penalty) {
                $penalty->delete();
            }

            // Delete all penalty points issued in the race
            $penaltyPoints = $raceInfo->getPenaltyPoints();
            foreach ($penaltyPoints as $penaltyPoint) {
                $penaltyPoint->delete();
            }

            // Delete race from the database
            $raceInfo->delete();

            DB::commit();
        } catch (\Exception $ex) {
            DB::rollBack();
            //return response()->json(['errors' => [$ex->getMessage()]]);
            return response()->json(['errors' => [Lang::get('messages.error_remove_race')]]);
        }

        return response()->json(['success' => [Lang::get('messages.success_remove_race')]]);
    }

    /**
     * Gets track list for selected country
     *
     * @param GetTracksRequest $request
     * @param string $league
     * @param string $split
     * @return \Illuminate\Http\JsonResponse
     */
    public function getTracks(GetTracksRequest $request, string $league, string $split) {
        $validated = $request->validated();
        $country = $validated['trackCountry'];

        try {
            if ($country == 'all') {
                $tracks = Track::all();
            } else {
                $tracks = Track::where('idCountry', $validated['trackCountry'])->get();
            }
            return response()->json(['success' => $tracks]);
        } catch (\Exception $ex) {
            return response()->json(['errors' => [Lang::get('messages.error_get_tracks')]]);
        }
    }
}
