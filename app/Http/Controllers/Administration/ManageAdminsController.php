<?php

namespace App\Http\Controllers\Administration;

use App\League;
use App\Managed_league;
use App\Misc\Security;
use App\Split;
use App\Super;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Lang;

class ManageAdminsController extends Controller {
    protected $redirHome = '/home';
    protected $redirBrowse = '/browse';
    protected $redirRequests = '/requests';
    protected $security;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('auth');
        $this->security = new Security();
    }

    /**
     * Shows admins for selected split
     *
     * @param string $league
     * @param string $split
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View|void
     */
    public function manageAdmins(string $league, string $split) {
        $idUser = Auth::user()->id;
        $leagueInfo = League::where('uri', $league)->first();

        // Verifying that user can edit selected league
        if (!$leagueInfo or !$this->security->canEditSplit($idUser, $leagueInfo->id, $split)) {
            return abort(403);
        }

        $splitInfo = Split::find($split);

        // Check if user is superadmin for selected league
        if (!$this->security->superAdmin($idUser, $leagueInfo->id)) {
            return abort(403);
        }

        // Get admins for selected split
        $admins = User::join('managed_leagues', 'users.id', '=', 'managed_leagues.idAdmin')
            ->where('managed_leagues.idSplit', $split)
            ->where('users.id', '!=', $idUser)
            ->get('users.*');

        $leagues = League::getUsersLeagues($idUser);

        return view('administration.admins',
            [
                'league' => $league,
                'splitId' => $split,
                'splitInfo' => $splitInfo,
                'leagueInfo' => $leagueInfo,
                'admins' => $admins,
                'leagues' => $leagues,
                'superAdmin' => $this->security->superAdmin($idUser, $leagueInfo->id),
            ]);
    }

    /**
     * Revokes permission for selected user
     *
     * @param string $league
     * @param string $split
     * @param string $user
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|void
     */
    public function revokePermission(string $league, string $split, string $user) {
        $idUser = Auth::user()->id;
        $leagueInfo = League::where('uri', $league)->first();

        // Verifying that user can edit selected league
        if (!$leagueInfo or !$this->security->canEditSplit($idUser, $leagueInfo->id, $split)) {
            return abort(403);
        }

        // Check if user is superadmin for selected league
        if (!$this->security->superAdmin($idUser, $leagueInfo->id)) {
            return abort(403);
        }

        $userInfo = User::find($user);

        // Check if selected user exists
        if (!$userInfo) {
            return redirect(route('manageAdmins', [urlencode($league), $split]))
                ->with('errorMessage', Lang::get('messages.error_not_found', [
                    'name' => Lang::get('vars.user')
                ]));
        }

        DB::beginTransaction();
        try {
            // Delete record in managed leagues
            $managed = Managed_league::where('idAdmin', $user)
                ->where('idSplit', $split)
                ->first();
            $managed->delete();

            // Delete request, so user can send it again
            $request = \App\Request::where('idUser', $user)
                ->where('idSplit', $split)
                ->first();
            if ($request) {
                $request->status = 'disapproved';
                $request->save();
            }

            DB::commit();

            return redirect(route('manageAdmins', [urlencode($league), $split]))
                ->with('message', Lang::get('messages.permission_revoked'));
        } catch (\Exception $ex) {
            DB::rollBack();
            return redirect(route('manageAdmins', [urlencode($league), $split]))
                ->with('errorMessage', Lang::get('messages.error_revoke'));
        }
    }

    /**
     * Creates request for selected split
     *
     * @param string $split
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function makeRequest(string $split) {
        $idUser = Auth::user()->id;
        try {
            // Check if split exists
            $splitInfo = Split::where('id', $split)->first();
            if (!$splitInfo) {
                return redirect($this->redirBrowse)
                    ->with('errorMessage', Lang::get('messages.error_not_found', [
                        'name' => Lang::get('vars.split')
                    ]));
            }

            // Check if user doesn't have a request
            $requestInfo = \App\Request::where('idUser', $idUser)
                ->where('idSplit', $split)->first();
            if ($requestInfo and $requestInfo->status === 'disapproved' and $requestInfo->updated_at < Carbon::now()->subDays(7)) {
                $requestInfo->status = 'unresolved';
                $requestInfo->save();
            } else if ($requestInfo and $requestInfo->status !== 'approved') {
                return redirect($this->redirRequests)
                    ->with('errorMessage', Lang::get('messages.request_twice'));
            } else {
                // Check if user isn't an admin
                $managedInfo = Managed_league::where('idAdmin', $idUser)
                    ->where('idSplit', $split)->first();
                if ($managedInfo) {
                    return redirect($this->redirBrowse)
                        ->with('errorMessage', Lang::get('messages.request_managed'));
                }

                // Create new request
                $newRequest = \App\Request::create([
                    'idSplit' => $split,
                    'idUser' => $idUser,
                    'status' => 'unresolved',
                ]);
            }
        } catch (\Exception $ex) {
            return redirect($this->redirBrowse)
                ->with('errorMessage', Lang::get('messages.error_request'));
        }

        return redirect($this->redirRequests)
            ->with('message', Lang::get('messages.request_sent'));
    }

    /**
     * Approves selected request
     *
     * @param string $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|void
     */
    public function approveRequest(string $request) {
        $idUser = Auth::user()->id;

        DB::beginTransaction();
        try {
            $requestInfo = \App\Request::where('id', $request)->first();

            // Check if user is superadmin for selected league
            if (!$this->security->superAdmin($idUser, $requestInfo->split->league->id)) {
                DB::rollback();
                return abort(403);
            }

            // Change status to approved
            $requestInfo->status = "approved";
            $requestInfo->save();

            // Create new managed record
            $managed = Managed_league::create([
                'idAdmin' => $requestInfo->idUser,
                'idSplit' => $requestInfo->idSplit,
                'idLeague' => $requestInfo->split->idLeague
            ]);

            // Commit changes
            DB::commit();
        } catch (\Exception $ex) {
            // Rollback if exception
            DB::rollback();
            // Redirect with an error message
            return redirect($this->redirHome)
                ->with('errorMessage', Lang::get('messages.error_approval'));
        }

        return redirect($this->redirHome)
            ->with('message', Lang::get('messages.request_approved'));
    }

    /**
     * Disapproves selected request
     *
     * @param string $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|void
     */
    public function disapproveRequest(string $request) {
        $idUser = Auth::user()->id;

        try {
            $requestInfo = \App\Request::where('id', $request)->first();

            // Check if user is superadmin for selected league
            if (!$this->security->superAdmin($idUser, $requestInfo->split->league->id)) {
                return abort(403);
            }

            // Change status to disapproved
            $requestInfo->status = "disapproved";
            $requestInfo->save();
        } catch (\Exception $ex) {
            // Redirect with an error message
            return redirect($this->redirHome)
                ->with('errorMessage', Lang::get('messages.error_approval'));
        }
        return redirect($this->redirHome)
            ->with('message', Lang::get('messages.request_disapproved'));
    }

    /**
     * Shows requests of selected user
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function manageRequests() {
        $idUser = Auth::user()->id;

        $leagues = League::getUsersLeagues($idUser);
        $requests = \App\Request::where('idUser', $idUser)->get();

        return view('administration.requests',
            [
                'leagues' => $leagues,
                'requests' => $requests
            ]);
    }

    /**
     * Transfers superadmin rights to selected user
     *
     * @param string $league
     * @param string $split
     * @param string $user
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|void
     */
    public function transferSuperadmin(string $league, string $split, string $user) {
        $idUser = Auth::user()->id;
        $leagueInfo = League::where('uri', $league)->first();

        // Verifying that user can edit selected league
        if (!$leagueInfo or !$this->security->canEditSplit($idUser, $leagueInfo->id, $split)) {
            return abort(403);
        }

        // Check if user is superadmin for selected league
        if (!$this->security->superAdmin($idUser, $leagueInfo->id)) {
            return abort(403);
        }

        $userInfo = User::find($user);

        // Check if selected user exists
        if (!$userInfo) {
            return redirect(route('manageAdmins', [urlencode($league), $split]))
                ->with('errorMessage', Lang::get('messages.error_not_found', [
                    'name' => Lang::get('vars.user')
                ]));
        }

        // Check if selected user is an admin
        $managed = Managed_league::where('idAdmin', $userInfo->id)
            ->where('idSplit', $split)
            ->first();
        if (!$managed) {
            return redirect(route('manageAdmins', [urlencode($league), $split]))
                ->with('errorMessage', Lang::get('messages.error_not_admin'));
        }

        DB::beginTransaction();
        try {
            // Delete old record from supers
            $super = Super::where('idAdmin', $idUser)
                ->where('idLeague', $leagueInfo->id)
                ->first();
            $super->delete();

            // Get all managed splits of old superadmin
            $allSplits = Managed_league::where('idLeague', $leagueInfo->id)
                ->where('idAdmin', $idUser)
                ->get();
            if  (count($allSplits) > 0) {
                // Give rights to all remaining splits to a new superadmin
                foreach ($allSplits as $allSplit) {
                    $managed = Managed_league::where('idSplit', $allSplit->split->id)
                        ->where('idAdmin', $userInfo->id)
                        ->first();
                    if (!$managed) {
                        $newManaged = Managed_league::create([
                            'idLeague' => $leagueInfo->id,
                            'idAdmin' => $userInfo->id,
                            'idSplit' => $allSplit->split->id
                        ]);
                    }
                    $request = \App\Request::where('idUser', $userInfo->id)
                        ->where('idSplit', $allSplit->split->id)
                        ->first();
                    if ($request) $request->delete();
                }
            }

            // Create new superadmin
            $newSuper = Super::create([
                'idLeague' => $leagueInfo->id,
                'idAdmin' => $userInfo->id
            ]);
            DB::commit();
        } catch (\Exception $ex) {
            DB::rollBack();
            return redirect(route('manageAdmins', [urlencode($league), $split]))
                ->with('errorMessage', Lang::get('messages.error_transfer_sa'));
        }
        return redirect(route('split.index', [urlencode($league), $split]))
            ->with('message', Lang::get('messages.success_transfer_sa'));
    }
}
