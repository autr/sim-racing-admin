<?php

namespace App\Http\Controllers\Administration;

use App\Http\Requests\AddTeamsRequest;
use App\Http\Requests\UpdateTeamRequest;
use App\League;
use App\Managed_league;
use App\Misc\Security;
use App\Season;
use App\Split;
use App\Team;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Lang;

class ManageTeamsController extends Controller {
    protected $redirHome = '/home';
    protected $security;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('auth');
        $this->security = new Security();
    }

    /**
     * Show teams management forms
     *
     * @param string $league
     * @param string $split
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View|void
     */
    public function index(string $league, string $split) {
        $idUser = Auth::user()->id;
        $leagueInfo = League::where('uri', $league)->first();

        // Verifying that user can edit selected league
        if (!$leagueInfo or !$this->security->canEditSplit($idUser, $leagueInfo->id, $split)) {
            return abort(403);
        }

        $splitInfo = Split::find($split);

        // Get teams for current season
        $seasonInfo = Season::getCurrentSeason($split);
        $teamsInfo = Team::where('idSplit', $seasonInfo->id)
            ->orderBy('name')
            ->get();

        $leagues = League::getUsersLeagues($idUser);

        return view('administration.teams',
            [
                'league' => $league,
                'splitId' => $split,
                'splitInfo' => $splitInfo,
                'leagueInfo' => $leagueInfo,
                'teams' => $teamsInfo,
                'leagues' => $leagues,
                'superAdmin' => $this->security->superAdmin($idUser, $leagueInfo->id),
            ]);
    }

    /**
     * Adds teams into the database
     *
     * @param AddTeamsRequest $request
     * @param string $league
     * @param string $split
     * @return \Illuminate\Http\JsonResponse|void
     */
    public function store(AddTeamsRequest $request, string $league, string $split) {
        $idUser = Auth::user()->id;
        $leagueInfo = League::where('uri', $league)->first();

        // Verifying that user can edit selected league
        if (!$leagueInfo or !$this->security->canEditSplit($idUser, $leagueInfo->id, $split)) {
            return abort(403);
        }

        $validated = $request->validated();

        DB::beginTransaction();
        try {
            // Get season info
            $seasonInfo = Season::getCurrentSeason($split);

            // Split teams by a new line
            $teams = preg_split('/\r\n|[\r\n]/',$validated['teams']);

            // Save each team to the database
            foreach ($teams as $team) {
                Team::create([
                    'name' => $team,
                    'idSplit' => $seasonInfo->id,
                    'primaryColour' => '#ffffff',
                    'secondaryColour1' => NULL,
                    'secondaryColour2' => NULL
                ]);
            }

            DB::commit();
        } catch (\Exception $ex) {
            DB::rollBack();
            return response()->json(['errors' => [Lang::get('messages.error_add', [
                'name' => Lang::get('vars.team(s)')
            ])]]);
        }

        return response()->json(['success' => Lang::get('messages.success_add', [
            'name' => ucfirst(Lang::get('vars.team(s)'))
        ])]);
    }

    /**
     * Updates team information
     *
     * @param UpdateTeamRequest $request
     * @param string $league
     * @param string $split
     * @param string $team
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|void
     */
    public function update(UpdateTeamRequest $request, string $league, string $split, string $team) {
        $idUser = Auth::user()->id;
        $leagueInfo = League::where('uri', $league)->first();

        // Verifying that user can edit selected league
        if (!$leagueInfo or !$this->security->canEditSplit($idUser, $leagueInfo->id, $split)) {
            return abort(403);
        }

        $validated = $request->validated();

        DB::beginTransaction();
        try {
            // Get team info
            $teamInfo = Team::find($team);

            // Update team info
            $teamInfo->name = $validated['name'];
            $teamInfo->primaryColour = $validated['primary'];
            $teamInfo->secondaryColour1 = $validated['secondary1'] != -1 ? $validated['secondary1'] : NULL;
            $teamInfo->secondaryColour2 = $validated['secondary2'] != -1 ? $validated['secondary2'] : NULL;

            $teamInfo->save();

            DB::commit();
        } catch (\Exception $ex) {
            DB::rollBack();
            return response()->json(['errors' => [Lang::get('messages.error_team_update')]]);
        }
        return response()->json(['success' => Lang::get('messages.success_update_team')]);
    }

    /**
     * Removes team
     *
     * @param Request $request
     * @param string $league
     * @param string $split
     * @param string $team
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|void
     */
    public function destroy(Request $request, string $league, string $split, string $team) {
        $idUser = Auth::user()->id;
        $leagueInfo = League::where('uri', $league)->first();

        // Verifying that user can edit selected league
        if (!$leagueInfo or !$this->security->canEditSplit($idUser, $leagueInfo->id, $split)) {
            return abort(403);
        }

        DB::beginTransaction();
        try {
            // Get team info
            $teamInfo = Team::find($team);

            // If team has results it can't be removed
            if ($teamInfo->hasResults()) {
                $type = 'errors';
                $message = Lang::get('messages.error_team_not_removed');
            } else {
                $teamInfo->delete();
                $type = 'success';
                $message = Lang::get('messages.success_team_removed');
            }
            DB::commit();
        } catch (\Exception $ex) {
            DB::rollBack();
            return response()->json(['errors' => [Lang::get('messages.error_remove_team')]]);
        }
        return response()->json([$type => $message]);
    }
}
