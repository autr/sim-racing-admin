<?php

namespace App\Http\Controllers\Administration;

use App\Driver;
use App\DriverClass;
use App\League;
use App\Misc\Security;
use App\Penalty;
use App\Race;
use App\Result;
use App\Season;
use App\Split;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Lang;

class ManageResultsController extends Controller {
    protected $security;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware(['auth', 'can_edit']);
        $this->security = new Security();
    }

    /**
     * Shows result management screen
     *
     * @param string $league
     * @param string $split
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View|void
     */
    public function index(string $league, string $split) {
        //TODO: Remove duplicities
        $idUser = Auth::user()->id;
        $leagueInfo = League::where('uri', $league)->first();

        // Verifying that user can edit selected league
        if (!$leagueInfo or !$this->security->canEditSplit($idUser, $leagueInfo->id, $split)) {
            return abort(403);
        }

        $splitInfo = Split::find($split);

        // Get current season
        $seasonInfo = Season::getCurrentSeason($split);

        // Get past races
        $races = Race::where('idSplit', $seasonInfo->id)
            ->whereDate('date', '<=', now())
            ->orderBy('date', 'desc')
            ->get();

        if (count($races) > 0) {
            $currentRace = $races->first();

            $results = Result::where('idRace', $currentRace->id)
                ->orderBy('position')
                ->get();

            $drivers = [];
            foreach ($results as $result) {
                $drivers[] = $result->idDriver;
            }

            $raceBans = Penalty::where('idRace', $currentRace->id)
                ->where('idPenaltyType', 4)
                ->get();

            if (count($raceBans) > 0) {
                $bannedDrivers = [];
                foreach ($raceBans as $raceBan) {
                    $bannedDrivers[] = $raceBan->idDriver;
                }

                $driversInfo = Driver::where('idSplit', $seasonInfo->id)
                    ->where('active', 1)
                    ->orderByRaw('CASE WHEN idTeam IS NULL THEN 1 ELSE 0 END')
                    ->orderBy('idTeam')
                    ->whereNotIn('id', $bannedDrivers)
                    ->whereNotIn('id', $drivers)
                    ->get();
            } else {
                $driversInfo = Driver::where('idSplit', $seasonInfo->id)
                    ->where('active', 1)
                    ->orderByRaw('CASE WHEN idTeam IS NULL THEN 1 ELSE 0 END')
                    ->orderBy('idTeam')
                    ->whereNotIn('id', $drivers)
                    ->get();
            }
        } else {
            $currentRace = null;
            $driversInfo = null;
            $results = [];
        }

        $leagues = League::getUsersLeagues($idUser);

        return view('administration.results',
            [
                'league' => $league,
                'splitId' => $split,
                'splitInfo' => $splitInfo,
                'leagueInfo' => $leagueInfo,
                'availableDrivers' => $driversInfo,
                'races' => $races,
                'currentRace' => $currentRace,
                'results' => $results,
                'leagues' => $leagues,
                'superAdmin' => $this->security->superAdmin($idUser, $leagueInfo->id),
            ]);
    }

    /**
     * Stores new results in the database
     *
     * @param Request $request
     * @param string $league
     * @param string $split
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|void
     * @throws \Exception
     */
    public function store(Request $request, string $league, string $split) {
        $redirTo = 'a/' . urlencode($league) . '/' . $split . '/results';

        $idUser = Auth::user()->id;
        $leagueInfo = League::where('uri', $league)->first();

        // Verifying that user can edit selected league
        if (!$leagueInfo or !$this->security->canEditSplit($idUser, $leagueInfo->id, $split)) {
            return abort(403);
        }

        $splitInfo = Split::find($split);

        DB::beginTransaction();
        try {
            $points = $splitInfo->getPoints();

            $results = $request['results'];
            if ($request->has('dnf')) {
                $dnfs = $request['dnf'];
            } else {
                $dnfs = [];
            }
            $currentRace = $request['idRace'];

            if ($request->has('bonusPoints')) {
                $bonusPoints = $request['bonusPoints'];
            } else {
                $bonusPoints = [];
            }

            $this->saveResults($currentRace, $results, $dnfs, $points, $bonusPoints, $splitInfo);

            DB::commit();
        } catch (\Exception $ex) {
            DB::rollBack();
            return redirect($redirTo)
                ->with('errorMessage', Lang::get($ex->getMessage(), [
                    'name' => Lang::get('vars.results')
                ]));
        }

        return redirect($redirTo)
            ->with('message', Lang::get('messages.success_add', [
                'name' => ucfirst(Lang::get('vars.results'))
            ]));
    }

    /**
     * Saves changes made to the results
     *
     * @param Request $request
     * @param string $league
     * @param string $split
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|void
     * @throws \Exception
     */
    public function update(Request $request, string $league, string $split) {
        $idRace = $request['idRace'];

        $redirTo = 'a/' . urlencode($league) . '/' . $split . '/results/' . $idRace;

        $idUser = Auth::user()->id;
        $leagueInfo = League::where('uri', $league)->first();

        // Verifying that user can edit selected league
        if (!$leagueInfo or !$this->security->canEditSplit($idUser, $leagueInfo->id, $split)) {
            return abort(403);
        }

        $splitInfo = Split::find($split);

        DB::beginTransaction();
        try {
            $points = $splitInfo->getPoints();

            Result::where('idRace', $idRace)->delete();

            $results = $request['results'];
            if ($request->has('dnf')) {
                $dnfs = $request['dnf'];
            } else {
                $dnfs = [];
            }

            if ($request->has('bonusPoints')) {
                $bonusPoints = $request['bonusPoints'];
            } else {
                $bonusPoints = [];
            }

            $this->saveResults($idRace, $results, $dnfs, $points, $bonusPoints, $splitInfo);

            DB::commit();
        } catch (\Exception $ex) {
            DB::rollBack();
            return redirect($redirTo)
                ->with('errorMessage', Lang::get('messages.error_update', [
                    'name' => Lang::get('vars.results')
                ]));
        }

        return redirect($redirTo)
            ->with('message', Lang::get('messages.success_update', [
                'name' => ucfirst(Lang::get('vars.results'))
            ]));
    }

    /**
     * Saves results
     *
     * @param int $idRace
     * @param $results
     * @param $dnfs
     * @param $points
     * @param $bonusPoints
     */
    private function saveResults(int $idRace, $results, $dnfs, $points, $bonusPoints, $splitInfo) {
        $raceInfo = Race::find($idRace);
        $seasonInfo = Season::getCurrentSeason($splitInfo->id);
        $classes = DriverClass::where('idSplit', $seasonInfo->id)->get()->pluck('id');

        $classPositions = [];
        foreach ($classes as $class) {
            $classPositions[$class] = 1;
        }

        // Save results
        foreach ($results as $position => $result) {
            $driver = Driver::find($result);

            $dnfMultiplier = (array_key_exists($driver->id, $dnfs) and $splitInfo->no_points_dnf) ? 0 : 1;

            Result::create([
                'idDriver' => $driver->id,
                'idRace' => $idRace,
                'position' => $position+1,
                'points' => $points[$position+1] * $raceInfo->multiplier * $dnfMultiplier,
                'classPoints' => count($classes) > 0 ? $points[$classPositions[$driver->idClass]] * $raceInfo->multiplier * $dnfMultiplier : 0,
                'bonusPoints' => array_key_exists($driver->id, $bonusPoints) ? $bonusPoints[$driver->id] : 0,
                'idTeam' => $driver->idTeam,
                'didNotFinish' => array_key_exists($driver->id, $dnfs) ? 1 : 0
            ]);
            if (count($classes) > 0) {
                $classPositions[$driver->idClass]++;
            }
        }
    }

    /**
     * Returns selected race results
     *
     * @param Request $request
     * @param string $league
     * @param string $split
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View|void
     */
    public function select(string $league, string $split, string $race) {
        $redirTo = 'a/' . urlencode($league) . '/' . $split . '/results';

        $idUser = Auth::user()->id;
        $leagueInfo = League::where('uri', $league)->first();

        // Verifying that user can edit selected league
        if (!$leagueInfo or !$this->security->canEditSplit($idUser, $leagueInfo->id, $split)) {
            return abort(403);
        }

        $splitInfo = Split::find($split);

        // Get current season
        $seasonInfo = Season::getCurrentSeason($split);

        $currentRace = Race::find($race);

        if (!$currentRace) {
            return redirect($redirTo)
                ->with('errorMessage', Lang::get('messages.error_no_race'));
        }

        $results = Result::where('idRace', $currentRace->id)
            ->orderBy('position')
            ->get();

        $drivers = [];
        foreach ($results as $result) {
            $drivers[] = $result->idDriver;
        }

        $raceBans = Penalty::where('idRace', $currentRace->id)
            ->where('idPenaltyType', 4)
            ->get();

        if (count($raceBans) > 0) {
            $drivers = [];
            foreach ($raceBans as $raceBan) {
                $drivers[] = $raceBan->idDriver;
            }

            $driversInfo = Driver::where('idSplit', $seasonInfo->id)
                ->where('active', 1)
                ->orderByRaw('CASE WHEN idTeam IS NULL THEN 1 ELSE 0 END')
                ->orderBy('idTeam')
                ->whereNotIn('id', $drivers)
                ->whereNotIn('id', $drivers)
                ->get();
        } else {
            $driversInfo = Driver::where('idSplit', $seasonInfo->id)
                ->where('active', 1)
                ->orderByRaw('CASE WHEN idTeam IS NULL THEN 1 ELSE 0 END')
                ->orderBy('idTeam')
                ->whereNotIn('id', $drivers)
                ->get();
        }

        $leagues = League::getUsersLeagues($idUser);

        // Get races
        $races = Race::where('idSplit', $seasonInfo->id)
            ->whereDate('date', '<=', now())
            ->orderBy('date', 'desc')
            ->get();

        return view('administration.results',
            [
                'league' => $league,
                'splitId' => $split,
                'splitInfo' => $splitInfo,
                'leagueInfo' => $leagueInfo,
                'availableDrivers' => $driversInfo,
                'races' => $races,
                'currentRace' => $currentRace,
                'results' => $results,
                'leagues' => $leagues,
                'superAdmin' => $this->security->superAdmin($idUser, $leagueInfo->id),
            ]);
    }
}
