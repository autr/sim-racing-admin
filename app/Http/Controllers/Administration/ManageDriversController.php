<?php

namespace App\Http\Controllers\Administration;

use App\Driver;
use App\DriverClass;
use App\Http\Requests\AddClassesRequest;
use App\Http\Requests\AddDriversRequest;
use App\Http\Requests\LoadFileRequest;
use App\Http\Requests\UpdateDriverRequest;
use App\League;
use App\Managed_league;
use App\Misc\CsvReader;
use App\Misc\Security;
use App\Request;
use App\Season;
use App\Split;
use App\Team;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Lang;

class ManageDriversController extends Controller {
    protected $security;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware(['auth']);
        $this->security = new Security();
    }

    /**
     * Show drivers management forms
     *
     * @param string $league
     * @param string $split
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View|void
     */
    public function index(string $league, string $split) {
        $idUser = Auth::user()->id;
        $leagueInfo = League::where('uri', $league)->first();

        // Verifying that user can edit selected league
        if (!$leagueInfo or !$this->security->canEditSplit($idUser, $leagueInfo->id, $split)) {
            return abort(403);
        }

        $splitInfo = Split::find($split);

        // Get drivers and teams for current season
        $seasonInfo = Season::getCurrentSeason($split);
        $driversInfo = Driver::getCurrentDrivers($seasonInfo->id);
        $teamsInfo = Team::where('idSplit', $seasonInfo->id)->get();
        $classesInfo = DriverClass::where('idSplit', $seasonInfo->id)->get();

        $leagues = League::getUsersLeagues($idUser);

        return view('administration.drivers',
            [
                'league' => $league,
                'splitId' => $split,
                'splitInfo' => $splitInfo,
                'leagueInfo' => $leagueInfo,
                'drivers' => $driversInfo,
                'teams' => $teamsInfo,
                'leagues' => $leagues,
                'classes' => $classesInfo,
                'superAdmin' => $this->security->superAdmin($idUser, $leagueInfo->id),
            ]);
    }

    /**
     * Adds drivers into the database
     *
     * @param AddDriversRequest $request
     * @param string $league
     * @param string $split
     * @return \Illuminate\Http\JsonResponse|void
     */
    public function store(AddDriversRequest $request, string $league, string $split) {
        $idUser = Auth::user()->id;
        $leagueInfo = League::where('uri', $league)->first();

        // Verifying that user can edit selected league
        if (!$leagueInfo or !$this->security->canEditSplit($idUser, $leagueInfo->id, $split)) {
            return abort(403);
        }

        $validated = $request->validated();

        DB::beginTransaction();
        try {
            // Get season info
            $seasonInfo = Season::getCurrentSeason($split);

            // Split drivers by a new line
            $drivers = preg_split('/\r\n|[\r\n]/',$validated['drivers']);

            // Save each driver to the database
            foreach ($drivers as $driver) {
                Driver::create([
                    'name' => $driver,
                    'idSplit' => $seasonInfo->id,
                    'idTeam' => NULL,
                    'active' => 1,
                    'idClass' => NULL,
                ]);
            }

            DB::commit();
        } catch (\Exception $ex) {
            DB::rollBack();
            return response()->json(['errors' => [Lang::get('messages.error_add', [
                'name' => Lang::get('vars.driver(s)')
            ])]]);
        }

        return response()->json(['success' => Lang::get('messages.success_add', [
            'name' => ucfirst(Lang::get('vars.driver(s)'))
        ])]);
    }

    /**
     * Updates driver information
     *
     * @param UpdateDriverRequest $request
     * @param string $league
     * @param string $split
     * @param string $driver
     * @return \Illuminate\Http\JsonResponse|void
     */
    public function update(UpdateDriverRequest $request, string $league, string $split, string $driver) {
        $idUser = Auth::user()->id;
        $leagueInfo = League::where('uri', $league)->first();

        // Verifying that user can edit selected league
        if (!$leagueInfo or !$this->security->canEditSplit($idUser, $leagueInfo->id, $split)) {
            return abort(403);
        }

        $validated = $request->validated();

        DB::beginTransaction();
        try {
            // Get season info
            $seasonInfo = Season::where('idSplit', $split)
                ->orderBy('season', 'desc')
                ->first();

            // Get split info
            $splitInfo = Split::find($split);

            // Get driver info
            $driverInfo = Driver::find($driver);

            // Check if team is full
            if ($driverInfo->idTeam != $validated['team'] and $validated['team'] != -1 and Team::isFull($validated['team'], $seasonInfo->id, $splitInfo->max_per_team)) {
                DB::rollBack();
                return response()->json(['errors' => [Lang::get('messages.error_team_full')]]);
            }

            if (!$validated['active'] and $validated['team'] != -1) {
                DB::rollBack();
                return response()->json(['errors' => [Lang::get('messages.error_inactive')]]);
            }

            // Update driver
            $driverInfo->name = $validated['name'];
            $driverInfo->idTeam = ($validated['team'] == -1 or !$validated['active']) ? NULL : $validated['team'];
            $driverInfo->active = $validated['active'];
            $driverInfo->idClass = $validated['class'] == -1 ? NULL : $validated['class'];

            $driverInfo->save();

            DB::commit();
        } catch (\Exception $ex) {
            DB::rollBack();
            return response()->json(['errors' => [Lang::get('messages.error_update', [
                'name' => ucfirst(Lang::get('vars.driver'))
            ])]]);
        }
        return response()->json(['success' => Lang::get('messages.success_update', [
            'name' => ucfirst(Lang::get('vars.driver'))
        ])]);
    }

    /**
     * Removes driver or makes driver inactive
     *
     * @param Request $request
     * @param string $league
     * @param string $split
     * @param string $driver
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|void
     */
    public function destroy(Request $request, string $league, string $split, string $driver) {
        $idUser = Auth::user()->id;
        $leagueInfo = League::where('uri', $league)->first();

        // Verifying that user can edit selected league
        if (!$leagueInfo or !$this->security->canEditSplit($idUser, $leagueInfo->id, $split)) {
            return abort(403);
        }

        DB::beginTransaction();
        try {
            // Get driver info
            $driverInfo = Driver::find($driver);

            // If driver has results make him inactive
            if ($driverInfo->hasResults()) {
                // Update driver
                $driverInfo->idTeam = NULL;
                $driverInfo->active = 0;

                $driverInfo->save();

                $message = Lang::get('messages.success_driver_deactivated');
            } else {
                $driverInfo->delete();
                $message = Lang::get('messages.success_driver_removed');
            }
            DB::commit();
        } catch (\Exception $ex) {
            DB::rollBack();
            return response()->json(['errors' => [Lang::get('messages.error_deactivate_driver')]]);
        }
        return response()->json(['success' => $message]);
    }

    /**
     * Shows form for loading drivers and teams from file
     *
     * @param string $league
     * @param string $split
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View|void
     */
    public function createFromFile(string $league, string $split) {
        $idUser = Auth::user()->id;
        $leagueInfo = League::where('uri', $league)->first();

        // Verifying that user can edit selected league
        if (!$leagueInfo or !$this->security->canEditSplit($idUser, $leagueInfo->id, $split)) {
            return abort(403);
        }

        $splitInfo = Split::find($split);

        $leagues = League::getUsersLeagues($idUser);

        return view('administration.load-file',
            [
                'league' => $league,
                'splitId' => $split,
                'splitInfo' => $splitInfo,
                'leagueInfo' => $leagueInfo,
                'leagues' => $leagues,
                'superAdmin' => $this->security->superAdmin($idUser, $leagueInfo->id),
            ]);
    }

    /**
     * Stores drivers and teams from file
     *
     * @param Request $request
     * @param string $league
     * @param string $split
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View|void
     */
    public function storeFromFile(LoadFileRequest $request, string $league, string $split) {
        $idUser = Auth::user()->id;
        $leagueInfo = League::where('uri', $league)->first();

        // Verifying that user can edit selected league
        if (!$leagueInfo or !$this->security->canEditSplit($idUser, $leagueInfo->id, $split)) {
            return abort(403);
        }

        $splitInfo = Split::find($split);
        $seasonInfo = Season::getCurrentSeason($split);
        $leagues = League::getUsersLeagues($idUser);
        $teamsInfo = Team::where('idSplit', $seasonInfo->id)->get();

        $validated = $request->validated();
        $csvReader = new CsvReader();

        $driverAdded = false;

        DB::beginTransaction();
        try {
            $csvLines = $csvReader->ReadCsv($validated['file'], ['delimiter' => ',']);

            foreach ($csvLines as $line) {
                $team = null;
                // Team is set and is not empty
                if (isset($line[1]) and $line[1] !== '') {
                    // Find if team already exists
                    $team = Team::where('name', $line[1])->first();

                    if (!$team) {
                        // If not, create it
                        $team = Team::create([
                            'name' => $line[1],
                            'idSplit' => $seasonInfo->id,
                            'primaryColour' => '#ffffff'
                        ]);
                    }
                }

                // Driver is set and is not empty
                if (isset($line[0]) and $line[0] !== '') {
                    // Create new driver
                    Driver::create([
                        'name' => $line[0],
                        'idSplit' => $seasonInfo->id,
                        'idTeam' => (is_null($team) or Team::isFull($team->id, $seasonInfo->id, $splitInfo->max_per_team)) ? NULL : $team->id,
                        'active' => 1
                    ]);
                    $driverAdded = true;
                }
            }

            DB::commit();
        } catch (\Exception $ex) {
            DB::rollBack();
            return redirect()->back()->with('errorMessage', Lang::get('messages.error_load'));
        }

        return redirect()->route($driverAdded ? 'drivers.index' : 'teams.index', [urlencode($league), $split])
                ->with('message', Lang::get('messages.success_load'));
    }

    /**
     * Adds drivers into the database
     *
     * @param AddClassesRequest $request
     * @param string $league
     * @param string $split
     * @return \Illuminate\Http\JsonResponse|void
     */
    public function storeClasses(AddClassesRequest $request, string $league, string $split): \Illuminate\Http\JsonResponse {
        $idUser = Auth::user()->id;
        $leagueInfo = League::where('uri', $league)->first();


        // Verifying that user can edit selected league
        if (!$leagueInfo or !$this->security->canEditSplit($idUser, $leagueInfo->id, $split)) {
            return abort(403);
        }

        $validated = $request->validated();

        DB::beginTransaction();
        try {
            // Get season info
            $seasonInfo = Season::getCurrentSeason($split);

            // Split drivers by a new line
            $classes = preg_split('/\r\n|[\r\n]/',$validated['classes']);

            // Save each driver to the database
            foreach ($classes as $class) {
                DriverClass::create([
                    'name' => $class,
                    'idSplit' => $seasonInfo->id,
                ]);
            }

            DB::commit();
        } catch (\Exception $ex) {
            DB::rollBack();
            return response()->json(['errors' => [Lang::get('messages.error_add', [
                'name' => Lang::get('vars.class(es)')
            ])]]);
        }

        return response()->json(['success' => Lang::get('messages.success_add', [
            'name' => ucfirst(Lang::get('vars.class(es)'))
        ])]);
    }

    /**
     * Removes team
     *
     * @param Request $request
     * @param string $league
     * @param string $split
     * @param string $team
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|void
     * @throws \Exception
     */
    public function destroyClass(Request $request, string $league, string $split, string $class) {
        $idUser = Auth::user()->id;
        $leagueInfo = League::where('uri', $league)->first();

        // Verifying that user can edit selected league
        if (!$leagueInfo or !$this->security->canEditSplit($idUser, $leagueInfo->id, $split)) {
            return abort(403);
        }

        DB::beginTransaction();
        try {
            // Get class info
            $classInfo = DriverClass::find($class);

            $classInfo->delete();
            DB::commit();
        } catch (\Exception $ex) {
            DB::rollBack();
            return response()->json(['errors' => [Lang::get('messages.error_deactivate_driver')]]);
        }
        return response()->json(['success' => Lang::get('messages.success_class_removed')]);
    }
}
