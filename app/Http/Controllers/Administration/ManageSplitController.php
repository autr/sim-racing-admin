<?php

namespace App\Http\Controllers\Administration;

use App\Driver;
use App\Game;
use App\Http\Requests\AddDriversRequest;
use App\Http\Requests\AddTeamsRequest;
use App\Http\Requests\CreateSplitRequest;
use App\Http\Requests\UpdateDriverRequest;
use App\Http\Requests\UpdateSplitRequest;
use App\League;
use App\Managed_league;
use App\Misc\Security;
use App\Platform;
use App\Region;
use App\Season;
use App\Split;
use App\Team;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Lang;

class ManageSplitController extends Controller {
    protected $redirHome = '/home';
    protected $security;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('auth');
        $this->security = new Security();
    }

    /**
     * Shows split overview
     *
     * @param string $league
     * @param string $split
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View|void
     */
    public function index(string $league, string $split) {
        $idUser = Auth::user()->id;
        $leagueInfo = League::where('uri', $league)->first();

        // Verifying that user can edit selected league
        if (!$leagueInfo or !$this->security->canEditSplit($idUser, $leagueInfo->id, $split)) {
            return abort(403);
        }

        $splitInfo = Split::find($split);
        $season = Season::getCurrentSeason($split);
        $leagues = League::getUsersLeagues($idUser);

        $platforms = Platform::all();
        $games = Game::all();
        $regions = Region::all();

        return view('administration.split',
            [
                'league' => $league,
                'leagueInfo' => $leagueInfo,
                'splitId' => $split,
                'splitInfo' => $splitInfo,
                'season' => $season->season,
                'platforms' => $platforms,
                'games' => $games,
                'regions' => $regions,
                'leagues' => $leagues,
                'superAdmin' => $this->security->superAdmin($idUser, $splitInfo->league->id),
            ]);
    }

    /**
     * Shows new split form
     *
     * @param string $league
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View|void
     */
    public function create(string $league) {
        $idUser = Auth::user()->id;
        $leagueInfo = League::where('uri', $league)->first();

        // Verifying that user can edit selected league
        if (!$leagueInfo or !$this->security->canEditLeague($idUser, $leagueInfo->id)) {
            return abort(403);
        }

        // Check if user is superadmin for selected league
        if (!$this->security->superAdmin($idUser, $leagueInfo->id)) {
            return redirect($this->redirHome)
                ->with('errorMessage', Lang::get('messages.error_denied'));
        }

        $platforms = Platform::all();
        $games = Game::all();
        $regions = Region::all();

        $leagues = League::getUsersLeagues($idUser);

        return view('administration.create-split',
            [
                'league' => $league,
                'leagueInfo' => $leagueInfo,
                'platforms' => $platforms,
                'games' => $games,
                'regions' => $regions,
                'leagues' => $leagues,
                'superAdmin' => $this->security->superAdmin($idUser, $leagueInfo->id),
                'leagueName' => $leagueInfo->name
            ]);
    }

    /**
     * Create new split for selected league
     *
     * @param CreateSplitRequest $request
     * @param string $league
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|void
     */
    public function store(CreateSplitRequest $request, string $league) {
        $redirTo = 'a/' . urlencode($league);
        $idUser = Auth::user()->id;
        $leagueInfo = League::where('uri', $league)->first();

        // Verifying that user can edit selected league
        if (!$leagueInfo or !$this->security->canEditLeague($idUser, $leagueInfo->id)) {
            return abort(403);
        }

        // Check if user is superadmin for selected league
        if (!$this->security->superAdmin($idUser, $leagueInfo->id)) {
            return redirect($this->redirHome)
                ->with('errorMessage', Lang::get('messages.error_denied'));
        }

        $validated = $request->validated();

        DB::beginTransaction();
        try {
            // Get league information
            $leagueInfo = League::where('uri', $league)->first();

            // Create new split
            $split = Split::create([
                'name' => $validated['splitName'],
                'idLeague' => $leagueInfo->id,
                'idPlatform' => $validated['idPlatform'],
                'idGame' => $validated['idGame'],
                'idRegion' => $validated['idRegion'],
                'max_per_team' => $validated['maxPerTeam'],
                'max_per_race' => $validated['maxPerRace'],
                'points' => $validated['points'],
                'no_points_dnf' => isset($validated['noPointsDnf']) ? $validated['noPointsDnf'] : 0,
                'omit_inactive' => isset($validated['omitInactive']) ? $validated['omitInactive'] : 0
            ]);

            // Create record in managed leagues table
            $managed = Managed_league::create([
                'idAdmin' => Auth::user()->id,
                'idSplit' => $split->id,
                'idLeague' => $leagueInfo->id
            ]);

            // Create new season
            $season = Season::create([
                'idSplit' => $split->id,
                'season' => $validated['season']
            ]);

            DB::commit();
        } catch (\Exception $ex) {
            DB::rollBack();
            return redirect($redirTo)
                ->with('errorMessage', Lang::get('messages.error_create',
                    ['name' => Lang::get('split')]));
        }

        return redirect($redirTo)
            ->with('message', Lang::get('messages.success_create',
                ['name' => ucfirst(Lang::get('split'))]));
    }

    /**
     * Update split's information
     *
     * @param CreateSplitRequest $request
     * @param string $league
     * @param string $split
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|void
     */
    public function update(UpdateSplitRequest $request, string $league, string $split) {
        $idUser = Auth::user()->id;
        $leagueInfo = League::where('uri', $league)->first();

        // Verifying that user can edit selected league
        if (!$leagueInfo or !$this->security->canEditSplit($idUser, $leagueInfo->id, $split)) {
            return abort(403);
        }

        $validated = $request->validated();

        DB::beginTransaction();
        try {
            // Get split information
            $splitInfo = Split::where('id', $split)->first();

            // Change attributes
            $splitInfo->name = $validated['splitName'];
            $splitInfo->idPlatform = $validated['idPlatform'];
            $splitInfo->idGame = $validated['idGame'];
            $splitInfo->idRegion = $validated['idRegion'];
            $splitInfo->max_per_team = $validated['maxPerTeam'];
            $splitInfo->max_per_race = $validated['maxPerRace'];
            $splitInfo->points = $validated['points'];
            $splitInfo->no_points_dnf = isset($validated['noPointsDnf']) ? $validated['noPointsDnf'] : 0;
            $splitInfo->omit_inactive = isset($validated['omitInactive']) ? $validated['omitInactive'] : 0;

            // Update the database
            $splitInfo->save();

            DB::commit();
        } catch (\Exception $ex) {
            DB::rollBack();
            return response()->json(['errors' => [Lang::get('messages.error_update',
                ['name' => Lang::get('vars.split')])]]);
        }
        return response()->json(['success' => Lang::get('messages.success_update', [
            'name' => ucfirst(Lang::get('vars.split'))
        ])]);
    }

    /**
     * Starts new season
     *
     * @param Request $request
     * @param string $league
     * @param string $split
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|void
     */
    public function startNewSeason(Request $request, string $league, string $split) {
        $idUser = Auth::user()->id;
        $leagueInfo = League::where('uri', $league)->first();

        // Verifying that user can edit selected league
        if (!$leagueInfo or !$this->security->canEditSplit($idUser, $leagueInfo->id, $split)) {
            return abort(403);
        }

        DB::beginTransaction();
        try {
            // Get current season
            $seasonInfo = Season::getCurrentSeason($split);

            // Create new season
            $newSeason = Season::create([
               'idSplit' => $split,
               'season' => $seasonInfo->season + 1,
            ]);

            // If driver transfer enabled, create new drivers from current ones for the new season
            if ($request['transferDrivers']) {
                $drivers = Driver::where('idSplit', $seasonInfo->id)
                    ->where('active', 1)
                    ->get();

                foreach ($drivers as $driver) {
                    Driver::create([
                        'name' => $driver->name,
                        'idSplit' => $newSeason->id,
                        'idTeam' => NULL,
                        'active' => 1
                    ]);
                }
            }

            // If team transfer enabled, create new drivers from current ones for the new season
            if ($request['transferTeams']) {
                $teams = Team::where('idSplit', $seasonInfo->id)->get();

                foreach ($teams as $team) {
                    Team::create([
                        'name' => $team->name,
                        'idSplit' => $newSeason->id,
                        'primaryColour' => $team->primaryColour,
                        'secondaryColour1' => $team->secondaryColour1,
                        'secondaryColour2' => $team->secondaryColour2
                    ]);
                }
            }

            DB::commit();
        } catch (\Exception $ex) {
            DB::rollBack();
            return response()->json(['errors' => [Lang::get('messages.error_season')]]);
        }

        return response()->json(['success' => [Lang::get('messages.success_season')]]);
    }

    /**
     * Deletes selected split
     *
     * @param string $league
     * @param int $split
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|void
     * @throws \Exception
     */
    public function destroy(string $league, int $split) {
        $idUser = Auth::user()->id;
        $leagueInfo = League::where('uri', $league)->first();

        // Verifying that user can edit selected league
        if (!$leagueInfo or !$this->security->superAdmin($idUser, $leagueInfo->id)) {
            return abort(403);
        }

        DB::beginTransaction();
        try {
            $splits = Split::where('idLeague', $leagueInfo->id)->count();

            if ($splits < 2) {
                // Delete the league if the split is last
                $leagueInfo->delete();
                $redirTo = 'home';
                $message = Lang::get('messages.success_delete_league');
            } else {
                // Delete the split
                $splitInfo = Split::find($split);

                $splitInfo->delete();
                $redirTo = 'a/' . urlencode($league);
                $message = Lang::get('messages.success_delete_split');
            }
            DB::commit();
        } catch (\Exception $ex) {
            DB::rollback();
            $redirTo = 'a/' . urlencode($league) . '/' . $split;
            return redirect($redirTo)
                ->with('errorMessage', Lang::get('messages.error_delete_split'));
        }
        return redirect($redirTo)
            ->with('message', $message);
    }
}
