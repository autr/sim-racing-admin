<?php

namespace App\Http\Controllers;

use App\League;
use App\Managed_league;
use App\Split;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class BrowseController extends Controller {
    /**
     * Show browsing page
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index() {
        $splits = Split::select('splits.*')
            ->leftJoin('leagues', 'splits.idLeague', '=', 'leagues.id')
            ->orderBy('leagues.name')
            ->orderBy('splits.name')
            ->paginate(15);

        $leagues = [];
        if (Auth::check()) {
            $idUser = Auth::user()->id;
            $leagues = League::getUsersLeagues($idUser);
        }

        return view('browse',
            [
                'splits' => $splits,
                'leagues' => $leagues,
            ]);
    }

    /**
     * Show searched leagues
     *
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
     */
    public function search(Request $request) {
        $leagues = [];
        if (Auth::check()) {
            $idUser = Auth::user()->id;
            $leagues = League::getUsersLeagues($idUser);
        }

        if (!$request->has('q') or $request->q == '') {
            return redirect(route('browse'));
        }

        $splits = Split::select('splits.*')
            ->leftJoin('leagues', 'splits.idLeague', '=', 'leagues.id')
            ->leftJoin('platforms', 'splits.idPlatform', '=', 'platforms.id')
            ->leftJoin('games', 'splits.idGame', '=', 'games.id')
            ->leftJoin('regions', 'splits.idRegion', '=', 'regions.id')
            ->where('leagues.name', 'like', '%' . $request->q . '%')
            ->orWhere('splits.name', 'like', '%' . $request->q . '%')
            ->orWhere('platforms.name', 'like', '%' . $request->q . '%')
            ->orWhere('games.name', 'like', '%' . $request->q . '%')
            ->orWhere('regions.name', 'like', '%' . $request->q . '%')
            ->orderBy('leagues.name')
            ->orderBy('splits.name')
            ->paginate(15);

        return view('browse',
            [
                'splits' => $splits,
                'leagues' => $leagues,
            ]);
    }
}
