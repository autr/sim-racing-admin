<?php

namespace App\Http\Controllers\Presentation;

use App\Driver;
use App\Http\Controllers\Controller;
use App\League;
use App\Penalty;
use App\Penalty_point;
use App\Penalty_type;
use App\Race;
use App\Result;
use App\Season;
use App\Split;
use App\Team;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Stevebauman\Purify\Facades\Purify;
use function foo\func;

class PresentationController extends Controller {
    /**
     * Shows basic information about selected league
     *
     * @param string $league
     * @param int $split
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(string $league, int $split) {
        $leagueInfo = League::where('uri', $league)->first();

        $splitInfo = Split::find($split);

        $seasonInfo = Season::getCurrentSeason($split);

        $leagues = null;
        if (Auth::check()) {
            $leagues = League::getUsersLeagues(Auth::user()->id);
        }

        return view('presentation.information',
            [
                'league' => $league,
                'splitId' => $split,
                'leagueInfo' => $leagueInfo,
                'splitInfo' => $splitInfo,
                'leagues' => $leagues,
            ]);
    }

    /**
     * Shows league description
     *
     * @param string $league
     * @param int $split
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function description(string $league, int $split) {
        $leagues = null;
        if (Auth::check()) {
            $leagues = League::getUsersLeagues(Auth::user()->id);
        }

        $leagueInfo = League::where('uri', $league)->first();
        //TODO: This is a fast fix, find better way (Purify doesn't work at my hosting for some reason)
        $leagueDescription = preg_replace('#<script(.*?)>(.*?)</script>#is', '~Removed script~', $leagueInfo->description);//Purify::clean($leagueInfo->description);
        $splitInfo = Split::find($split);

        return view('presentation.description',
            [
                'league' => $league,
                'splitId' => $split,
                'leagueInfo' => $leagueInfo,
                'leagueDescription' => $leagueDescription,
                'splitInfo' => $splitInfo,
                'leagues' => $leagues,
            ]);
    }

    /**
     * Shows driver and team lineups for selected league/split
     *
     * @param string $league
     * @param int $split
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function lineups(string $league, int $split) {
        $leagues = null;
        if (Auth::check()) {
            $leagues = League::getUsersLeagues(Auth::user()->id);
        }

        $leagueInfo = League::where('uri', $league)->first();
        $splitInfo = Split::find($split);

        $seasonInfo = Season::getCurrentSeason($split);
        $teamsInfo = Team::where('idSplit', $seasonInfo->id)
            ->orderBy('name')
            ->get();
        $unalignedDrivers = Driver::where('idTeam', null)
            ->where('idSplit', $seasonInfo->id)
            ->get();

        return view('presentation.lineups',
            [
                'league' => $league,
                'splitId' => $split,
                'leagueInfo' => $leagueInfo,
                'splitInfo' => $splitInfo,
                'leagues' => $leagues,
                'teamsInfo' => $teamsInfo,
                'unalignedDrivers' => $unalignedDrivers,
            ]);
    }

    /**
     * Shows race calendar for selected league/split
     *
     * @param string $league
     * @param int $split
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function calendar(string $league, int $split) {
        $leagues = null;
        if (Auth::check()) {
            $leagues = League::getUsersLeagues(Auth::user()->id);
        }

        $leagueInfo = League::where('uri', $league)->first();
        $splitInfo = Split::find($split);

        $seasonInfo = Season::getCurrentSeason($split);
        $calendar = Race::where('idSplit', $seasonInfo->id)
            ->orderByRaw('CASE WHEN races.date IS NULL THEN 1 ELSE 0 END')
            ->orderBy('races.date')
            ->get();

        return view('presentation.calendar',
            [
                'league' => $league,
                'splitId' => $split,
                'leagueInfo' => $leagueInfo,
                'splitInfo' => $splitInfo,
                'leagues' => $leagues,
                'calendar' => $calendar,
            ]);
    }

    /**
     * Shows penalties for selected league/split
     *
     * @param string $league
     * @param int $split
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function penalties(string $league, int $split) {
        $leagues = null;
        if (Auth::check()) {
            $leagues = League::getUsersLeagues(Auth::user()->id);
        }

        $leagueInfo = League::where('uri', $league)->first();
        $splitInfo = Split::find($split);

        $seasonInfo = Season::getCurrentSeason($split);
        $penalties = Penalty::getUpcomingPenalties($seasonInfo->id);
        $penaltyPoints = Penalty_point::getPenaltyPoints($seasonInfo->id);

        return view('presentation.penalties',
            [
                'league' => $league,
                'splitId' => $split,
                'leagueInfo' => $leagueInfo,
                'splitInfo' => $splitInfo,
                'leagues' => $leagues,
                'penalties' => $penalties,
                'penaltyPoints' => $penaltyPoints
            ]);
    }

    /**
     * Shows driver standings for selected league/split
     *
     * @param string $league
     * @param int $split
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function standings(Request $request, string $league, int $split) {
        $leagues = null;
        if (Auth::check()) {
            $leagues = League::getUsersLeagues(Auth::user()->id);
        }

        $leagueInfo = League::where('uri', $league)->first();
        $splitInfo = Split::find($split);

        if ($request->has('season')) {
            $seasonInfo = Season::getSeason($request->season, $split);
        } else {
            $seasonInfo = Season::getCurrentSeason($split);
        }

        $calendar = Race::where('idSplit', $seasonInfo->id)
            ->orderByRaw('CASE WHEN races.date IS NULL THEN 1 ELSE 0 END')
            ->orderBy('races.date')
            ->get();
        $results = $this->getResults($calendar, $seasonInfo);

        $seasons = Season::getAllSeasons($split);

        return view('presentation.standings',
            [
                'league' => $league,
                'splitId' => $split,
                'leagueInfo' => $leagueInfo,
                'splitInfo' => $splitInfo,
                'leagues' => $leagues,
                'calendar' => $calendar,
                'results' => $results,
                'seasons' => $seasons,
                'selectedSeason' => $seasonInfo->id
            ]);
    }

    /**
     * Shows team standings for selected league/split
     *
     * @param string $league
     * @param int $split
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function teamStandings(string $league, int $split) {
        $leagues = null;
        if (Auth::check()) {
            $leagues = League::getUsersLeagues(Auth::user()->id);
        }

        $leagueInfo = League::where('uri', $league)->first();
        $splitInfo = Split::find($split);

        $seasonInfo = Season::getCurrentSeason($split);
        $teamStandings = $this->getTeamStandings($seasonInfo);

        return view('presentation.team-standings',
            [
                'league' => $league,
                'splitId' => $split,
                'leagueInfo' => $leagueInfo,
                'splitInfo' => $splitInfo,
                'leagues' => $leagues,
                'teamStandings' => $teamStandings,
            ]);
    }

    /*
     * Standings
     * Team standings
     * */

    /**
     * Gets driver results
     *
     * @param $calendar
     * @param $season
     * @return array Table with results
     */
    private function getResults($calendar, $season) {
        $drivers = Driver::where('idSplit', $season->id)
            ->when($season->split->omit_inactive, function($q) {
                return $q->where('active', 1);
            })
            ->get();

        $results = [];
        $c = 0;
        foreach ($drivers as $driver) {
            $results[$c]['id'] = $driver->id;
            $results[$c]['driver'] = $driver->name;
            $results[$c]['team'] = $driver->team;
            $results[$c]['points'] = Result::where('idDriver', $driver->id)->sum('points') + Result::where('idDriver', $driver->id)->sum('bonusPoints');
            foreach ($calendar as $race) {
                $position = 'TBR';
                $result = Result::where('idDriver', $driver->id)->where('idRace', $race->id)->first();
                if ($result) {
                    if (!is_null($result->bonusPoints) and $result->bonusPoints > 0) {
                        $position = $result->position.'+';
                    } else {
                        $position = $result->position;
                    }
                    if ($result->didNotFinish) {
                        $position = $position.' (DNF)';
                    }
                } elseif (!is_null($race->date) and $race->hasResults()) {
                    $position = 'DNS';
                }
                $results[$c]['races'][$race->id] = $position;
            }
            $c++;
        }
        usort($results, function ($a, $b) {
            if ($a['points'] == $b['points']) {
                return $this->sortEqual($a['id'], $b['id']);
            } else {
                return $a['points'] < $b['points'];
            }
        });

        return $results;
    }

    /**
     * Finds which driver is to be at higher position
     *
     * @param $a
     * @param $b
     * @return bool
     */
    private function sortEqual($a, $b) {
        $driverA = Driver::find($a);
        $driverB = Driver::find($b);

        // Skip those with no results
        if (!$driverA->hasResults() and !$driverB->hasResults()) {
            return false;
        } else if (!$driverA->hasResults()) {
            return true;
        } else if (!$driverB->hasResults()) {
            return false;
        } else {
            // Find min and max position of the driver
            $minA = Result::where('idDriver', $driverA->id)->min('position');
            $maxA = Result::where('idDriver', $driverA->id)->max('position');
            $minB = Result::where('idDriver', $driverB->id)->min('position');
            $maxB = Result::where('idDriver', $driverB->id)->max('position');

            // Compare min positions
            if ($minA < $minB) {
                return false;
            } else if ($minA > $minB) {
                return true;
            } else {
                // Compare other positions when mins are equal
                $count = min($minA, $minB);
                $max = min($maxA, $maxB);

                for ($pos = $count; $pos < $max; $pos++) {
                    $countPosA = Result::where('idDriver', $driverA->id)->where('position', $pos)->count();
                    $countPosB = Result::where('idDriver', $driverB->id)->where('position', $pos)->count();
                    if ($countPosA < $countPosB) {
                        return true;
                    }
                }

                // If non have higher position, find driver with more starts
                $countResA = Result::where('idDriver', $driverA->id)->count();
                $countResB = Result::where('idDriver', $driverB->id)->count();
                return $countResA < $countResB;
            }
        }
    }

    /**
     * Gets team standings
     *
     * @param $season
     * @return mixed
     */
    private function getTeamStandings($season) {
        $teamResults = [];
        $c = 0;
        $teams = Team::where('idSplit', $season->id)->get();
        foreach ($teams as $team) {
            $teamResults[$c]['name'] = $team->name;
            $teamResults[$c]['primaryColour'] = $team->primaryColour;
            $teamResults[$c]['secondaryColour1'] = $team->secondaryColour1;
            $teamResults[$c]['secondaryColour2'] = $team->secondaryColour2;

            $teamResults[$c]['points'] = Result::where('idTeam', $team->id)->sum('points') + Result::where('idTeam', $team->id)->sum('bonusPoints');
            $c++;
        }
        usort($teamResults, function ($a, $b) {
            return $a['points'] < $b['points'];
        });

        return $teamResults;
    }
}
