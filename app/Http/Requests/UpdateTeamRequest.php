<?php

namespace App\Http\Requests;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class UpdateTeamRequest extends FormRequest {
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        return Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        return [
            'name' => 'required|string|max:100',
            'primary' => 'required|string|max:7',
            'secondary1' => 'string|max:7',
            'secondary2' => 'string|max:7',
        ];
    }

    /**
     * Custom response when validation fails
     *
     * @param Validator $validator
     * @throws \Illuminate\Validation\ValidationException
     */
    protected function failedValidation(Validator $validator) {
        $response = response()->json(['errors'=> $validator->errors()]);

        throw new \Illuminate\Validation\ValidationException($validator, $response);
    }
}
