<?php

namespace App\Http\Requests;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class CreateSplitRequest extends FormRequest {
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        return Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        return [
            'splitName' => 'required|string|max:100',
            'idPlatform' => 'required|integer|min:1',
            'idGame' => 'required|integer|min:1',
            'idRegion' => 'required|string|size:2',
            'maxPerTeam' => 'required|integer|min:0',
            'maxPerRace' => 'required|integer|min:1',
            'points' => 'required|string|regex:/\d+(,\d+)*/',
            'season' => 'required|integer|min:1',
            'noPointsDnf' => 'integer',
            'omitInactive' => 'integer'
        ];
    }
}
