<?php

namespace App\Http\Middleware;

use App\League;
use App\Misc\Security;
use Closure;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Lang;

class CanEdit {
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next) {
        $idUser = Auth::user()->id;
        $leagueInfo = League::where('uri', $request->league)->first();
        $security = new Security();

        // Verifying that user can edit selected league
        if (!$leagueInfo or !$security->canEditSplit($idUser, $leagueInfo->id, $request->split)) {
            return redirect('/home')
                ->with('errorMessage', Lang::get('messages.error_denied'));
        }

        return $next($request);
    }
}
