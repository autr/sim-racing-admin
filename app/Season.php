<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Season extends Model {
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'idSplit', 'season'
    ];

    /**
     * Returns managed split
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function split(): \Illuminate\Database\Eloquent\Relations\BelongsTo {
        return $this->belongsTo('App\Split', 'idSplit');
    }

    /**
     * Returns driver classes
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function driverClasses(): \Illuminate\Database\Eloquent\Relations\HasMany {
        return $this->hasMany('App\Split', 'idSplit');
    }

    /**
     * Returns current season for selected split
     *
     * @param $idSplit
     * @return mixed
     */
    public static function getCurrentSeason($idSplit) {
        return Season::where('idSplit', $idSplit)
            ->orderBy('season', 'desc')
            ->first();
    }

    /**
     * Returns selected season
     *
     * @param $season
     * @param $idSplit
     * @return mixed
     */
    public static function getSeason($season, $idSplit) {
        $season = Season::find($season);
        return $season ?: Season::getCurrentSeason($idSplit);
    }

    /**
     * Returns all seasons for selected split
     *
     * @param $idSplit
     * @return mixed
     */
    public static function getAllSeasons($idSplit) {
        return Season::where('idSplit', $idSplit)
            ->orderBy('season', 'desc')
            ->get();
    }
}
