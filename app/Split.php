<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Split extends Model {
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'idLeague', 'idPlatform', 'idGame', 'idRegion', 'max_per_team', 'max_per_race', 'points',
        'no_points_dnf', 'omit_inactive'
    ];

    /**
     * Returns platform for selected split
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function platform() {
        return $this->belongsTo('App\Platform', 'idPlatform');
    }

    /**
     * Returns game for selected split
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function game() {
        return $this->belongsTo('App\Game', 'idGame');
    }

    /**
     * Returns region for selected split
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function region() {
        return $this->belongsTo('App\Region', 'idRegion');
    }

    /**
     * Returns league for selected split
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function league() {
        return $this->belongsTo('App\League', 'idLeague');
    }

    /**
     * Returns array of points for selected split
     *
     * @return array
     */
    public function getPoints() {
        // Get points
        $points = array_fill(1, $this->max_per_race, 0);
        $splitPoints = array_map('intval',explode(',', $this->points));
        $i = 1;
        foreach ($splitPoints as $splitPoint) {
            $points[$i++] = $splitPoint;
        }

        return $points;
    }
}
