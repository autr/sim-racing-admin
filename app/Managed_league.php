<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Managed_league extends Model {
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'idLeague', 'idAdmin', 'idSplit'
    ];

    /**
     * Returns managed league
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function league() {
        return $this->belongsTo('App\League', 'idLeague');
    }

    /**
     * Returns managed split
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function split() {
        return $this->belongsTo('App\Split', 'idSplit');
    }
}
