<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Penalty_point extends Model {
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'idDriver', 'idSplit', 'idRace', 'amount',
    ];

    /**
     * Returns driver for selected penalty point record
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function driver() {
        return $this->belongsTo('App\Driver', 'idDriver');
    }

    /**
     * Returns season for selected penalty point record
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function split() {
        return $this->belongsTo('App\Season', 'idSplit');
    }

    /**
     * Returns race for selected penalty point record
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function race() {
        return $this->belongsTo('App\Race', 'idRace');
    }

    /**
     * Returns list of drivers with penalty points for current season of selected split
     *
     * @param int $split
     * @return mixed
     */
    public static function getPenaltyPoints(int $splitSeason) {
        return Penalty_point::select(DB::raw('sum(amount) as overall_amount, date(min(created_at)) as oldest,
                date(max(created_at)) as newest, idDriver'))
            ->where('idSplit', $splitSeason)
            ->groupBy('idDriver')
            ->orderBy('overall_amount', 'desc')
            ->get();
    }
}
