<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DriverClass extends Model {
    protected $table = 'classes';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'idSplit'
    ];

    /**
     * Returns valid split season
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function split(): \Illuminate\Database\Eloquent\Relations\BelongsTo {
        return $this->belongsTo('App\Season', 'idSplit');
    }
}
