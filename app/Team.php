<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Team extends Model {
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'idSplit', 'primaryColour', 'secondaryColour1', 'secondaryColour2',
    ];

    public function drivers() {
        return $this->hasMany('App\Driver', 'idTeam');
    }

    /**
     * Returns true when there are results for the team
     *
     * @return bool
     */
    public function hasResults() {
        $results = Result::where('idTeam', $this->id)->get();
        return count($results) > 0;
    }

    /**
     * Checks if the team is full or not
     *
     * @param int $team
     * @param int $split
     * @param int $max
     * @return bool
     */
    public static function isFull(int $team, int $split, int $max) {
        if ($team == -1) return false;
        if ($max == 0) return false;

        $drivers = Driver::where('idTeam', $team)
            ->where('idSplit', $split)
            ->get();

        return count($drivers) >= $max;
    }
}
