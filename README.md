# Sim-Racing Admin
A useful tool for sim-racing administrators, that should make league administration easier.

## Information
This project started as a personal project to practice web development. An opportunity occurred to turn it into my 
master's degree thesis. 

My main goal is to make a tool that could replace spreadsheets that many leagues use nowadays. A tool that offers 
quality of life features and offers an automation for tasks where mistakes can be made very often.

## Availability
The app is available at [https://simracingadmin.com/](https://simracingadmin.com/).

## Roadmap
This is predicted roadmap. It may change in the future, since I have other responsibilities.

#### v0.1.x - April :heavy_check_mark:
- Initial layout :heavy_check_mark:
- User management :heavy_check_mark:
#### v0.2.x - May/June :heavy_check_mark:
- League management :heavy_check_mark:
- Split management :heavy_check_mark:
#### v0.3.x - July :heavy_check_mark:
- Driver management :heavy_check_mark:
- Team management :heavy_check_mark:
- Season management :heavy_check_mark:
#### v0.4.x - July :heavy_check_mark:
- Racing calendar management :heavy_check_mark:
#### v0.5.x - Q1 2020 :heavy_check_mark:
- Racing results management :heavy_check_mark:
- Penalty management :heavy_check_mark:
#### v0.6.x - Q1 2020 :heavy_check_mark:
- Presentation foundation for visualisation :heavy_check_mark:
#### v0.7.x - Q1 2020 :heavy_check_mark:
- Driver and team lineups visualisation :heavy_check_mark:
#### v0.8.x - Q2 2020 :heavy_check_mark:
- Racing calendar visualisation :heavy_check_mark:
- Racing results visualisation :heavy_check_mark:
#### v0.9.x - Q4 2020
- Proper hosting + domain :heavy_check_mark:
- Fixing bugs from public testing
- Improving features
#### v1.0.x - 2021
- Screenshot generator
- API for communities
#### v1.x+
- Medium probability
    - Localizations
    - Statistics
- Low probability
    - Screenshot OCR for race results
    - Custom themes for visualisation
    
**Used plugins/packages/libraries**
- [Bootstrap-Cookie-Alert](https://github.com/Wruczek/Bootstrap-Cookie-Alert) by Wruczek
- [canvas2image](https://github.com/hongru/canvas2image) by hongru
- [clipboard.js](https://clipboardjs.com/) by zenorocha
- [html2canvas](https://html2canvas.hertzen.com/) by niklasvh
- [jQuery UI Touch Punch](https://github.com/furf/jquery-ui-touch-punch) by furf
- [Purify](https://github.com/stevebauman/purify) by stevebauman
- [Trumbowyg](https://alex-d.github.io/Trumbowyg/) by Alex-D

