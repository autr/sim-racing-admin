$(document).ready(function () {
    $('#click-scr-standings').click(function () {
        $('.drivers-pts-col').removeClass('sticky-col-pts');
        driverStandings = $('#driver-standings-scr').get(0);
        html2canvas(driverStandings, {
            height: $('#driver-standings-scr').height(),
            width: driverStandings.scrollWidth,
            scrollY: -window.pageYOffset
        }).then(function (canvas) {
            var canvasWidth = canvas.width;
            var canvasHeight = canvas.height;
            Canvas2Image.saveAsPNG(canvas, canvasWidth, canvasHeight, 'driver-standings');
            $('.drivers-pts-col').addClass('sticky-col-pts');
        });
    });
    $('#click-scr-team-standings').click(function () {
        teamStandings = $('#team-standings-scr').get(0);
        html2canvas(teamStandings, {
            height: $('#team-standings-scr').height(),
            width: teamStandings.scrollWidth,
            scrollY: -window.pageYOffset
        }).then(function (canvas) {
            var canvasWidth = canvas.width;
            var canvasHeight = canvas.height;
            Canvas2Image.saveAsPNG(canvas, canvasWidth, canvasHeight, 'team-standings');
        });
    });
});
