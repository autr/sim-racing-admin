$(document).ready(function(){
    const saveResultsBtn = $('#saveResults-Btn');
    const updateResultsBtn = $('#saveChanges-Btn');
    const results = $('#results');
    const raceSelect = $('#race');
    const oldResults = $('#oldResults');

    results.sortable();
    oldResults.sortable();

    saveResultsBtn.click(function () {
        if (results.children('li').length === 0) {
            alert('Add drivers to the results')
        } else {
            $('#resultsForm').submit();
        }
    });

    updateResultsBtn.click(function () {
        $('#resultsEditForm').submit();
    });

    raceSelect.change(function () {
        let path = window.location.pathname.split('/');
        if (path[[path.length - 1]] === "results") {
            window.location.href = window.location.href + '/' + raceSelect.val();
        } else {
            path.pop();
            let strPath = path.join('/');
            window.location.href = window.location.origin + strPath + '/' + raceSelect.val();
        }
    });
});

function addToResults(button) {
    const btn = $(button);
    const results = $('#results');

    const driverId = btn.data('driverid');
    const driverName = btn.data('drivername');
    const driverTeam = btn.data('driverteam');
    const maxPerRace = btn.data('max');
    const c1 = btn.data('c1');
    const c2 = btn.data('c2');
    const c3 = btn.data('c3');

    if (results.children('li').length < maxPerRace) {
        results.append('<li id="resultsDriver-' + driverId + '" class="list-group-item" draggable="true">' +
            '<div class="d-flex justify-content-between">'+
            '   <div>'+driverName +'</div>' +
            (driverTeam ? colour(c1, c2, c3) : '<div class="team-colour"></div>') +
            '   <div class="custom-control custom-checkbox">' +
            '       <input type="checkbox" name="dnf['+driverId+']" class="custom-control-input" id="dnf'+driverId+'">' +
            '       <label class="custom-control-label" for="dnf'+driverId+'">DNF</label>' +
            '   </div>' +
            '   <div class="input-group mb-3 bonuspoints">' +
            '       <input type="number" name="bonusPoints['+driverId+']" class="form-control form-control-sm  bonuspoints-input" min="-999" max="999" aria-label="bonus-points" value="0">' +
            '       <div class="input-group-append bonuspoints-text">' +
            '           <span class="input-group-text">BP</span>' +
            '       </div>' +
            '   </div>' +
            '   <button type="button" onclick="removeFromResults(this)" class="btn bg-accent light-text btn-sm"' +
            '           data-driverid="' + driverId + '"' +
            '           data-drivername="' + driverName + '"' +
            '           data-driverteam="' + driverTeam + '"' +
            '           data-c1="' + c1 + '"' +
            '           data-c2="' + c2 + '"' +
            '           data-c3="' + c3 + '"' +
            '           data-max="' + maxPerRace + '"><i class="fas fa-minus"></i></button>' +
            '</div> ' +
            '<input type="hidden" name="results[]" value="' + driverId + '"></li>');
        $('#availableDriver-' + driverId).remove();
    }
}

function removeFromResults(button) {
    const btn = $(button);
    const availableDrivers = $('#availableDrivers');

    const driverId = btn.data('driverid');
    const driverName = btn.data('drivername');
    const driverTeam = btn.data('driverteam');
    const maxPerRace = btn.data('max');
    const c1 = btn.data('c1');
    const c2 = btn.data('c2');
    const c3 = btn.data('c3');

    availableDrivers.append('<li id="availableDriver-'+driverId+'" class="list-group-item">' +
        '<div class="d-flex justify-content-between">' +
        '   <div>'+driverName +'</div>' +
        (driverTeam ? colour(c1, c2, c3) : '<div class="team-colour"></div>') +
        '       <button type="button" onclick="addToResults(this)" class="btn bg-accent light-text btn-sm"' +
        '           data-driverid="'+driverId+'"' +
        '           data-drivername="'+driverName+'"' +
        '           data-driverteam="' + driverTeam + '"' +
        '           data-c1="' + c1 + '"' +
        '           data-c2="' + c2 + '"' +
        '           data-c3="' + c3 + '"' +
        '           data-max="' + maxPerRace + '"><i class="fas fa-plus"></i></button>' +
        '</div>' +
        '</li>');
    $('#resultsDriver-'+driverId).remove();
}

function colour(c1, c2, c3) {
    if (c2 && c3) {
        return '<div style="height: 25px;width: 60px;background: linear-gradient(110deg, '+c1+' 50%, '+c2+' 75%, '+c3+' 100%);"></div>';
    } else if (c2 && !c3) {
        return '<div style="height: 25px;width: 60px;background: linear-gradient(110deg, '+c1+' 50%, '+c2+' 75%, '+c2+' 100%);"></div>';
    } else if (!c2 && c3) {
        return '<div style="height: 25px;width: 60px;background: linear-gradient(110deg, '+c1+' 50%, '+c3+' 75%, '+c3+' 100%);"></div>';
    } else {
        return '<div style="height: 25px;width: 60px;background: linear-gradient(110deg, '+c1+' 50%, '+c1+' 75%, '+c1+' 100%);"></div>';
    }
}
