$(document).ready(function(){
    const searchBar = $('#searchBar');
    const searched = $('#searched tr');

    searchBar.on("keyup", function() {
        var value = $(this).val().toLowerCase();
        searched.filter(function() {
            $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
        });
    });
});
