$(document).ready(function () {
    const updateDriverModal = $('#updateDriver');
    const updateDriverBtn = $('#updateDriver-btn');
    const deactivateDriverModal = $('#deactivateDriver');
    const deactivateDriverBtn = $('#deactivateDriver-btn');
    const updateTeamModal = $('#updateTeam');
    const updateTeamBtn = $('#updateTeam-btn');
    const removeTeamModal = $('#removeTeam');
    const removeTeamBtn = $('#removeTeam-btn');
    const updateLeagueModal = $('#updateLeague');
    const updateLeagueBtn = $('#updateLeague-btn');
    const updateSplitModal = $('#updateSplit');
    const updateSplitBtn = $('#updateSplit-btn');
    const transferSuperadminModal = $('#transferSuperadmin');
    const transferSuperadminBtn = $('#transferSuperadmin-btn');
    const revokePermissionModal = $('#revokePermission');
    const revokePermissionBtn = $('#revokePermission-btn');
    const newSeasonModal = $('#newSeason');
    const newSeasonBtn = $('#newSeason-btn');
    const updateRaceModal = $('#updateRace');
    const updateRaceBtn = $('#updateRace-btn');
    const removeRaceModal = $('#removeRace');
    const removeRaceBtn = $('#removeRace-btn');
    const issuePenaltyModal = $('#issuePenalty');
    const issuePenaltyBtn = $('#issuePenalty-btn');
    const addPenaltyPointsModal = $('#addPenaltyPoints');
    const addPenaltyPointsBtn = $('#addPenaltyPoints-btn');
    const removePointsModal = $('#removePoints');
    const removePointsBtn = $('#removePoints-btn');
    const removePenaltyModal = $('#removePenalty');
    const removePenaltyBtn = $('#removePenalty-btn');
    const addDriverModal = $('#addDriver');
    const addDriverBtn = $('#addDriver-btn');
    const addTeamModal = $('#addTeam');
    const addTeamBtn = $('#addTeam-btn');
    const addTracksModal = $('#addTracks');
    const addTracksBtn = $('#addTracks-btn');
    const addTrackModal = $('#newTrack');
    const addTrackBtn = $('#newTrack-btn');
    const renameLeagueModal = $('#renameLeague');
    const renameLeagueBtn = $('#renameLeague-btn');
    const addClassBtn = $('#addClass-btn');
    const removeClassModal = $('#removeClass');
    const removeClassBtn = $('#removeClass-btn');


    /**
     * --------------------------------------------------------------------------------------------
     * Add driver
     * --------------------------------------------------------------------------------------------
     */
    // What to do when add drivers button is clicked
    addDriverBtn.click(function (e) {
        e.preventDefault();

        const drivers = $('#drivers');

        // Get message divs
        const errorMessage = $('#error');
        const successMessage = $('#success');

        // Hide them
        errorMessage.html('');
        errorMessage.hide();
        successMessage.html('');
        successMessage.hide();

        // Setup ajax
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('#addDriver input[name=_token]').val()
            }
        });

        // Create ajax request of POST type
        $.ajax({
            url: window.location.pathname,
            type: 'POST',
            data: {
                drivers: drivers.val(),
            },
            success: function (result) {
                if (result.errors) {
                    // Show errors
                    errorMessage.html('');

                    $.each(result.errors, function (key, value) {
                        errorMessage.show();
                        errorMessage.append('<li>' + value + '</li>');
                    });
                } else {
                    // Success
                    sessionStorage.setItem('message', result.success);
                    location.reload(true);
                }
            }
        });
    });

    /**
     * --------------------------------------------------------------------------------------------
     * Update driver
     * --------------------------------------------------------------------------------------------
     */

    // What to do when modal is shown
    updateDriverModal.on('show.bs.modal', function (e) {
        // Get related button
        const button = $(e.relatedTarget);

        // Get data from button's dataset
        const idDriver = button.data('driverid');
        const name = button.data('drivername');
        const team = button.data('driverteam');
        const active = button.data('driveractive');
        const driverClass = button.data('driverclass');

        // Get modal
        const modal = $(this);

        // Update values
        modal.find('#idDriver-u').val(idDriver);
        modal.find('.modal-body #driverName').val(name);
        if (team) {
            modal.find('.modal-body #driverTeam').val(team);
        }
        if (active) {
            modal.find('.modal-body #driverActive').prop("checked", true);
        }
        if (driverClass) {
            modal.find('.modal-body #driverClass').val(driverClass);
        }
    });

    // What to do when update driver button is clicked
    updateDriverBtn.click(function (e) {
        e.preventDefault();

        const idDriver = $('#idDriver-u');

        // Get message divs
        const errorMessage = $('#error-u');
        const successMessage = $('#success-u');

        // Hide them
        errorMessage.html('');
        errorMessage.hide();
        successMessage.html('');
        successMessage.hide();

        // Setup ajax
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('#updateDriver input[name=_token]').val()
            }
        });

        // Create ajax request of PATCH type
        $.ajax({
            url: window.location.pathname + "/" + idDriver.val(),
            type: 'PATCH',
            data: {
                name: $('#driverName').val(),
                team: $('#driverTeam').val(),
                active: $('#driverActive').is(':checked') ? 1 : 0,
                class: $('#driverClass').val(),
            },
            success: function (result) {
                if (result.errors) {
                    // Show errors
                    errorMessage.html('');

                    $.each(result.errors, function (key, value) {
                        errorMessage.show();
                        errorMessage.append('<li>' + value + '</li>');
                    });
                } else {
                    // Success
                    sessionStorage.setItem('message', result.success);
                    location.reload(true);
                }
            }
        });
    });

    /**
     * --------------------------------------------------------------------------------------------
     * Deactivate driver
     * --------------------------------------------------------------------------------------------
     */

    // What to do when modal is shown
    deactivateDriverModal.on('show.bs.modal', function (e) {
        // Get related button
        const button = $(e.relatedTarget);

        // Get data from button's dataset
        const idDriver = button.data('driverid');

        // Get modal
        const modal = $(this);

        // Update values
        modal.find('#idDriver-d').val(idDriver);
    });

    // What to do when update driver button is clicked
    deactivateDriverBtn.click(function (e) {
        e.preventDefault();

        const idDriver = $('#idDriver-d');

        // Get message divs
        const errorMessage = $('#error-d');
        const successMessage = $('#success-d');

        // Hide them
        errorMessage.html('');
        errorMessage.hide();
        successMessage.html('');
        successMessage.hide();

        // Setup ajax
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('#deactivateDriver input[name=_token]').val()
            }
        });

        // Create ajax request of DELETE type
        $.ajax({
            url: window.location.pathname + "/" + idDriver.val(),
            type: 'DELETE',
            success: function (result) {
                if (result.errors) {
                    // Show errors
                    errorMessage.html('');

                    $.each(result.errors, function (key, value) {
                        errorMessage.show();
                        errorMessage.append('<li>' + value + '</li>');
                    });
                } else {
                    // Success
                    sessionStorage.setItem('message', result.success);
                    location.reload(true);
                }
            }
        });
    });

    /**
     * --------------------------------------------------------------------------------------------
     * Add class
     * --------------------------------------------------------------------------------------------
     */
    // What to do when add classes button is clicked
    addClassBtn.click(function (e) {
        console.log('yes');
        e.preventDefault();

        const classes = $('#classes');

        // Get message divs
        const errorMessage = $('#error-ac');
        const successMessage = $('#success-ac');

        // Hide them
        errorMessage.html('');
        errorMessage.hide();
        successMessage.html('');
        successMessage.hide();

        // Setup ajax
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('#addClass input[name=_token]').val()
            }
        });

        // Create ajax request of POST type
        $.ajax({
            url: window.location.pathname + "/add-classes",
            type: 'POST',
            data: {
                classes: classes.val(),
            },
            success: function (result) {
                if (result.errors) {
                    // Show errors
                    errorMessage.html('');

                    $.each(result.errors, function (key, value) {
                        errorMessage.show();
                        errorMessage.append('<li>' + value + '</li>');
                    });
                } else {
                    // Success
                    sessionStorage.setItem('message', result.success);
                    location.reload(true);
                }
            }
        });
    });

    /**
     * --------------------------------------------------------------------------------------------
     * Remove class
     * --------------------------------------------------------------------------------------------
     */

    // What to do when modal is shown
    removeClassModal.on('show.bs.modal', function (e) {
        // Get related button
        const button = $(e.relatedTarget);

        // Get data from button's dataset
        const idClass = button.data('classid');

        // Get modal
        const modal = $(this);

        // Update values
        modal.find('#idClass').val(idClass);
    });

    // What to do when update driver button is clicked
    removeClassBtn.click(function (e) {
        e.preventDefault();

        const idClass = $('#idClass');

        // Get message divs
        const errorMessage = $('#error-dc');
        const successMessage = $('#success-dc');

        // Hide them
        errorMessage.html('');
        errorMessage.hide();
        successMessage.html('');
        successMessage.hide();

        // Setup ajax
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('#removeClass input[name=_token]').val()
            }
        });

        // Create ajax request of DELETE type
        $.ajax({
            url: window.location.pathname + "/remove-class/" + idClass.val(),
            type: 'DELETE',
            success: function (result) {
                if (result.errors) {
                    // Show errors
                    errorMessage.html('');

                    $.each(result.errors, function (key, value) {
                        errorMessage.show();
                        errorMessage.append('<li>' + value + '</li>');
                    });
                } else {
                    // Success
                    sessionStorage.setItem('message', result.success);
                    location.reload(true);
                }
            }
        });
    });

    /**
     * --------------------------------------------------------------------------------------------
     * Add team
     * --------------------------------------------------------------------------------------------
     */
    // What to do when add teams button is clicked
    addTeamBtn.click(function (e) {
        e.preventDefault();

        const teams = $('#teams');

        // Get message divs
        const errorMessage = $('#error');
        const successMessage = $('#success');

        // Hide them
        errorMessage.html('');
        errorMessage.hide();
        successMessage.html('');
        successMessage.hide();

        // Setup ajax
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('#addTeam input[name=_token]').val()
            }
        });

        // Create ajax request of POST type
        $.ajax({
            url: window.location.pathname,
            type: 'POST',
            data: {
                teams: teams.val(),
            },
            success: function (result) {
                if (result.errors) {
                    // Show errors
                    errorMessage.html('');

                    $.each(result.errors, function (key, value) {
                        errorMessage.show();
                        errorMessage.append('<li>' + value + '</li>');
                    });
                } else {
                    // Success
                    sessionStorage.setItem('message', result.success);
                    location.reload(true);
                }
            }
        });
    });

    /**
     * --------------------------------------------------------------------------------------------
     * Update team
     * --------------------------------------------------------------------------------------------
     */

    // What to do when modal is shown
    updateTeamModal.on('show.bs.modal', function (e) {
        // Get related button
        const button = $(e.relatedTarget);

        // Get data from button's dataset
        const idTeam = button.data('teamid');
        const name = button.data('teamname');
        const primary = button.data('teamprimarycolour');
        const secondary1 = button.data('teamsecondarycolour1');
        const secondary2 = button.data('teamsecondarycolour2');

        // Get modal
        const modal = $(this);

        // Update values
        modal.find('#idTeam-u').val(idTeam);
        modal.find('.modal-body #teamName').val(name);
        modal.find('.modal-body #teamPrimary').val(primary);
        if (secondary1 !== -1) {
            modal.find('.modal-body #teamSecondary1-chbox').prop("checked", true);
            modal.find('.modal-body #teamSecondary1').val(secondary1);
        } else {
            modal.find('.modal-body #teamSecondary1-chbox').prop("checked", false);
            modal.find('.modal-body #teamSecondary1').val('#ffffff');
        }
        if (secondary2 !== -1) {
            modal.find('.modal-body #teamSecondary2-chbox').prop("checked", true);
            modal.find('.modal-body #teamSecondary2').val(secondary2);
        } else {
            modal.find('.modal-body #teamSecondary2-chbox').prop("checked", false);
            modal.find('.modal-body #teamSecondary2').val('#ffffff');
        }
    });

    // What to do when update team button is clicked
    updateTeamBtn.click(function (e) {
        e.preventDefault();

        const idTeam = $('#idTeam-u');

        // Get message divs
        const errorMessage = $('#error-u');
        const successMessage = $('#success-u');

        // Hide them
        errorMessage.html('');
        errorMessage.hide();
        successMessage.html('');
        successMessage.hide();

        // Setup ajax
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('#updateTeam input[name=_token]').val()
            }
        });

        // Create ajax request of PATCH type
        $.ajax({
            url: window.location.pathname + "/" + idTeam.val(),
            type: 'PATCH',
            data: {
                name: $('#teamName').val(),
                primary: $('#teamPrimary').val(),
                secondary1: $('#teamSecondary1-chbox').is(':checked') ? $('#teamSecondary1').val() : -1,
                secondary2: $('#teamSecondary2-chbox').is(':checked') ? $('#teamSecondary2').val() : -1,
            },
            success: function (result) {
                if (result.errors) {
                    // Show errors
                    errorMessage.html('');

                    $.each(result.errors, function (key, value) {
                        errorMessage.show();
                        errorMessage.append('<li>' + value + '</li>');
                    });
                } else {
                    // Success
                    sessionStorage.setItem('message', result.success);
                    location.reload(true);
                }
            }
        });
    });

    $('#teamSecondary1').click(function () {
        $('#teamSecondary1-chbox').prop("checked", true);
    });

    $('#teamSecondary2').click(function () {
        $('#teamSecondary2-chbox').prop("checked", true);
    });

    /**
     * --------------------------------------------------------------------------------------------
     * Remove team
     * --------------------------------------------------------------------------------------------
     */

    // What to do when modal is shown
    removeTeamModal.on('show.bs.modal', function (e) {
        // Get related button
        const button = $(e.relatedTarget);

        // Get data from button's dataset
        const idTeam = button.data('teamid');

        // Get modal
        const modal = $(this);

        // Update values
        modal.find('#idTeam-d').val(idTeam);
    });

    // What to do when update driver button is clicked
    removeTeamBtn.click(function (e) {
        e.preventDefault();

        const idTeam = $('#idTeam-d');

        // Get message divs
        const errorMessage = $('#error-d');
        const successMessage = $('#success-d');

        // Hide them
        errorMessage.html('');
        errorMessage.hide();
        successMessage.html('');
        successMessage.hide();

        // Setup ajax
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('#removeTeam input[name=_token]').val()
            }
        });

        // Create ajax request of DELETE type
        $.ajax({
            url: window.location.pathname + "/" + idTeam.val(),
            type: 'DELETE',
            success: function (result) {
                if (result.errors) {
                    // Show errors
                    errorMessage.html('');

                    $.each(result.errors, function (key, value) {
                        errorMessage.show();
                        errorMessage.append('<li>' + value + '</li>');
                    });
                } else {
                    // Success
                    sessionStorage.setItem('message', result.success);
                    location.reload(true);
                }
            }
        });
    });

    /**
     * --------------------------------------------------------------------------------------------
     * Update league
     * --------------------------------------------------------------------------------------------
     */
    updateLeagueBtn.click(function () {
        $('#updateLeague-Form').submit();
        updateLeagueModal.modal('hide');
    });

    /**
     * --------------------------------------------------------------------------------------------
     * Rename league
     * --------------------------------------------------------------------------------------------
     */
    // What to do when rename league button is clicked
    renameLeagueBtn.click(function (e) {
        e.preventDefault();

        // Get message divs
        const errorMessage = $('#error');
        const successMessage = $('#success');

        // Hide them
        errorMessage.html('');
        errorMessage.hide();
        successMessage.html('');
        successMessage.hide();

        // Setup ajax
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('#renameLeague input[name=_token]').val()
            }
        });

        // Create ajax request of POST type
        $.ajax({
            url: window.location.pathname + "/rename",
            type: 'POST',
            data: {
                leagueName: $('#leagueName').val(),
                leagueId: $('#leagueId').val(),
            },
            success: function (result) {
                if (result.errors) {
                    // Show errors
                    errorMessage.html('');

                    $.each(result.errors, function (key, value) {
                        errorMessage.show();
                        errorMessage.append('<li>' + value + '</li>');
                    });
                } else {
                    // Success
                    sessionStorage.setItem('message', result.message);
                    window.location.replace("/" + encodeURI(result.success));
                }
            }
        });
    });

    /**
     * --------------------------------------------------------------------------------------------
     * Update split
     * --------------------------------------------------------------------------------------------
     */
    // What to do when update split button is clicked
    updateSplitBtn.click(function (e) {
        e.preventDefault();

        // Get message divs
        const errorMessage = $('#error');
        const successMessage = $('#success');

        // Hide them
        errorMessage.html('');
        errorMessage.hide();
        successMessage.html('');
        successMessage.hide();

        // Setup ajax
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('#updateSplit input[name=_token]').val()
            }
        });

        // Create ajax request of PATCH type
        $.ajax({
            url: window.location.pathname,
            type: 'PATCH',
            data: {
                splitName: $('#splitName').val(),
                idPlatform: $('#idPlatform').val(),
                idGame: $('#idGame').val(),
                idRegion: $('#idRegion').val(),
                maxPerTeam: $('#maxPerTeam').val(),
                maxPerRace: $('#maxPerRace').val(),
                points: $('#points').val(),
                season: $('#season').val(),
                noPointsDnf: $('#noPointsDnf').is(':checked') ? 1 : 0,
                omitInactive: $('#omitInactive').is(':checked') ? 1 : 0,
            },
            success: function (result) {
                if (result.errors) {
                    // Show errors
                    errorMessage.html('');

                    $.each(result.errors, function (key, value) {
                        errorMessage.show();
                        errorMessage.append('<li>' + value + '</li>');
                    });
                } else {
                    // Success
                    sessionStorage.setItem('message', result.success);
                    location.reload(true);
                }
            }
        });
    });

    /**
     * --------------------------------------------------------------------------------------------
     * Transfer superadmin
     * --------------------------------------------------------------------------------------------
     */
    // What to do when modal is shown
    transferSuperadminModal.on('show.bs.modal', function (e) {
        // Get related button
        const button = $(e.relatedTarget);

        // Get data from button's dataset
        const idAdmin = button.data('adminid');
        const idSplit = button.data('splitid');
        const league = button.data('league');

        transferSuperadminBtn.attr('href', '/a/'+league+'/'+idSplit+'/transfer-sa/'+idAdmin)
    });

    /**
     * --------------------------------------------------------------------------------------------
     * Revoke permission
     * --------------------------------------------------------------------------------------------
     */
    // What to do when modal is shown
    revokePermissionModal.on('show.bs.modal', function (e) {
        // Get related button
        const button = $(e.relatedTarget);

        // Get data from button's dataset
        const idAdmin = button.data('adminid');
        const idSplit = button.data('splitid');
        const league = button.data('league');

        revokePermissionBtn.attr('href', '/a/'+league+'/'+idSplit+'/revoke/'+idAdmin)
    });

    /**
     * --------------------------------------------------------------------------------------------
     * New season
     * --------------------------------------------------------------------------------------------
     */
    newSeasonBtn.click(function (e) {
        e.preventDefault();

        const idTeam = $('#idTeam-u');

        // Get message divs
        const errorMessage = $('#error');
        const successMessage = $('#success');

        // Hide them
        errorMessage.html('');
        errorMessage.hide();
        successMessage.html('');
        successMessage.hide();

        // Setup ajax
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('#newSeason input[name=_token]').val()
            }
        });

        let path = window.location.pathname.replace('home', 'new-season');

        // Create ajax request of POST type
        $.ajax({
            url: path,
            type: 'POST',
            data: {
                transferDrivers: $('#transferDrivers').is(':checked') ? 1 : 0,
                transferTeams: $('#transferTeams').is(':checked') ? 1 : 0,
            },
            success: function (result) {
                if (result.errors) {
                    // Show errors
                    errorMessage.html('');

                    $.each(result.errors, function (key, value) {
                        errorMessage.show();
                        errorMessage.append('<li>' + value + '</li>');
                    });
                } else {
                    // Success
                    sessionStorage.setItem('message', result.success);
                    location.reload(true);
                }
            }
        });
    });

    /**
     * --------------------------------------------------------------------------------------------
     * Add new track
     * --------------------------------------------------------------------------------------------
     */
    // What to do when add new track button is clicked
    addTrackBtn.click(function (e) {
        e.preventDefault();

        // Get message divs
        const errorMessage = $('#error');
        const successMessage = $('#success');

        // Hide them
        errorMessage.html('');
        errorMessage.hide();
        successMessage.html('');
        successMessage.hide();

        // Setup ajax
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('#newTrack input[name=_token]').val()
            }
        });

        // Create ajax request of POST type
        $.ajax({
            url: window.location.pathname + '/add-new',
            type: 'POST',
            data: {
                trackName: $('#trackName').val(),
                trackCountry: $('#trackCountry').val()
            },
            success: function (result) {
                if (result.errors) {
                    // Show errors
                    errorMessage.html('');

                    $.each(result.errors, function (key, value) {
                        errorMessage.show();
                        errorMessage.append('<li>' + value + '</li>');
                    });
                } else {
                    // Success
                    sessionStorage.setItem('message', result.success);
                    location.reload(true);
                }
            }
        });
    });

    /**
     * --------------------------------------------------------------------------------------------
     * Add existing tracks
     * --------------------------------------------------------------------------------------------
     */
    // What to do when add tracks button is clicked
    addTracksBtn.click(function (e) {
        e.preventDefault();

        const trackSelection = $('#trackSelection');

        // Get message divs
        const errorMessage = $('#error-m');
        const successMessage = $('#success-m');

        // Hide them
        errorMessage.html('');
        errorMessage.hide();
        successMessage.html('');
        successMessage.hide();

        // Setup ajax
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('#addTracks input[name=_token]').val()
            }
        });

        // Create ajax request of POST type
        $.ajax({
            url: window.location.pathname + '/add',
            type: 'POST',
            data: {
                trackSelection: trackSelection.val(),
            },
            success: function (result) {
                if (result.errors) {
                    // Show errors
                    errorMessage.html('');

                    $.each(result.errors, function (key, value) {
                        errorMessage.show();
                        errorMessage.append('<li>' + value + '</li>');
                    });
                } else {
                    // Success
                    sessionStorage.setItem('message', result.success);
                    location.reload(true);
                }
            }
        });
    });

    /**
     * --------------------------------------------------------------------------------------------
     * Update race
     * --------------------------------------------------------------------------------------------
     */

    // What to do when modal is shown
    updateRaceModal.on('show.bs.modal', function (e) {
        // Get related button
        const button = $(e.relatedTarget);

        // Get data from button's dataset
        const idRace = button.data('raceid');
        const name = button.data('racename');
        const country = button.data('racecountry');
        const date = button.data('racedate');
        const multiplier = button.data('racemultiplier');

        // Get modal
        const modal = $(this);

        // Update values
        modal.find('#idRace-u').val(idRace);
        modal.find('.modal-body #trackName-modal').text(name);
        modal.find('.modal-body #raceMultiplier').val(multiplier);
        if (country !== -1) {
            modal.find('.modal-body #trackCountry-modal').text(country);
        } else {
            modal.find('.modal-body #trackCountry-modal').text('Unknown');
        }
        if (date !== -1) {
            modal.find('.modal-body #raceDate').val(date.slice(0, 10));
        } else {
            modal.find('.modal-body #raceDate').val(new Date().toISOString().slice(0, 10));
        }
    });

    // What to do when update driver button is clicked
    updateRaceBtn.click(function (e) {
        e.preventDefault();

        const idRace = $('#idRace-u');

        // Get message divs
        const errorMessage = $('#error-u');
        const successMessage = $('#success-u');

        // Hide them
        errorMessage.html('');
        errorMessage.hide();
        successMessage.html('');
        successMessage.hide();

        // Setup ajax
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('#updateRace input[name=_token]').val()
            }
        });

        // Create ajax request of PATCH type
        $.ajax({
            url: window.location.pathname + "/" + idRace.val(),
            type: 'PATCH',
            data: {
                date: $('#raceDate').val() ? $('#raceDate').val() : null,
                multiplier: $('#raceMultiplier').val() ? $('#raceMultiplier').val() : 1
            },
            success: function (result) {
                if (result.errors) {
                    // Show errors
                    errorMessage.html('');

                    $.each(result.errors, function (key, value) {
                        errorMessage.show();
                        errorMessage.append('<li>' + value + '</li>');
                    });
                } else {
                    // Success
                    sessionStorage.setItem('message', result.success);
                    location.reload(true);
                }
            }
        });
    });

    /**
     * --------------------------------------------------------------------------------------------
     * Remove race
     * --------------------------------------------------------------------------------------------
     */

    // What to do when modal is shown
    removeRaceModal.on('show.bs.modal', function (e) {
        // Get related button
        const button = $(e.relatedTarget);

        // Get data from button's dataset
        const idRace = button.data('raceid');

        // Get modal
        const modal = $(this);

        // Update values
        modal.find('#idRace-r').val(idRace);
    });

    // What to do when update driver button is clicked
    removeRaceBtn.click(function (e) {
        e.preventDefault();

        const idRace = $('#idRace-r');

        // Get message divs
        const errorMessage = $('#error-rr');
        const successMessage = $('#success-rr');

        // Hide them
        errorMessage.html('');
        errorMessage.hide();
        successMessage.html('');
        successMessage.hide();

        // Setup ajax
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('#removeRace input[name=_token]').val()
            }
        });

        // Create ajax request of DELETE type
        $.ajax({
            url: window.location.pathname + "/" + idRace.val(),
            type: 'DELETE',
            success: function (result) {
                if (result.errors) {
                    // Show errors
                    errorMessage.html('');

                    $.each(result.errors, function (key, value) {
                        errorMessage.show();
                        errorMessage.append('<li>' + value + '</li>');
                    });
                } else {
                    // Success
                    sessionStorage.setItem('message', result.success);
                    location.reload(true);
                }
            }
        });
    });

    /**
     * --------------------------------------------------------------------------------------------
     * Issue penalty
     * --------------------------------------------------------------------------------------------
     */
    // What to do when issue penalty button is clicked
    issuePenaltyBtn.click(function (e) {
        e.preventDefault();

        let typeSelection = $('#typeSelection');

        // Get message divs
        const errorMessage = $('#error');
        const successMessage = $('#success');

        // Hide them
        errorMessage.html('');
        errorMessage.hide();
        successMessage.html('');
        successMessage.hide();

        // Setup ajax
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('#issuePenalty input[name=_token]').val()
            }
        });

        // Create ajax request of POST type
        $.ajax({
            url: window.location.pathname,
            type: 'POST',
            data: {
                driverSelection: $('#driverSelection').val(),
                raceSelection: $('#raceSelection').val(),
                typeSelection: typeSelection.val(),
                amount: (typeSelection.val() == 3 || typeSelection.val() == 4) ? 1 : $('#amount').val(),
            },
            success: function (result) {
                if (result.errors) {
                    // Show errors
                    errorMessage.html('');

                    $.each(result.errors, function (key, value) {
                        errorMessage.show();
                        errorMessage.append('<li>' + value + '</li>');
                    });
                } else {
                    // Success
                    sessionStorage.setItem('message', result.success);
                    location.reload(true);
                }
            }
        });
    });

    /**
     * --------------------------------------------------------------------------------------------
     * Add penalty points
     * --------------------------------------------------------------------------------------------
     */
    // What to do when add penalty points button is clicked
    addPenaltyPointsBtn.click(function (e) {
        e.preventDefault();

        // Get message divs
        const errorMessage = $('#error-pp');
        const successMessage = $('#success-pp');

        // Hide them
        errorMessage.html('');
        errorMessage.hide();
        successMessage.html('');
        successMessage.hide();

        // Setup ajax
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('#addPenaltyPoints input[name=_token]').val()
            }
        });

        // Create ajax request of POST type
        $.ajax({
            url: window.location.pathname + '/add-points',
            type: 'POST',
            data: {
                driverSelectionPP: $('#driverSelectionPP').val(),
                raceSelectionPP: $('#raceSelectionPP').val(),
                amountPP: $('#amountPP').val()
            },
            success: function (result) {
                if (result.errors) {
                    // Show errors
                    errorMessage.html('');

                    $.each(result.errors, function (key, value) {
                        errorMessage.show();
                        errorMessage.append('<li>' + value + '</li>');
                    });
                } else {
                    // Success
                    sessionStorage.setItem('message', result.success);
                    location.reload(true);
                }
            }
        });
    });

    /**
     * --------------------------------------------------------------------------------------------
     * Remove points
     * --------------------------------------------------------------------------------------------
     */

    // What to do when modal is shown
    removePointsModal.on('show.bs.modal', function (e) {
        // Get related button
        const button = $(e.relatedTarget);

        // Get data from button's dataset
        const idDriver = button.data('driverid');

        // Get modal
        const modal = $(this);

        // Update values
        modal.find('#idDriver-rpp').val(idDriver);
    });

    // What to do when remove points button is clicked
    removePointsBtn.click(function (e) {
        e.preventDefault();

        const idDriver = $('#idDriver-rpp');

        // Get message divs
        const errorMessage = $('#error-rpp');
        const successMessage = $('#success-rpp');

        // Hide them
        errorMessage.html('');
        errorMessage.hide();
        successMessage.html('');
        successMessage.hide();

        // Setup ajax
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('#removePoints input[name=_token]').val()
            }
        });

        // Create ajax request of DELETE type
        $.ajax({
            url: window.location.pathname + "/remove-points/" + idDriver.val(),
            type: 'DELETE',
            success: function (result) {
                if (result.errors) {
                    // Show errors
                    errorMessage.html('');

                    $.each(result.errors, function (key, value) {
                        errorMessage.show();
                        errorMessage.append('<li>' + value + '</li>');
                    });
                } else {
                    // Success
                    sessionStorage.setItem('message', result.success);
                    location.reload(true);
                }
            }
        });
    });

    /**
     * --------------------------------------------------------------------------------------------
     * Remove penalty
     * --------------------------------------------------------------------------------------------
     */

    // What to do when modal is shown
    removePenaltyModal.on('show.bs.modal', function (e) {
        // Get related button
        const button = $(e.relatedTarget);

        // Get data from button's dataset
        const idPenalty = button.data('penaltyid');

        // Get modal
        const modal = $(this);

        // Update values
        modal.find('#idPenalty-rp').val(idPenalty);
    });

    // What to do when remove points button is clicked
    removePenaltyBtn.click(function (e) {
        e.preventDefault();

        const idPenalty = $('#idPenalty-rp');

        // Get message divs
        const errorMessage = $('#error-rp');
        const successMessage = $('#success-rp');

        // Hide them
        errorMessage.html('');
        errorMessage.hide();
        successMessage.html('');
        successMessage.hide();

        // Setup ajax
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('#removePenalty input[name=_token]').val()
            }
        });

        // Create ajax request of DELETE type
        $.ajax({
            url: window.location.pathname + "/remove-penalty/" + idPenalty.val(),
            type: 'DELETE',
            success: function (result) {
                if (result.errors) {
                    // Show errors
                    errorMessage.html('');

                    $.each(result.errors, function (key, value) {
                        errorMessage.show();
                        errorMessage.append('<li>' + value + '</li>');
                    });
                } else {
                    // Success
                    sessionStorage.setItem('message', result.success);
                    location.reload(true);
                }
            }
        });
    });
});
