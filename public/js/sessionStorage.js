$(document).ready(function(){
    if (sessionStorage.getItem('message') != null) {
        $('#messageFromSessionStorage').text(sessionStorage.getItem('message'));
        $('#messageFromSessionStorage').show();
        sessionStorage.removeItem('message');
    }
});

$(window).on('beforeunload', function () {
   $(window).scrollTop(0);
});
