$(document).ready(function () {
    const trackCountrySelection = $('#trackCountrySelection');
    const trackSelection = $('#trackSelection');
    // const addTracksBtn = $('#addTracks-btn');

    trackCountrySelection.change(function () {
        const selected = $("option:selected", this).val();

        trackSelection.find('option')
            .remove()
            .end();

        // Setup ajax
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        // Create ajax request of PATCH type
        $.ajax({
            url: window.location.pathname + "/get-tracks",
            type: 'POST',
            data: {
                trackCountry: selected,
            },
            success: function (result) {
                if (result.errors) {
                    trackSelection.append('<option value="error">'+result.errors[0]+'</option>')
                        .attr('disabled', true);
                    addTracksBtn.attr('disabled', true);
                } else {
                    $.each(result.success, function (key, value) {
                        trackSelection.append('<option value="'+value['id']+'">'+value['name']+', '+value['idCountry']+'</option>')
                    });
                }
            }
        });
    });
});
