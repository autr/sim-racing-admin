$(document).ready(function () {
    const typeSelection = $('#typeSelection');
    const amount = $('#amount');
    const collapseButtonPenForms = $('#collapseButton-pen-forms');
    const collapseFormsPen = $('#collapseForms-pen');
    const collapseButtonPenOverview = $('#collapseButton-pen-overview');
    const collapseOverviewPen = $('#collapseOverview-pen');

    // Disable amount input when certain types are selected
    typeSelection.change(function () {
        const selected = $("option:selected", this).val();

        if (selected == 3 || selected == 4) {
            amount.prop('disabled', true);
        } else {
            amount.prop('disabled', false);
        }
    });

    collapseFormsPen.on('shown.bs.collapse', function () {
        collapseButtonPenForms.html('<i class="fas fa-eye-slash">');
    });

    collapseFormsPen.on('hidden.bs.collapse', function () {
        collapseButtonPenForms.html('<i class="fas fa-eye">');
    });

    collapseOverviewPen.on('shown.bs.collapse', function () {
        collapseButtonPenOverview.html('<i class="fas fa-eye-slash">');
    });

    collapseOverviewPen.on('hidden.bs.collapse', function () {
        collapseButtonPenOverview.html('<i class="fas fa-eye">');
    });
});
