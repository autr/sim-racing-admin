<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*
 * Title page routes
 */
Route::get('/', function () {
    return view('welcome');
})->name('welcome');

/*
 * Auth routes
 */
Auth::routes();
Route::get('/redirect', 'Auth\LoginController@redirectToProvider')->name('googleRedirect');
Route::get('/callback', 'Auth\LoginController@handleProviderCallback');

/*
 * Home routes
 */
Route::get('/home', 'HomeController@index')->name('home');
Route::get('/dismiss-news', 'HomeController@dismissNews')->name('dismiss-news');

/*
 * Browse routes
 */
Route::get('/browse', 'BrowseController@index')->name('browse');
Route::get('/browse/search', 'BrowseController@search')->name('browse.search');

/*
 * League routes
 */
Route::get('/a/{league}', 'Administration\ManageLeagueController@index')->name('league.index');
Route::get('/new-league', 'Administration\ManageLeagueController@create')->name('league.create');
Route::post('/new-league', 'Administration\ManageLeagueController@store')->name('league.store');
Route::patch('/a/{league}', 'Administration\ManageLeagueController@update')->name('league.update');
Route::post('/a/{league}/rename', 'Administration\ManageLeagueController@rename')->name('league.rename');
Route::get('/a/{league}/delete', 'Administration\ManageLeagueController@destroy')->name('league.destroy');

/*
 * Split routes
 */
Route::get('/a/{league}/{split}/home', 'Administration\ManageSplitController@index')->name('split.index');
Route::get('/a/{league}/new-split', 'Administration\ManageSplitController@create')->name('split.create');
Route::post('/a/{league}/new-split', 'Administration\ManageSplitController@store')->name('split.store');
Route::patch('/a/{league}/{split}/home', 'Administration\ManageSplitController@update')->name('split.update');
Route::post('/a/{league}/{split}/new-season', 'Administration\ManageSplitController@startNewSeason')->name('split.newSeason');
Route::get('/a/{league}/{split}/delete', 'Administration\ManageSplitController@destroy')->name('split.destroy');

/*
 * Driver administration routes
 */
Route::resource('/a/{league}/{split}/drivers', 'Administration\ManageDriversController', [
    'only' => ['index', 'store', 'update', 'destroy']
]);
Route::get('/a/{league}/{split}/load-file', 'Administration\ManageDriversController@createFromFile')->name('drivers.createFromFile');
Route::post('/a/{league}/{split}/load-file', 'Administration\ManageDriversController@storeFromFile')->name('drivers.storeFromFile');
Route::post('/a/{league}/{split}/drivers/add-classes', 'Administration\ManageDriversController@storeClasses')->name('drivers.storeClasses');
Route::delete('/a/{league}/{split}/drivers/remove-class/{class}', 'Administration\ManageDriversController@destroyClass')->name('drivers.destroyClass');

/*
 * Team administration routes
 */
Route::resource('/a/{league}/{split}/teams', 'Administration\ManageTeamsController', [
    'only' => ['index', 'store', 'update', 'destroy']
]);

/*
 * Calendar routes
 */
Route::resource('/a/{league}/{split}/calendar', 'Administration\ManageCalendarController', [
    'only' => ['index', 'update', 'destroy']
]);
Route::post('/a/{league}/{split}/calendar/add', 'Administration\ManageCalendarController@store')->name('calendar.addExisting');
Route::post('/a/{league}/{split}/calendar/add-new', 'Administration\ManageCalendarController@storeNew')->name('calendar.addNew');
Route::post('/a/{league}/{split}/calendar/get-tracks', 'Administration\ManageCalendarController@getTracks')->name('calendar.getTracks');

/*
 * Result management routes
 */
Route::resource('/a/{league}/{split}/results', 'Administration\ManageResultsController', [
   'only' => ['index', 'store']
]);
Route::post('/a/{league}/{split}/results/update', 'Administration\ManageResultsController@update')->name('results.update');
Route::get('/a/{league}/{split}/results/{race}', 'Administration\ManageResultsController@select')->name('results.select');

/*
 * Permission request routes
 */
Route::get('/a/{league}/{split}/admins', 'Administration\ManageAdminsController@manageAdmins')->name('manageAdmins');
Route::get('/approve/{request}', 'Administration\ManageAdminsController@approveRequest')->name('approveRequest');
Route::get('/disapprove/{request}', 'Administration\ManageAdminsController@disapproveRequest')->name('disapproveRequest');
Route::get('/request/{split}', 'Administration\ManageAdminsController@makeRequest')->name('makeRequest');
Route::get('/a/{league}/{split}/revoke/{user}', 'Administration\ManageAdminsController@revokePermission')->name('revokePermission');
Route::get('/a/{league}/{split}/transfer-sa/{user}', 'Administration\ManageAdminsController@transferSuperadmin')->name('transferSuperadmin');
Route::get('/requests', 'Administration\ManageAdminsController@manageRequests')->name('manageRequests');

/*
 * Penalties routes
 */
Route::resource('/a/{league}/{split}/penalties', 'Administration\ManagePenaltiesController', [
    'only' => ['index', 'store', 'destroy']
]);
Route::get('/a/{league}/{split}/penalties/make-applied/{penalty}', 'Administration\ManagePenaltiesController@makeApplied')->name('penalties.makeApplied');
Route::get('/a/{league}/{split}/penalties/make-not-applied/{penalty}', 'Administration\ManagePenaltiesController@makeNotApplied')->name('penalties.makeNotApplied');
Route::delete('/a/{league}/{split}/penalties/remove-penalty/{penalty}', 'Administration\ManagePenaltiesController@destroyPenalty')->name('penalties.destroyPenalty');
Route::post('/a/{league}/{split}/penalties/add-points', 'Administration\ManagePenaltiesController@storePoints')->name('penalties.storePoints');
Route::delete('/a/{league}/{split}/penalties/remove-points/{driver}', 'Administration\ManagePenaltiesController@destroyPoints')->name('penalties.destroyPoints');
Route::get('/a/{league}/{split}/penalties/apply/{penalty}', 'Administration\ManagePenaltiesController@applyPenalty')->name('penalties.apply');

/*
 * Presentation routes
 */
Route::get('/v/{league}/{split}', 'Presentation\PresentationController@index')->name('presentation.index');
Route::get('/v/{league}/{split}/description', 'Presentation\PresentationController@description')->name('presentation.description');
Route::get('/v/{league}/{split}/lineups', 'Presentation\PresentationController@lineups')->name('presentation.lineups');
Route::get('/v/{league}/{split}/calendar', 'Presentation\PresentationController@calendar')->name('presentation.calendar');
Route::get('/v/{league}/{split}/penalties', 'Presentation\PresentationController@penalties')->name('presentation.penalties');
Route::get('/v/{league}/{split}/standings', 'Presentation\PresentationController@standings')->name('presentation.standings');
Route::get('/v/{league}/{split}/team-standings', 'Presentation\PresentationController@teamStandings')->name('presentation.teamStandings');

/*
 * Misc
 */
Route::get('/bug-report', function () {
    if (Auth::check()) {
        $managed = App\Managed_league::where('idAdmin', Auth::user()->id)
            ->groupBy('idLeague')
            ->get();
        $leagues = [];
        foreach ($managed as $m) {
            $leagues[] = $m->league;
        }

        return view('bug-report', [
            'leagues' => $leagues,
        ]);
    } else {
        return view('bug-report');
    }
})->name('bug-report');

Route::get('/guide', function () {
    if (Auth::check()) {
        $managed = App\Managed_league::where('idAdmin', Auth::user()->id)
            ->groupBy('idLeague')
            ->get();
        $leagues = [];
        foreach ($managed as $m) {
            $leagues[] = $m->league;
        }

        return view('guide', [
            'leagues' => $leagues,
        ]);
    } else {
        return view('guide');
    }
})->name('guide');

//Route::get('/config', function() {
//    Artisan::call('config:cache');
//    return "done";
//});
